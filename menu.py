# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from admin_tools.menu import items, Menu
from django_generic_flatblocks.models import GenericFlatblock


def get_gblock(slug):
    try:
        return GenericFlatblock.objects.get(slug=slug).content_object
    except Exception as ex:
        return None

def get_gblock_admin_url(slug):
    related_object = get_gblock(slug)
    if related_object:
        app_label = related_object._meta.app_label
        module_name = related_object._meta.module_name
        return '/admin/%s/%s/%s/' % (app_label, module_name, related_object.pk)
    else:
        return '#'

class CustomMenu(Menu):
    """
    Custom Menu for apple admin site.
    """
    def __init__(self, **kwargs):
        Menu.__init__(self, **kwargs)
        self.children += [
            items.MenuItem(_('Dashboard'), reverse('admin:index')),
            items.Bookmarks(),
           
            items.ModelList(
                u'Магазин',
                models=('plugins.core.models.Shop',),
            ),
            items.ModelList(
                u'Сайт',
                models=('page.models.ActionGroup','page.models.Action', 'page.models.Page', 'news.models.News', 'plugins.slider.models.Slider',
                        'django.contrib.*', 'chronograph.models.*',),
                children = [
                    items.MenuItem(u'Редактируемые блоки',
                        children = [
                            items.MenuItem(
                                u'Телефон в шапке и подвале',
                                get_gblock_admin_url('phone_one'),
                            ),
                            items.MenuItem(
                                u'Копирайт в подвале',
                                get_gblock_admin_url('copyright'),
                            ),

                            items.MenuItem(
                                u'Текст внизу на главной',
                                get_gblock_admin_url('text_on_main'),
                            ),
                        ]
                    ),
                ]
            ),
            
            items.ModelList(
                u'SEO',
                ('', ),
                children = [
                    items.MenuItem(
                        u'код для счётчиков',
                        get_gblock_admin_url('footer_block'),
                    ),

                    items.MenuItem(
                        u'robots.txt',
                        get_gblock_admin_url('robots_txt'),
                    ),
                ]
            ),
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomMenu, self).init_with_context(context)
