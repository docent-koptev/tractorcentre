from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.views.generic import TemplateView


from docent.sitemaps import *

handler500 = 'docent.views.server_error'

admin.autodiscover()
sitemaps = {
    'pages': PagesSitemap,
    'news': NewsSitemap,
    'categories': CategoriesSitemap,
    'service':ServiceSitemap,
    'articles':ArticleSitemap,
    }

urlpatterns = patterns('',
    url(r'', include('docent.urls')),
    url(r'', include('plugins.core.urls')),
    url(r'^map', TemplateView.as_view(template_name="map/map.html"),name='docent_map'),
    #url(r'^docs/', include('sphinxdoc.urls')),
    url(r'^news/', include('plugins.news.urls')),
    url(r'^articles/', include('plugins.articles.urls')),
    url(r'^service/', include('plugins.service.urls')),
    url(r'^texts/', include('plugins.simplepage.urls')),
    url(r'^contacts', include('plugins.contacts.urls')),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^photo', include('plugins.photogallery.urls')),
    url(r'^video',include('plugins.videogallery.urls')),
    url(r'^vacancies/',include('plugins.vacancies.urls')),
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    (r'^admin_tools/', include('admin_tools.urls')),
    (r'^admin/', include(admin.site.urls)),
    url(r'^sitemap.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
    )
    urlpatterns += patterns('django.contrib.staticfiles.views',
        url(r'^media/static/(?P<path>.*)$', 'serve'),
    )