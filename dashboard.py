# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for apple.
    """
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        self.children.append(modules.Group(
            title=u"Каталог и товары",
            display="tabs",
            children=[
                modules.AppList(
                    title=u'Каталог',
                    models=('plugins.catalog.models.Category','plugins.catalog.models.Product',)
                ),


                modules.AppList(
                    title=u'Заказы',
                    models=('plugins.customer.models.Customer',
                        'plugins.customer.models.Address','plugins.order.models.Order',)
                ),

                modules.AppList(
                    title='Контакты',
                    models=('plugins.contacts.models.ShopAddress','plugins.contacts.models.Manager','plugins.contacts.models.Office',)
                ),
            ]
        ))

        
        self.children.append(modules.Group(
            title=u"Управление контентом",
            display="tabs",
            children=[
                modules.AppList(
                    title=u'Меню',
                    models=('plugins.simplepage.models.ActionGroup','plguins.simplepage.models.Action',)
                ),
                modules.AppList(
                    title=u'Сервисы',
                    models=('plugins.service.*',)
                ),
                modules.AppList(
                    title=u'Фото и видео',
                    models=('plugins.photogallery.*','plugins.videogallery.models.*',)
                ),
                modules.AppList(
                    title='Контент',
                    models=('plugins.simplepage.models.Page','plugins.news.models.News','plugins.articles.models.Article',)
                ),
                modules.AppList(
                    title='Партнеры',
                    models=('plugins.manufacturer.models.Partners',)
                ),
                modules.AppList(
                    title='Вакансии',
                    models=('plugins.vacancies.models.Vacancy','plugins.vacancies.models.Rezume',)
                ),

                
            ]
        ))

        # append a recent actions module
        self.children.append(modules.RecentActions(_('Recent Actions'), 5))



class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for apple.
    """

    # we disable title because its redundant with the model list module
    title = ''

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(self.app_title, self.models),
            modules.RecentActions(
                _('Recent Actions'),
                include_list=self.get_app_content_types(),
                limit=5
            )
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomAppIndexDashboard, self).init_with_context(context)
