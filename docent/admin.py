# -*- coding: utf-8 -*-
from django.contrib import admin
from docent.seo import SEOMetadata
from rollyourown.seo.admin import register_seo_admin

register_seo_admin(admin.site, SEOMetadata)
