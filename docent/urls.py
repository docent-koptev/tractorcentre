# -*- coding: utf-8 -*-
from django.conf.urls import *
from django.views.generic.base import TemplateView
from plugins.manufacturer.models import Partners

urlpatterns = patterns('docent.views',
    url(r'^$', 'home', name='home'),
    url(r'^catalog/(?P<slug>[-\w]*)$', 'catalog_product_level', name='catalog_product_level'),
    url(r'^catalog-development/(?P<slug>[-\w]*)$', 'catalog_product_level_development', name='catalog_product_level_development'),
    url(r'^warehouse$', 'warehouse', name='warehouse_level'),
    url(r'^goods/(?P<slug>[-\w]*)$', 'product', name='product'),
    url(r'^call_me_please$', 'call_me_please', name='call_me_please'),
    url(r'^request_price_product$', 'request_price_product', name='request_price_product'),
    url(r'^sitemap$', 'sitemap', name='sitemap'),
    url(r'^robots.txt$', 'robots', name='robots'),
	url(r'^partners$', TemplateView.as_view(template_name="includes/partners.html", get_context_data=lambda: {'partners_list': Partners.objects.all().exclude(hide=True)}),name='partners'),
	url(r'^story_of_company$', TemplateView.as_view(template_name="includes/story_of_company.html",)),
	url(r'^yandex_7bc11426ddfa417a.html$', TemplateView.as_view(template_name="yandex_7bc11426ddfa417a.html",)),
    url(r'^yandex_746f642074d60718.html$', TemplateView.as_view(template_name="yandex_746f642074d60718.html",)),
)
