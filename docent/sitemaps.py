from plugins.catalog.models import ProductsSitemap, CategoriesSitemap
from plugins.manufacturer.models import BrandsSitemap
from plugins.news.models import NewsSitemap
from plugins.simplepage.models import PagesSitemap
from plugins.service.models import ServiceSitemap
from plugins.articles.models import ArticleSitemap