# -*- coding: utf-8 -*-
from django import template
from django.contrib.auth.forms import AuthenticationForm
from django.utils.safestring import SafeUnicode
import urlparse
from plugins.catalog.models import Category
from plugins.customer.forms import OrderCallPhone
from plugins.reviews.forms import ReviewForm
from plugins.cart import utils as cart_utils
from plugins.simplepage.models import Action
from plugins.simplepage.templatetags.page_tags import get_action_link
from plugins.catalog.models import *
from plugins.catalog.forms import RequestPriceForm
from plugins.news.models import News
from plugins.manufacturer.models import Manufacturer
from plugins.contacts.models import Manager

from plugins.contacts.models import ShopAddress

register = template.Library()

@register.inclusion_tag('tags/up_menu.html', takes_context=True)
def up_menu(context, current=None):
    if current:
        current = current.get_ancestors(include_self=True)[0]

    cats = Category.objects.root_nodes().filter(display=True,display_on_top=True).order_by("top_position")
    context.update({ 'cats': cats })
    return context

@register.inclusion_tag('tags/foot_menu.html', takes_context=True)
def foot_menu(context, current=None):
    main_cats = Category.objects.filter(display_on_top=True)
    context.update({ 'main_cats': main_cats, })
    return context


@register.inclusion_tag('tags/left_menu.html', takes_context=True)
def left_menu(context, current=None):
    cats = Category.objects.filter(display=True,display_on_left=True).order_by("name")
    context.update({ 'cats': cats, })
    return context    


@register.inclusion_tag('includes/login.html', takes_context=True)
def login_register_form(context):
    form = AuthenticationForm()
    context.update({'login_form': form})
    return context

@register.inclusion_tag('cart/cart_products_counter.html', takes_context=True)
def cart_products_counter(context):
    request = context.get("request")
    cart = cart_utils.get_cart(request)
    count = 0
    if cart:
        count = cart.cartitem_set.count()
    # для определения окончания слова
    if (count % 100) > 20 or (count % 100) < 5 :
        count_modulo_10 = count % 10
    else:
        count_modulo_10 = 5
    # стоимость корзины
    cart_costs = cart_utils.get_cart_costs(request, cart)
    return { 'count': count, 'count_modulo_10': count_modulo_10, "cart_costs": cart_costs }



@register.inclusion_tag('tags/field_errors.html')
def field_errors(field):
    return {'field': field}

@register.inclusion_tag('includes/breadcrumbs.html', takes_context=True)
def breadcrumbs(context, object=None, mode=None):
    items = []
    request = context.get("request")
    if isinstance(object, Action):
        tree = object.get_ancestors(include_self=True)
        for item in tree:
            item = get_action_link(item)
            items.append({ 'name': item.title, 'url': item.link })
    if isinstance(object, SafeUnicode):
        items.append({'name': object})
    return {'breadcrumbs': items}


@register.inclusion_tag("includes/call_form.html",takes_context=True)
def show_form(context):
    context.update({ 'form': OrderCallPhone() })
    return context

#show special products on main (10)
@register.inclusion_tag("includes/show_product_on_main.html",takes_context=True)
def show_product_on_main(context,vid):
    import random
    if vid == 'hit':
        products = Product.objects.filter(active=True,is_hit=True).order_by("?")[:4]
    else:
        products =  Product.objects.filter(active=True,is_new=True).order_by("?")[:4]

    context.update({ 'products': products,})
    return context

@register.inclusion_tag("includes/show_product_on_main.html",takes_context=True)
def show_product_on_collections(context,category,limit=None):
    products = Product.objects.filter(categories=category)
    if limit:
        products = products[:limit]
    context.update({ 'products': products,})
    return context

@register.inclusion_tag("includes/reviews_form.html",takes_context=True)
def review_form(context):
    context.update({ 'form': ReviewForm() })
    return context

@register.filter
# truncate after a certain number of characters
def delimeter(value):
    if value % 2 == 0:
        return True
    else:
        return False

@register.inclusion_tag('tags/nav_cat_menu.html', takes_context=True)
def nav_cat_menu(context):
    cat_list = Category.objects.filter(display=True)
    context.update({'cat_list': cat_list})
    return context


@register.inclusion_tag('tags/breadcrumbs.html', takes_context=True)
def get_breadcrumbs(context,type,slug=None):
    warehouse = False
    if type == 'catalog':
        category = Category.objects.get(slug=slug)
        context.update({'category':category,})
    if type == 'warehouse':
        warehouse = True
        context.update({'warehouse':warehouse,})
    return context


@register.filter
def YouToubeId(string):
    if 'embed' in string:
        video = string.split("embed")[1]
        if '?rel' in video:
            video = video.split("?rel")[0]
    if 'v=' in string:
        video = string.split("v=")[1]
    return video


@register.inclusion_tag('includes/call_me_please.html', takes_context=True)
def call_me_please(context):
    form = OrderCallPhone()
    context.update({'form': form})
    return context

@register.inclusion_tag('includes/request_product_price.html', takes_context=True)
def request_product_price(context):
    form = RequestPriceForm()
    context.update({'form': form})
    return context

@register.inclusion_tag('includes/get_managers_office.html', takes_context=True)
def get_managers_office(context,office):
    managers = Manager.objects.filter(office=office).order_by("man_position")
    context.update({'managers': managers,})
    return context

@register.filter
def is_null(string):
    if string == '':
        return True
    else:
        return False

@register.inclusion_tag('includes/show_filials_on_map.html', takes_context=True)
def show_filials_on_map(context):
    shops = ShopAddress.objects.all()
    context.update({'shops': shops,})
    return context

@register.filter
def shops_on_map():
    shops = ShopAddress.objects.all()
    return shops

@register.filter
def to_utf(string):
    return unicode(string)

@register.filter
def only_display_cat(descendans_list):
    finish_descendans = []
    for descendans in descendans_list:
        if descendans.display:
            if descendans not in finish_descendans:
                finish_descendans.append(descendans)

    return sorted(finish_descendans, key=lambda x: x.prioritet, reverse=False)

