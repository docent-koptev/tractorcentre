
function off_obl() {
    $(".obl").attr("fill", "#2B61A9");
}

function show_dialog(point, id) {
    var x = $(point).offset().left - 24;
    var y = $(point).offset().top - 215;
    $("#filial_"+id).css("left", x + "px");
    $("#filial_"+id).css("top", y + "px");
    $("#filial_"+id).show();
}

$(document).ready(function() {
    $("svg > *:not(.point, .obl, g)").mouseover(function() {
        off_obl();
    });
    $(".obl").mouseover(function() {
        off_obl();
        $(this).attr("fill", "#22428e"); 
    });
    $("svg > *:not(.point)").mouseover(function() {
        $(".message-box").hide();
    })
    $(".point").mouseover(function() {
        var shop_code = $(this).attr("data-filial");
        show_dialog($(this), shop_code);
    })

})

$(document).on("click",".point", function() {
    var city = $(this).attr("data-city");
    var region = $(this).attr("data-region");
    
    $("#contact_region").val(region);
    $("#contact_city").val(city);

    $("#map_city_form").submit();

    return false;
});