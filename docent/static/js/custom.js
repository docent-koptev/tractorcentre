$('.collapse').collapse();
$(document).ready(function(){
  $('.slider_1').bxSlider({
    pagerCustom: '#bx-pager',
      auto: true,
      pause: 6500,
  });

  $('.slider_2').bxSlider({
    minSlides: 6,
    maxSlides: 6,
    slideWidth: 160,
    slideMargin: 30,
    moveSlides: 1,
    pager: false,
     auto: true,
      pause: 2000,
  });

// $('.jstree-hovered').ready(function() {
//  $(this).parent('li').removeClass('jstree-closed').addClass('jstree-open');

// });






$('.window').click(function (e) {
    $('#modal-authorization').modal({
      opacity: 75,
      overlayClose: true,
    });

    return false;
  });

$('.call_back').click(function (e) {
    $('#modal_callback').modal({
      opacity: 75,
      overlayClose: true,
    });

    return false;
  });


$('.slider_tovar').bxSlider({
  pagerCustom: '#bx-pager',
  controls: false,
  infiniteLoop: false,
});

$("input[type='tel']").mask("+7 (999) 999-99-99");       

 //СКРИПТ ДЛЯ КОЛИЧЕСТВА ТОВАРА
$('.minus').click(function () {
    var $input = $(this).parent().find('input');
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();
    return false;
});
$('.plus').click(function () {
    var $input = $(this).parent().find('input');
    $input.val(parseInt($input.val()) + 1);
    $input.change();
    return false;
});




$("#arh").hover(
  function () {
    $(".test").css("display", "block");
  },
  function () {
    $(".test").css("display", "none");
  }
);


 $('.registr').click (function (){
       //alert('test');
       $('.item_2').addClass('active');
       $('#registration').addClass('active');     
  });

    $('.enter').click (function (){
       //alert('test');
       $('.item_1').addClass('active');
       $('#enter').addClass('active');     
  });



  $('.jstree').jstree();
  jQuery(".jstree ul").on("click","li.jstree-node a",function(){
      document.location.href = this; 
  });

});
 

 function Selected(a) {
  var label = a.value;
    if (label=="transport") {
       document.getElementById("order_block_3").style.display='block';
       document.getElementById("samovyvoz").disabled = "disabled";
   } else {
       document.getElementById("order_block_3").style.display='none';
       document.getElementById("samovyvoz").disabled = "";
   } 
}
 function Selected_2(a) {
  var label = a.value;
    if (label=="beznal") {
       document.getElementById("order_block_4").style.display='block';
   } else {
       document.getElementById("order_block_4").style.display='none';
   } 
}

$(document).ready(function(){
    $.ajaxSetup({ 
     beforeSend: function(xhr, settings) {
         function getCookie(name) {
             var cookieValue = null;
             if (document.cookie && document.cookie != '') {
                 var cookies = document.cookie.split(';');
                 for (var i = 0; i < cookies.length; i++) {
                     var cookie = jQuery.trim(cookies[i]);
                     // Does this cookie string begin with the name we want?
                 if (cookie.substring(0, name.length + 1) == (name + '=')) {
                     cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                     break;
                 }
             }
         }
         return cookieValue;
         }
         if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
             // Only send the token to relative URLs i.e. locally.
             xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
         }
     } 
});
});

function ShowLoader(){
  $( "#loader" ).show();
}
function HideLoader(){
  $( "#loader" ).hide();
}

function docentAjax(url){
  var $filter = $("div.filter");
  var arr = {};
  var data = '';
    $(':checkbox:checked', $filter).each(function(){
        arr[$(this).val()] = $(this).attr("name");
        data = 'data=' + JSON.stringify(arr);
    });
    $.ajax({
      url:encodeURI(url),
      data:data,
      type:"POST",
      beforeSend: function(){
        ShowLoader();
      },
      complete: function(){
        HideLoader();
      },
      success:function(data){
        $("div#docent_ajax").html("");
        $("div#docent_ajax").html(data);
      }
    });  
}



//левый фильтр
$(document).on("click",'.filter_check',function(event){
  window.location.hash = '';
  var url = $(this).attr("href");
  docentAjax(url);
});

//пагинация
$(document).on("click",'.docent_pagination li a',function(event){
  var url = $(this).attr("href");
  var word = $("#word").val();
  if (word != ''){
    url = url + "&word=" + word;
  }
  docentAjax(url);
  return false;
});

//алфавитный поиск
$(document).on("click",'.docent_word',function(event){
  var url = $(this).attr("href");
  docentAjax(url);
  $(this).addClass("active");
  return false;
});

$(document).on("click",'.video a.youtube',function(event){
  var clickEl = $(this);
  var url = $(this).attr("href");
    $.ajax({
      url:url,
      type:"GET",
      beforeSend: function(){
        ShowLoader();
      },
      complete: function(){
        HideLoader();
      },
      success:function(data){
        var link = "https://www.youtube.com/embed/" + data;
        $(".main_video iframe").attr("src",link);

      }

    });
    return false;
        
});



/*Аякс-покупка*/
$(document).on("click",'.buy',function(el){
    var url = $(this).attr("href");
    var clkEl = $(this);
    var block = '';
    if (clkEl.hasClass("add_cart_card")){
      block = clkEl.parent().children(".card_price");
    }else{
      block = clkEl;
    }
      $.ajax({
        url:url,
        type:"GET",
        success:function(data){
            block
                .clone()
                .css({ 'position' : 'absolute', 'z-index' : '1110000000000', top: clkEl.offset().top, left:clkEl.offset().left+12})
                .appendTo("body")
                .animate({opacity: 0.9,
                    left: $(".cart_insert").offset()['left'] + 20,
                    top: $(".cart_insert").offset()['top'] + 30,
                    }, 1600, function() {
                    $(this).remove();
                    $(".cart_insert").html(data);
                    });
        }
    });
    return false;

});


function display_form_errors(form,errors) {
    var input = $('form'+form +' :input,select');
    input.each(function() {
        $(this).removeClass("error");
    }); 
    for (var k in errors) {
      $("span.NoInputErrors").html(errors['__all__']);
      $("form"+form).find('input[name=' + k + ']').addClass("error").val("").attr("placeholder",errors[k]);
      $("form"+form).find('select[name=' + k + ']').addClass("error");
      $("form"+form).find('div[class=' + k + ']').html(errors['__all__']);
    }
}
//авторизация
$(document).on("click","#LoginFormSubmit", function() {
    $("#LoginForm").ajaxSubmit({
        "type" : "post",
        "dataType": 'json',
        beforeSend: function(){
          ShowLoader();
        },
        complete: function(){
          HideLoader();
        },
        "success" : function(data) {
            switch (data['status']) {
              case 0:
                location.href = data['next'];
                break
              case 1:
                display_form_errors("#LoginForm",data['errors']);
                break
            }   
        }
    })
    return false;
});

//регистрация
$(document).on("click","#RegisterFormSubmit", function() {
    $("#RegisterForm").ajaxSubmit({
        "type" : "post",
        "dataType": 'json',
          beforeSend: function(){
            ShowLoader();
          },
          complete: function(){
            HideLoader();
          },
        "success" : function(data) {
            switch (data['status']) {
              case 0:
                location.href = data['next'];
                break
              case 1:
                display_form_errors("#RegisterForm",data['errors']);
                break
            }               
        }
    })
    return false;
});

//восстановление пароля
$(document).on("click","#ResetFormSubmit", function() {
    $("#ResetForm").ajaxSubmit({
        "type" : "post",
        "dataType": 'json',
        beforeSend: function(){
          ShowLoader();
        },
        complete: function(){
          HideLoader();
        },
        "success" : function(data) {
            switch (data['status']) {
              case 0:
                $("#ResetForm").html("На Ваш электронный адрес выслана инструкция по восстановлению пароля");
                break
              case 1:
                display_form_errors("#ResetForm",data['errors']);
                break
            }               
        }
    })
    return false;
});


//Форма отправки заказа
$(document).on("click","#go_checkout", function(event) {
  event.preventDefault;
    $("#CheckoutForm").ajaxSubmit({
        "type" : "post",
        "dataType": 'json',
        beforeSend: function(){
          ShowLoader();
        },
        complete: function(){
          HideLoader();
        },
        "success" : function(data) {
            switch (data['status']) {
              case 100:
                location.href="/thank-you";
                break
              case 1:
                $("body,html").animate({
                  scrollTop:350
                }, 800);
                display_form_errors("#CheckoutForm",data['errors']);
                break
            }               
        }
    })
    return false;
});

$(function () {
  var 
    $region = $('[name="region"]'),
    $city = $('[name="city"]'),
    $street = $('[name="street"]'),
    $building = $('[name="building"]');

  var $tooltip = $('.tooltip');

  $.kladr.setDefault({
    parentInput: '.js-form-address',
    verify: true,
    select: function (obj) {
      setLabel($(this), obj.type);
      $tooltip.hide();
    },
    check: function (obj) {
      var $input = $(this);

      if (obj) {
        setLabel($input, obj.type);
        $tooltip.hide();
      }
      else {
        showError($input, 'Введено неверно');
      }
    },
    checkBefore: function () {
      var $input = $(this);

      if (!$.trim($input.val())) {
        $tooltip.hide();
        return false;
      }
    }
  });
  $region.kladr('type', $.kladr.type.region);
  $city.kladr('type', $.kladr.type.city);
  $street.kladr('type', $.kladr.type.street);
  $building.kladr('type', $.kladr.type.building);

  // Отключаем проверку введённых данных для строений
  $building.kladr('verify', false);

  function setLabel($input, text) {
    text = text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
    $input.parent().find('label').text(text);
  }

  function showError($input, message) {
    $tooltip.find('span').text(message);

    var inputOffset = $input.offset(),
      inputWidth = $input.outerWidth(),
      inputHeight = $input.outerHeight();

    var tooltipHeight = $tooltip.outerHeight();

    $tooltip.css({
      left: (inputOffset.left + inputWidth + 10) + 'px',
      top: (inputOffset.top + (inputHeight - tooltipHeight) / 2 - 1) + 'px'
    });

    $tooltip.show();
  }
});

//Работаем в личном кабинете
//Форма отправки заказа
$(document).on("click","#save_profile_form", function(event) {
  event.preventDefault;
    $("#my_office_form").ajaxSubmit({
        "type" : "post",
        "dataType": 'json',
          beforeSend: function(){
            ShowLoader();
          },
          complete: function(){
            HideLoader();
          },
        "success" : function(data) {
            switch (data['status']) {
              case 1:
                $("body,html").animate({
                  scrollTop:350
                }, 800);
                display_form_errors("#my_office_form",data['errors']);
                break
            }               
        }
    })
    return false;
});

//Заказать звонок
$(document).on("click","input.one_c", function(event) {
  $("input[type='tel']").mask("+7 (999) 999-99-99"); 
  $.ajax({
    url:$("form.call").attr("action"),
    data:$("form.call").serialize(),
    type:"POST",
    dataType:"json",
    beforeSend: function(){
      ShowLoader();
    },
    complete: function(){
      HideLoader();
    },
    success:function(data){
      if(data['status'] == -1){
         display_form_errors(".call",data['errors']);
      }
      if (data['status'] == 0){
        $("form.call").hide();
        $("div.success").show();
        setTimeout(function() { $.modal.close();}, 3500);
      }
    }
  });
  return false;
});


//Узнать цену у товара
$(document).on("click","#request_product", function(event) {
  $("#req_pr_form").ajaxSubmit({
      "type" : "post",
      "dataType": 'json',
      "beforeSend": function(){
        ShowLoader();
      },
      "complete": function(){
        HideLoader();
      },
      "success" : function(data) {
          if(data['status'] == -1){
                 display_form_errors("#req_pr_form",data['errors']);
              }
          if (data['status'] == 0){
            $("form#req_pr_form").hide();
            $(".hide_modal").hide();
            $("div.success").show();
            setTimeout(function() { $.modal.close();}, 3500);
          }
      }
  })
  return false;
});

// =============== Live search =====================
$.fn.delay_opt = function(options) {
    var timer;
    var delayImpl = function(eventObj) {
        if (timer != null) {
            clearTimeout(timer);
        }
        var newFn = function() {
            options.fn(eventObj);
        };
        timer = setTimeout(newFn, options.delay);
    }

    return this.each(function() {
        var obj = $(this);
        obj.bind(options.event, function(eventObj) {
             delayImpl(eventObj);
        });
    });
};



$(document).ready(function(){
    var $search_input = $("#searchInput");
    var $live_search_result = $("div.searchHelp");

   $(document).on("blur", $search_input.live, function(e) {
        window.setTimeout(function() {
            $live_search_result.hide();
        }, 200);
    });

  $( document ).on( "click", "#show_all", function() {
    location.href="/search?q=" + $search_input.val();
    return false;

  });
    $search_input.delay_opt({
        delay: 400,
        event: "keyup",
        fn: function(e) {

          var code = e.keyCode || e.which;
            if (code ==  27) {
                location.href="/search?q=" + $search_input.val();
            }
            else {
                            var q = $search_input.val();
                var url = $search_input.attr("data");
                $.get(url, {"q" : q}, function(data) {
                    if (data.state == "success") {
                
                        $live_search_result.html(data.products);
                        $live_search_result.show();                      

                        //$('body').append("<div class='over'></div >");
                       
                       
                    }
                    else {
                        $live_search_result.html('');
                        $live_search_result.hide(); 
                    }
                })
            }
        }
    });



});

$( document ).on( "click", ".learn_price", function() {
  var product_pk = $(this).attr("data-product");
  $("form#req_pr_form input[name=product]").val(product_pk);
  $('#modal_learn_price').modal({
      opacity: 75,
      overlayClose: true,
    });
});

