$(document).on("change","#contact_region", function() {
	$("#contact_city").html("");
    $("#form_contacts").ajaxSubmit({
        "type" : "get",
        "dataType": 'json',
        "success" : function(data) {
        	if (data['status'] == 0){
        		var option_html = '<option disabled selected>Выберите город</option>';
        		var dict = data['city_dict'];
        		for (var key in dict){
        			option_html += '<option value="' +key+'">'+ dict[key]+'</option>';
        		}
        		$("#contact_city").append(option_html)
        	}
        }
    })
    return false;
});

$(document).on("change","#contact_city", function() {
	$("#form_contacts").submit();
});