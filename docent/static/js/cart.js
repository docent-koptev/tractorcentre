$(function(){
    function refreshCart(html)
    {
        $(".article").html(html);
    }

    $(document).on("click",".delete-cart-item", function() {
        var url = $(this).attr("href");
        $.post(url, function(data) {
            refreshCart(data);
        });
        return false;
    });

    // TODO: Optimize
    $(document).on("change",".cart-amount", function() {
        $("#cart-form").ajaxSubmit({
            "type" : "post",
            "success" : function(data) {
                refreshCart(data.html);
            }
        })
    });

});
