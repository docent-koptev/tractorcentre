# -*- coding: utf-8 -*-
from rollyourown import seo

def populate_title(metadata, model_instance = None, **kwargs):                    
    if model_instance:
        return "%s" % model_instance.name
    else:
        return u'Трактороцентр'        

class SEOMetadata(seo.Metadata):
    title       = seo.Tag(head=True, max_length=250,populate_from = populate_title)
    description = seo.MetaTag(max_length=250)
    keywords    = seo.KeywordTag()

    class Meta:
        #seo_models = ('docent',)
        seo_views = ('docent', 'page', 'news', 'news_detail', 'ecko.checkout.thank_you')
        verbose_name = "SEO"
        verbose_name_plural = "SEO"

    class HelpText:
        title       = u"Тайтл страницы"
        keywords    = u"Ключевые слова"
        description = u"Описание для сео"

