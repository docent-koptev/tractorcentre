# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Place',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='\u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0433\u043e\u0440\u043e\u0434\u0430')),
            ],
            options={
                'verbose_name': '\u0433\u043e\u0440\u043e\u0434',
                'verbose_name_plural': '\u0433\u043e\u0440\u043e\u0434\u0430',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ShopAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('shop_name', models.CharField(max_length=100, verbose_name='\u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043c\u0430\u0433\u0430\u0437\u0438\u043d\u0430')),
                ('address', models.CharField(help_text='\u0424\u043e\u0440\u043c\u0430\u0442 \u0430\u0434\u0440\u0435\u0441\u0430: \u041a\u0440\u0430\u0441\u043d\u0430\u044f \u043f\u043b\u043e\u0449\u0430\u0434\u044c, 3, \u0413\u0423\u041c, 3-\u044f \u043b\u0438\u043d\u0438\u044f, 3-\u0439 \u044d\u0442\u0430\u0436, 109012 - \u041c\u043e\u0441\u043a\u0432\u0430', max_length=300, verbose_name='\u0430\u0434\u0440\u0435\u0441')),
                ('place', models.ForeignKey(verbose_name='\u0413\u043e\u0440\u043e\u0434', to='docent.Place')),
            ],
            options={
                'verbose_name': '\u0430\u0434\u0440\u0435\u0441 \u043c\u0430\u0433\u0430\u0437\u0438\u043d\u0430',
                'verbose_name_plural': '\u0430\u0434\u0440\u0435\u0441\u0430 \u043c\u0430\u0433\u0430\u0437\u0438\u043d\u043e\u0432',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ShopPhone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phone_number', models.CharField(max_length=50, verbose_name='\u043d\u043e\u043c\u0435\u0440 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0430')),
                ('shop', models.ForeignKey(related_name='phones', verbose_name='\u043c\u0430\u0433\u0430\u0437\u0438\u043d', to='docent.ShopAddress')),
            ],
            options={
                'verbose_name': '\u0442\u0435\u043b\u0435\u0444\u043e\u043d \u043c\u0430\u0433\u0430\u0437\u0438\u043d\u0430',
                'verbose_name_plural': '\u0442\u0435\u043b\u0435\u0444\u043e\u043d\u044b \u043c\u0430\u0433\u0430\u0437\u0438\u043d\u043e\u0432',
            },
            bases=(models.Model,),
        ),
    ]
