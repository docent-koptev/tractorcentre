# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('docent', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shopaddress',
            name='place',
        ),
        migrations.DeleteModel(
            name='Place',
        ),
        migrations.RemoveField(
            model_name='shopphone',
            name='shop',
        ),
        migrations.DeleteModel(
            name='ShopAddress',
        ),
        migrations.DeleteModel(
            name='ShopPhone',
        ),
    ]
