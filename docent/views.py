# -*- coding: utf-8 -*-
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.contrib.auth import login, authenticate
from django import forms
from django.shortcuts import get_object_or_404, redirect, render,HttpResponse
from django.views.decorators.csrf import requires_csrf_token
from annoying.decorators import render_to


from plugins.catalog.models import VARIANT
from plugins.catalog.models import *
from plugins.manufacturer.models import Manufacturer,Partners
from plugins.slider.models import Slider
from plugins.mail.utils import send_mail_to_order_call,send_request_to_product_price
from plugins.customer.forms import OrderCallPhone
from plugins.reviews.models import Review
from utils.helpers import Paginate
from plugins.videogallery.models import Video
from plugins.catalog.forms import RequestPriceForm



from django.http import Http404

from docent.settings import *
from django.core.cache import cache
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json

from django.shortcuts import render_to_response
from django.template import RequestContext

from annoying.functions import get_object_or_None

from django.db.models import Q

from docent.sitemaps import *

@render_to(template='home/home.html')
def home(request):
    slider = Slider.objects.filter(display=True, show_on='main')
    partners_list = Partners.objects.filter(show_on_main=True).exclude(hide=True)
    video = Video.objects.all()[0]

    if not request.session.get('last_category'):
        request.session["last_category"] = request.get_full_path()
    if not request.session.exists(request.session.session_key):
        request.session.create()
    return locals()

def get_product_sorter_params(request):
    count = int(request.GET.get('count_show', request.session.get('count_show', PAGING_COUNTS[0]['value'])))
    page = int(request.GET.get('page', 5))

    if count != request.session.get('count_show'):
        page = 1
    if count:
        request.session['count_show'] = count
    return count, page

def reverse_key(filters_elements):
    elements = {}
    for key,parent in filters_elements.iteritems():
        if parent in elements.keys(): #check if key already exist in a dict
            elements[parent].update(key) 
        else:
            elements[parent] = key
    return elements

def catalog_product_level(request,slug=None,filter_str=''):
    products_list = None
    count_show, page = get_product_sorter_params(request)

    if slug is not None:
        category = get_object_or_None(Category, slug=slug,display=True)
    if category is not None:
        ancestors = category.get_ancestors()
        nodes = list(category.get_children().filter(display=True).order_by("name"))
        
        #Показываем верхнее меню. Уровень 0 (главные родители)
        products_list = category.get_all_products()
        filter_list = {}
        tech_info = ProductTechInfo.objects.filter(product__in=list(products_list)).order_by("name")
        for p in tech_info:
            filter_list[p.value] = p.pk
        tech_info = tech_info.filter(pk__in=list(filter_list.values()))
        word_list = []
        for product in products_list:
            curr_word = product.name[0].upper()
            if curr_word not in word_list:
                word_list.append(curr_word)

        if category.template == 1:
            template = 'catalog/catalog_product_with_variants.html'
        if category.template == 2:
            template =  'catalog/development/catalog_product_with_variants_new_cat.html'
        if category.template == 3:
            template = 'catalog/catalog_main_category.html'

    else:
        raise Http404("")
    
    if request.is_ajax():
        if 'word' in request.GET:
            word = request.GET.get('word')
            products_list = products_list.filter(name__startswith=word)
        params = None
        template = 'catalog/catalog_ajax.html'
        if category.template == 2:
            template = 'catalog/development/catalog_ajax.html'
        if 'data' in request.POST:
            params = request.POST['data']
        if params is not None:
            data = params.encode("utf-8")
            filters_elements = json.loads(data)
            products_pks = []
            products_pks_tmp = []
            v = {}
            for key, value in sorted(filters_elements.iteritems()):
                v.setdefault(value, []).append(key)
            for key, value in v.iteritems():
                for p in value:
                    tech_info = ProductTechInfo.objects.filter(name__name=unicode(key),value=unicode(p))
                    for ps in tech_info:
                        products_pks_tmp.append(ps.product.pk)
            if products_pks_tmp:
                dict_len = len(v)
                for el in products_pks_tmp:
                    count = products_pks_tmp.count(el)
                    if count == dict_len:
                        products_pks.append(el)

                if category is not None:
                    products_list = products_list.filter(pk__in=products_pks,category=category)
                else:
                    products_list = products_list.filter(pk__in=products_pks)
    
    if products_list:
        products = Paginate(request,products_list,count_show)

    return render_to_response(template,locals(),context_instance=RequestContext(request))



def catalog_product_level_development(request,slug=None,filter_str=''):
    products_list = None
    count_show, page = get_product_sorter_params(request)

    if slug is not None:
        category = get_object_or_None(Category, slug=slug,display=True)
    if category is not None:
        ancestors = category.get_ancestors()
        nodes = list(category.get_children().filter(display=True).order_by("name"))
        
        #Показываем верхнее меню. Уровень 0 (главные родители)

        products_list = category.get_all_products()
        filter_list = {}
        tech_info = ProductTechInfo.objects.filter(product__in=list(products_list)).order_by("name")
        for p in tech_info:
            filter_list[p.value] = p.pk
        tech_info = tech_info.filter(pk__in=list(filter_list.values()))
        word_list = []
        for product in products_list:
            curr_word = product.name[0].upper()
            if curr_word not in word_list:
                word_list.append(curr_word)
    else:
        raise Http404("")
    
    
    if products_list:
        products = Paginate(request,products_list,count_show)

    template = 'catalog/development/catalog_product_with_variants_new_cat.html'
    return render_to_response(template,locals(),context_instance=RequestContext(request))

def warehouse(request):
    template = 'catalog/warehouse.html'
    all_cats = Category.objects.filter(parent=None,display=True,display_on_top=True).exclude(this_is_text_cat=True)
    
    #all_cats = sorted(tmp_all_cats, key=lambda x: x.prioritet, reverse=False)
    return render_to_response(template,locals(),context_instance=RequestContext(request))


def sitemap(request):
    template = 'sitemap.html'
    productSM = ProductsSitemap
    categorySM = CategoriesSitemap
    newsSM = NewsSitemap
    pageSM = PagesSitemap
    serviceSM = ServiceSitemap
    articelSM = ArticleSitemap

    


    return render_to_response(template,locals(),context_instance=RequestContext(request))

@render_to(template='product/product.html')
def product(request, slug):
    try:
        p = get_object_or_404(Product, slug=slug,active=True)
    except Exception as e:
        raise Http404
    related_products = p.related_products.all()
    tech_info = ProductTechInfo.objects.filter(product=p)
    tech_info_other = ProductTechInfoOtherAttrs.objects.filter(product=p)
    breadcrumbs = []
    breadcrumbs = p.get_categories()
    return locals()

@requires_csrf_token
def server_error(request, template_name='500.html'):
    return render(request, template_name)

def robots(request):
    return render(request, 'robots.txt', {}, content_type='text/plain')

def call_me_please(request):
    response = {'status':0,'errors':None,}
    if request.method == 'POST':
        form = OrderCallPhone(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            phone = cd.get("phone")
            name = cd.get("name")
            send_mail_to_order_call(phone,name)
        else:
            errors = {}
            for k in form.errors:
                errors[k] = form.errors[k][0]
            response = {'status':-1,'errors':errors}
    return HttpResponse(json.dumps(response))

def request_price_product(request):
    response = {'status':0,'errors':None,}
    if request.method == 'POST':
        form = RequestPriceForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            phone = cd.get("phone")
            name = cd.get("name")
            email = cd.get("email","")
            product = get_object_or_None(Product,pk=int(request.POST.get("product")))
            send_request_to_product_price(product,name,phone,email)
        else:
            errors = {}
            for k in form.errors:
                errors[k] = form.errors[k][0]
            response = {'status':-1,'errors':errors}
    return HttpResponse(json.dumps(response))