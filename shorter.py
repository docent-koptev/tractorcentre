# -*- coding: utf-8 -*-
import os
import base64 

from django.contrib.contenttypes.models import ContentType
import xml.etree.ElementTree as ET

from plugins.catalog.models import *
from annoying.functions import get_object_or_None
from helpers.filesandimages.models import AttachedImage

def assign_cat():
	cats = Category.objects.all()
	for cat in cats:
		if cat.puid is not None:
			try:
				category = get_object_or_None(Category,uid=cat.puid)
				cat.parent = category
				cat.save()
			except Exception:
				print cat.puid

def swith_off_cat_no_product():
	cats = Category.objects.all().exclude(this_is_text_cat=True)
	for cat in cats:
		if not cat.has_products():
			cat.display = False
			print cat.name
			cat.save()


def cats_null():
	cats = Category.objects.all()
	for cat in cats:
		if cat.name[-1] == '0':
			cat.delete()


def assign_cat_for_tech():
	category_technika = Category.objects.get(pk=1259)
	products = Product.objects.filter(category=category_technika)
	tech_attr_cat = TechAttrs.objects.get(pk=36)
	for p in products:
		last_cat = p.get_last_category()
		if last_cat is not None:
			prod_tech,created = ProductTechInfo.objects.get_or_create(product=p,name=tech_attr_cat, defaults={'value': last_cat.name})