# -*- coding: utf-8 -*-
from django.http import Http404
from django.shortcuts import get_object_or_404
from annoying.decorators import render_to
from plugins.news.models import News
from django.utils.translation import ugettext_lazy as _
from datetime import datetime
from utils.helpers import Paginate

SORT_BY_DATE = (
    ('-last_modification', _(u'начиная с последних &darr;')),
    ('last_modification', _(u'начиная с первых &uarr;')),
)

SORT_PARAM = 'sort_news'

def get_sort(request):
    sort = request.GET.get(SORT_PARAM, request.session.get(SORT_PARAM, SORT_BY_DATE[0][0]))
    if sort:
        request.session['sort'] = sort
    return sort

@render_to('news_list.html')
def news(request):
    sort = get_sort(request)
    news_list = News.objects.filter(display=True, last_modification__lt=datetime.now()).order_by(sort)
    news = Paginate(request,news_list,10,2)
    return { 'news': news, 'sort': sort, 'SORT_PARAM': SORT_PARAM, 'SORT_BY_DATE': SORT_BY_DATE }

@render_to('news_detail.html')
def news_detail(request, slug):
    news_item = get_object_or_404(News, slug=slug)
    if news_item.display:
        return { 'news_item': news_item }
    raise Http404('No %s matches the given query.' % news_item._meta.object_name)
