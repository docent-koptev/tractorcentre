from datetime import datetime
from django import template
from plugins.news.models import News

register = template.Library()

@register.inclusion_tag('tags/last_news.html')
def last_news(number):
    last = News.objects.filter(display=True, creation_date__lt=datetime.now())[:number]
    return {'last': last}


@register.inclusion_tag('news_on_main.html', takes_context=True)
def news_list(context):
    news = News.objects.filter(display=True).order_by('-last_modification')[:3]
    context.update({'news':news })
    return context