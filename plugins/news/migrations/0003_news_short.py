# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_remove_news_short'),
    ]

    operations = [
        migrations.AddField(
            model_name='news',
            name='short',
            field=models.TextField(help_text='\u041a\u0440\u0430\u0442\u043a\u0438\u0439 \u0442\u0435\u043a\u0441\u0442.\u0410\u043d\u043e\u043d\u0441', verbose_name='\u043a\u0440\u0430\u0442\u043a\u043e', blank=True),
            preserve_default=True,
        ),
    ]
