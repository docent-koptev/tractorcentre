# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0437\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('slug', models.SlugField(help_text='\u0444\u043e\u0440\u043c\u0438\u0440\u0443\u0435\u0442 \u0432\u0438\u0434 \u0443\u0440\u043b \u043d\u043e\u0432\u043e\u0441\u0442\u0438.\u0423\u043d\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u044d\u043b\u0435\u043c\u0435\u043d\u0442 \u0432 \u043d\u043e\u0432\u043e\u0441\u0442\u044f\u0445', unique=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430')),
                ('body', models.TextField(help_text='\u041f\u043e\u043b\u043d\u044b\u0439 \u0442\u0435\u043a\u0441\u0442 \u043d\u043e\u0432\u043e\u0441\u0442\u0438', verbose_name='\u0442\u0435\u043a\u0441\u0442 \u043d\u043e\u0432\u043e\u0441\u0442\u0438', blank=True)),
                ('short', models.TextField(help_text='\u041a\u0440\u0430\u0442\u043a\u0438\u0439 \u0442\u0435\u043a\u0441\u0442. \u0414\u043b\u044f \u0430\u043d\u043e\u043d\u0441\u0430 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439,\u0438\u043b\u0438 \u0432 \u0441\u043f\u0438\u0441\u043a\u0435 \u043d\u043e\u0432\u043e\u0441\u0442\u0435\u0439.\u0412\u0441\u0435\u0433\u043e \u043e\u0434\u043d\u0443 \u0441\u0442\u0440\u043e\u0447\u043a\u0443', verbose_name='\u043a\u0440\u0430\u0442\u043a\u043e', blank=True)),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=b'uploads/news', verbose_name='\u0438\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435', blank=True)),
                ('display', models.BooleanField(default=True, verbose_name='\u043e\u043f\u0443\u0431\u043b\u0438\u043a\u043e\u0432\u0430\u0442\u044c')),
                ('last_modification', models.DateTimeField(verbose_name='\u0434\u0430\u0442\u0430 \u043f\u043e\u0441\u043b\u0435\u0434\u043d\u0435\u0433\u043e \u0438\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u044f', blank=True)),
            ],
            options={
                'ordering': ('-last_modification',),
                'verbose_name': '\u043d\u043e\u0432\u043e\u0441\u0442\u044c \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438',
                'verbose_name_plural': '\u043d\u043e\u0432\u043e\u0441\u0442\u0438 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438',
            },
            bases=(models.Model,),
        ),
    ]
