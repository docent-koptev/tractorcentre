# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from django.db import models
from django import forms
from plugins.news.models import News
from sorl.thumbnail import get_thumbnail
from sorl.thumbnail.admin import AdminImageMixin
from ckeditor.widgets import CKEditorWidget


class NewsForm(forms.ModelForm):
    short = forms.CharField(widget=CKEditorWidget(),label=u'Кратко')
    body = forms.CharField(widget=CKEditorWidget(),label=u'Текст полный')
    class Meta:
        model = News
        fields = "__all__"

class NewsAdmin(AdminImageMixin, admin.ModelAdmin):
    form = NewsForm
    list_display = ['title','display', 'last_modification',]
    list_editable = ['display',]
    search_fields = ['title', 'body', ]
    list_filter = ['display',]
    readonly_fields = ['last_modification',]

    prepopulated_fields = {'slug': ['title',]}
    fieldsets = (
        (None,{
            'fields': ('title', 'slug', 'short' ,'body',  'image', 'last_modification','display',  )
        }),
    )

admin.site.register(News, NewsAdmin)