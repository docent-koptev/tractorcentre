# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Action',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=40, verbose_name='Title', blank=True)),
                ('link', models.CharField(help_text=b'\xd0\xbc\xd0\xbe\xd0\xb6\xd0\xbd\xd0\xbe \xd0\xb8\xd1\x81\xd0\xbf\xd0\xbe\xd0\xbb\xd1\x8c\xd0\xb7\xd0\xbe\xd0\xb2\xd0\xb0\xd1\x82\xd1\x8c \xd1\x88\xd0\xb0\xd0\xb1\xd0\xbb\xd0\xbe\xd0\xbd reverse__["url name", (arg1, arg2), {"kwarg1": kwarg1} ]', max_length=100, verbose_name='Link', blank=True)),
                ('active', models.BooleanField(default=False, verbose_name='Active')),
                ('position', models.IntegerField(default=999, verbose_name='Position')),
                ('lft', models.PositiveIntegerField(editable=False, db_index=True)),
                ('rght', models.PositiveIntegerField(editable=False, db_index=True)),
                ('tree_id', models.PositiveIntegerField(editable=False, db_index=True)),
                ('level', models.PositiveIntegerField(editable=False, db_index=True)),
            ],
            options={
                'ordering': ('position',),
                'verbose_name': '\u042d\u043b\u0435\u043c\u0435\u043d\u0442 \u043c\u0435\u043d\u044e',
                'verbose_name_plural': '\u042d\u043b\u0435\u043c\u0435\u043d\u0442\u044b \u043c\u0435\u043d\u044e',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ActionGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100, blank=True)),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': '\u041c\u0435\u043d\u044e',
                'verbose_name_plural': '\u041c\u0435\u043d\u044e',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100, verbose_name='Title')),
                ('slug', models.CharField(max_length=100, verbose_name='Slug')),
                ('short_text', models.TextField(blank=True)),
                ('body', models.TextField(verbose_name='Text', blank=True)),
                ('active', models.BooleanField(default=False, verbose_name='Active')),
                ('exclude_from_navigation', models.BooleanField(default=False, verbose_name='Exclude from navigation')),
                ('position', models.IntegerField(default=999, verbose_name='Position')),
                ('file', models.FileField(upload_to=b'files', verbose_name='File', blank=True)),
            ],
            options={
                'ordering': ('position',),
                'verbose_name': '\u0442\u0435\u043a\u0441\u0442\u043e\u0432\u0430\u044f \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0430',
                'verbose_name_plural': '\u0442\u0435\u043a\u0441\u0442\u043e\u0432\u044b\u0435 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='action',
            name='group',
            field=models.ForeignKey(related_name='actions', verbose_name='Group', to='simplepage.ActionGroup'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='action',
            name='page',
            field=models.ForeignKey(related_name='actions', verbose_name='Page', blank=True, to='simplepage.Page', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='action',
            name='parent',
            field=models.ForeignKey(verbose_name='Parent', blank=True, to='simplepage.Action', null=True),
            preserve_default=True,
        ),
    ]
