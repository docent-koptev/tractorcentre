# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('simplepage', '0004_auto_20150304_1140'),
    ]

    operations = [
        migrations.AddField(
            model_name='action',
            name='target',
            field=models.BooleanField(default=False, verbose_name='\u0412 \u043d\u043e\u0432\u043e\u0439 \u0432\u043a\u043b\u0430\u0434\u043a\u0435'),
            preserve_default=True,
        ),
    ]
