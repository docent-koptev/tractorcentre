# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import autoslug.fields


class Migration(migrations.Migration):

    dependencies = [
        ('simplepage', '0003_actiongroup_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='actiongroup',
            name='slug',
            field=autoslug.fields.AutoSlugField(default=1, unique=True, max_length=255, verbose_name='Slug'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='actiongroup',
            name='description',
            field=models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='actiongroup',
            name='name',
            field=models.CharField(unique=True, max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
    ]
