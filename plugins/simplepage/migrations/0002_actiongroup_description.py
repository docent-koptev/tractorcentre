# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('simplepage', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='actiongroup',
            name='description',
            field=models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u043c\u0435\u043d\u044e', blank=True),
            preserve_default=True,
        ),
    ]
