from django.conf.urls import *

urlpatterns = patterns('plugins.simplepage.views',
    url(r'(?P<slug>[-\w]*)$', "page_view", name="page_view"),
)