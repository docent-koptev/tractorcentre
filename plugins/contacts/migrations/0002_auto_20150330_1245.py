# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Manager',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fio', models.CharField(max_length=200, verbose_name='\u0424\u0418\u041e')),
                ('worker', models.CharField(max_length=200, verbose_name='\u0414\u043e\u043b\u0436\u043d\u043e\u0441\u0442\u044c')),
                ('vector', models.CharField(max_length=200, verbose_name='\u041d\u0430\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u0435', blank=True)),
                ('email', models.EmailField(max_length=200, verbose_name='\u041f\u043e\u0447\u0442\u0430', blank=True)),
                ('active', models.BooleanField(default=True, help_text='\u0420\u0430\u0431\u043e\u0442\u0430\u0435\u0442 \u043b\u0438 \u0435\u0449\u0435 \u043c\u0435\u043d\u0435\u0434\u0436\u0435\u0440', verbose_name='\u0410\u043a\u0442\u0438\u0432\u0435\u043d')),
            ],
            options={
                'verbose_name': '\u041c\u0435\u043d\u0435\u0434\u0436\u0435\u0440',
                'verbose_name_plural': '\u041c\u0435\u043d\u0435\u0434\u0436\u0435\u0440\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ManagerPhone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.CharField(max_length=200, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0430')),
                ('manager', models.ForeignKey(to='contacts.Manager')),
            ],
            options={
                'verbose_name': '\u0422\u0435\u043b\u0435\u0444\u043e\u043d',
                'verbose_name_plural': '\u0422\u0435\u043b\u0435\u0444\u043e\u043d\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Office',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043e\u0442\u0434\u0435\u043b\u0430')),
                ('position', models.IntegerField(default=1000, verbose_name='Position')),
            ],
            options={
                'verbose_name': '\u041e\u0442\u0434\u0435\u043b',
                'verbose_name_plural': '\u041e\u0442\u0434\u0435\u043b\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='manager',
            name='office',
            field=models.ManyToManyField(to='contacts.Office', verbose_name='\u041e\u0442\u0434\u0435\u043b'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='manager',
            name='shop',
            field=models.ForeignKey(verbose_name='\u041c\u0430\u0433\u0430\u0437\u0438\u043d', to='contacts.ShopAddress', help_text='\u0413\u0434\u0435 \u0440\u0430\u0431\u043e\u0442\u0430\u0435\u0442 \u043c\u0435\u043d\u0435\u0434\u0436\u0435\u0440'),
            preserve_default=True,
        ),
        migrations.AlterModelOptions(
            name='shopcity',
            options={'verbose_name': '\u0413\u043e\u0440\u043e\u0434', 'verbose_name_plural': '\u0413\u043e\u0440\u043e\u0434\u0430'},
        ),
        migrations.AlterModelOptions(
            name='shopphone',
            options={'verbose_name': '\u0422\u0435\u043b\u0435\u0444\u043e\u043d', 'verbose_name_plural': '\u0422\u0435\u043b\u0435\u0444\u043e\u043d\u044b'},
        ),
        migrations.AlterModelOptions(
            name='shopregion',
            options={'verbose_name': '\u041e\u0431\u043b\u0430\u0441\u0442\u044c', 'verbose_name_plural': '\u041e\u0431\u043b\u0430\u0441\u0442\u0438'},
        ),
    ]
