# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0006_shopaddress_details_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='manager',
            name='phone',
            field=models.CharField(max_length=200, verbose_name='\u041b\u0438\u0447\u043d\u044b\u0439 \u0442\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
            preserve_default=True,
        ),
    ]
