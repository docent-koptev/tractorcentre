# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0003_shopcity_region'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shopaddress',
            name='region',
        ),
    ]
