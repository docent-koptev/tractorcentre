# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0002_auto_20150330_1245'),
    ]

    operations = [
        migrations.AddField(
            model_name='shopcity',
            name='region',
            field=models.ForeignKey(default=1, verbose_name='\u041e\u0431\u043b\u0430\u0441\u0442\u044c', to='contacts.ShopRegion'),
            preserve_default=False,
        ),
    ]
