# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ShopAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('shop_name', models.CharField(max_length=100, verbose_name='\u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u043c\u0430\u0433\u0430\u0437\u0438\u043d\u0430')),
                ('address', models.CharField(help_text='\u0424\u043e\u0440\u043c\u0430\u0442 \u0430\u0434\u0440\u0435\u0441\u0430: \u041a\u0440\u0430\u0441\u043d\u0430\u044f \u043f\u043b\u043e\u0449\u0430\u0434\u044c, 3, \u0413\u0423\u041c, 3-\u044f \u043b\u0438\u043d\u0438\u044f, 3-\u0439 \u044d\u0442\u0430\u0436, 109012 - \u041c\u043e\u0441\u043a\u0432\u0430', max_length=300, verbose_name='\u0430\u0434\u0440\u0435\u0441')),
                ('email', models.EmailField(max_length=200, null=True, verbose_name='\u041f\u043e\u0447\u0442\u0430', blank=True)),
                ('details', models.FileField(upload_to=b'details_pdf', null=True, verbose_name='\u0420\u0435\u043a\u0432\u0438\u0437\u0438\u0442\u044b \u0432 pdf', blank=True)),
                ('mapx', models.CharField(max_length=100, null=True, verbose_name='\u0425-\u043a\u043e\u043e\u0440\u0434\u0438\u043d\u0430\u0442\u0430', blank=True)),
                ('mapy', models.CharField(max_length=100, null=True, verbose_name='Y-\u043a\u043e\u043e\u0440\u0434\u0438\u043d\u0430\u0442\u0430', blank=True)),
            ],
            options={
                'verbose_name': '\u0410\u0434\u0440\u0435\u0441 \u043c\u0430\u0433\u0430\u0437\u0438\u043d\u0430',
                'verbose_name_plural': '\u0410\u0434\u0440\u0435\u0441\u0430 \u043c\u0430\u0433\u0430\u0437\u0438\u043d\u043e\u0432',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ShopCity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='\u0413\u043e\u0440\u043e\u0434')),
                ('active', models.BooleanField(default=True, help_text='\u0414\u043b\u044f \u0432\u043a\u043b\u044e\u0447\u0435\u043d\u0438\u044f/\u0432\u044b\u043a\u043b\u044e\u0447\u0435\u043d\u0438\u044f \u0433\u043e\u0440\u043e\u0434\u0430', verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u0430')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ShopPhone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.CharField(max_length=200, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0430')),
                ('shop', models.ForeignKey(to='contacts.ShopAddress')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ShopRegion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='\u041e\u0431\u043b\u0430\u0441\u0442\u044c')),
                ('active', models.BooleanField(default=True, help_text='\u0414\u043b\u044f \u0432\u043a\u043b\u044e\u0447\u0435\u043d\u0438\u044f/\u0432\u044b\u043a\u043b\u044e\u0447\u0435\u043d\u0438\u044f \u043e\u0431\u043b\u0430\u0441\u0442\u0435\u0439', verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u0430')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='shopaddress',
            name='city',
            field=models.ForeignKey(verbose_name='\u0413\u043e\u0440\u043e\u0434', to='contacts.ShopCity'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='shopaddress',
            name='region',
            field=models.ForeignKey(verbose_name='\u041e\u0431\u043b\u0430\u0441\u0442\u044c', to='contacts.ShopRegion'),
            preserve_default=True,
        ),
    ]
