# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0004_remove_shopaddress_region'),
    ]

    operations = [
        migrations.AddField(
            model_name='shopaddress',
            name='is_main',
            field=models.BooleanField(default=False, help_text='\u041f\u043e\u043a\u0430\u0437 \u043f\u0435\u0440\u0432\u044b\u043c \u0432 \u043a\u043e\u043e\u0440\u0434\u0438\u043d\u0430\u0442\u0430\u0445', verbose_name='\u0413\u043b\u0430\u0432\u043d\u044b\u0439 \u043c\u0430\u0433\u0430\u0437\u0438\u043d'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shopaddress',
            name='city',
            field=models.ForeignKey(verbose_name='\u0413\u043e\u0440\u043e\u0434,\u043e\u0431\u043b\u0430\u0441\u0442\u044c', to='contacts.ShopCity'),
            preserve_default=True,
        ),
    ]
