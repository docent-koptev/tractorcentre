# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0008_auto_20150331_1540'),
    ]

    operations = [
        migrations.AddField(
            model_name='manager',
            name='position',
            field=models.IntegerField(default=1000, verbose_name='Position'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shopaddress',
            name='unic_code',
            field=models.PositiveSmallIntegerField(default=1, help_text='\u0423\u043d\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u043a\u043e\u0434 \u0434\u043b\u044f \u0438\u043d\u0442\u0435\u0440\u0430\u043a\u0442\u0438\u0432\u043d\u043e\u0439 \u043a\u0430\u0440\u0442\u044b', verbose_name='\u041a\u043e\u0434 \u0434\u043b\u044f \u043a\u0430\u0440\u0442\u044b'),
            preserve_default=True,
        ),
    ]
