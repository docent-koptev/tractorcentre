# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0007_manager_phone'),
    ]

    operations = [
        migrations.AddField(
            model_name='shopaddress',
            name='unic_code',
            field=models.PositiveSmallIntegerField(default=1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shopaddress',
            name='city',
            field=models.ForeignKey(verbose_name='\u0413\u043e\u0440\u043e\u0434', to='contacts.ShopCity'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shopaddress',
            name='mapx',
            field=models.CharField(help_text='\u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c http://www.mapcoordinates.net/ru', max_length=100, null=True, verbose_name='\u0425-\u043a\u043e\u043e\u0440\u0434\u0438\u043d\u0430\u0442\u0430', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='shopaddress',
            name='mapy',
            field=models.CharField(help_text='\u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c http://www.mapcoordinates.net/ru', max_length=100, null=True, verbose_name='Y-\u043a\u043e\u043e\u0440\u0434\u0438\u043d\u0430\u0442\u0430', blank=True),
            preserve_default=True,
        ),
    ]
