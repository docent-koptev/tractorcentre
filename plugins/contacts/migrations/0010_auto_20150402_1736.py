# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0009_auto_20150402_1725'),
    ]

    operations = [
        migrations.RenameField(
            model_name='manager',
            old_name='position',
            new_name='man_position',
        ),
    ]
