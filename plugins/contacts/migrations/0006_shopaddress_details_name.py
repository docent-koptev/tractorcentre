# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0005_auto_20150330_1407'),
    ]

    operations = [
        migrations.AddField(
            model_name='shopaddress',
            name='details_name',
            field=models.CharField(max_length=100, null=True, verbose_name='\u043d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0434\u043e\u043a\u0443\u043c\u0435\u043d\u0442\u0430', blank=True),
            preserve_default=True,
        ),
    ]
