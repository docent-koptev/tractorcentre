# -*- coding: utf-8 -*-
from django.http import Http404
from django.shortcuts import get_object_or_404,HttpResponse
from annoying.decorators import render_to
from plugins.contacts.models import *
from django.utils.translation import ugettext_lazy as _
from datetime import datetime
import json

from annoying.functions import get_object_or_None
from utils.helpers import Paginate

@render_to('contacts.html')
def contacts(request):
    if request.is_ajax():
        if 'contact_city' in request.GET and 'contact_region' in request.GET:
            pass
        else:
            if 'contact_region' in request.GET:
                region = get_object_or_None(ShopRegion,pk=int(request.GET.get('contact_region')))
                city_list = ShopCity.objects.filter(region=region)
                city_dict = {}
                for city in city_list:
                    city_dict[city.pk] = city.name

                response = {'status':0,'city_dict':city_dict}
        return HttpResponse(json.dumps(response))

    regions = ShopRegion.objects.filter(active = True)
    if request.POST:
        contact_city = get_object_or_None(ShopCity,pk=int(request.POST.get("contact_city")))
        if contact_city is not None:
            region = get_object_or_None(ShopRegion,pk=int(request.POST.get('contact_region')))
            shop = ShopAddress.objects.get(city=contact_city)
            city = ShopCity.objects.filter(active = True,region=region)
    else:
        shop = ShopAddress.objects.filter(is_main = True)[0]
        region = shop.city.region
        city = ShopCity.objects.filter(active = True,region=region)

    managers = Manager.objects.filter(shop=shop)
    office_pk = []
    for manager in managers:
        for office in manager.office.all():
            if office.pk not in office_pk:
                office_pk.append(office.pk)

    if len(office_pk) > 0:
        offices = Office.objects.filter(pk__in=office_pk)

    return locals()

