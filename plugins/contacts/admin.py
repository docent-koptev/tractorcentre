#-*- coding: utf-8 -*-
# django imports
from django.contrib import admin
from django.contrib.auth.decorators import user_passes_test
from django import forms, template
from django.conf.urls import *
from django.contrib.contenttypes.models import ContentType
from django.forms.widgets import Textarea
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse, resolve
from django.conf import settings

from models import *

class ShopPhoneInline(admin.TabularInline):
    model = ShopPhone
    extra = 1

class ManagerPhoneInline(admin.TabularInline):
    model = ManagerPhone
    extra = 1

class ShopAddressAdmin(admin.ModelAdmin):
    inlines = [ShopPhoneInline,]
    list_display = ('shop_name','email','mapx','mapy','is_main','address','unic_code',)
    readonly_fields = ('is_main',)
    list_editable = ('mapx','mapy','address','unic_code',)
    fieldsets = (
        ('Основное',{
            'fields': ('shop_name','is_main',)
        }),

        ('Координаты',{
            'fields': ('city','address','email','mapx','mapy',)
        }),

        ('Дополнительно',{
            'fields': ('details','details_name',)
        }),
    )

class ManagerAdmin(admin.ModelAdmin):
	inlines = [ManagerPhoneInline,]
	list_display = ('fio','worker','vector','email','active','man_position',)
	list_filter = ('worker',)
	filter_horizontal = ('office',)
    
	fieldsets = (
        ('Основное',{
            'fields': ('shop','fio','email','phone',)
        }),

        ('Где работает и кем',{
            'fields': ('worker','vector','office',)
        }),

        ('',{
            'fields': ('active','man_position',)
        }),
    )

class OfficeAdmin(admin.ModelAdmin):
	list_display = ('name','position',)
	list_editable = ('name','position',)

admin.site.register(ShopPhone)
admin.site.register(ShopRegion)
admin.site.register(ShopCity)
admin.site.register(ShopAddress,ShopAddressAdmin)
admin.site.register(Manager,ManagerAdmin)
admin.site.register(Office,OfficeAdmin)
admin.site.register(ManagerPhone)