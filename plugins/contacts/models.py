# -*- coding: utf-8 -*-
import re

# django imports
from django.contrib.auth.models import User
from django.core.cache import cache
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.core.urlresolvers import reverse

class ShopRegion(models.Model):
	name = models.CharField(u'Область', max_length=100)
	active = models.BooleanField(u'Активна',default=True,help_text=u'Для включения/выключения областей')

	class Meta:
		verbose_name = u'Область'
		verbose_name_plural = u'Области'
	
	def __unicode__(self):
		return self.name


class ShopCity(models.Model):
	name = models.CharField(u'Город', max_length=100)
	region = models.ForeignKey(ShopRegion,verbose_name=u'Область')
	active = models.BooleanField(u'Активна',default=True,help_text=u'Для включения/выключения города')

	class Meta:
		verbose_name = u'Город'
		verbose_name_plural = u'Города'

	def __unicode__(self):
		return "%s-%s" % (self.name, self.region.name)

class ShopAddress(models.Model):
	shop_name = models.CharField(u'название магазина', max_length=100)
	#region = models.ForeignKey(ShopRegion,verbose_name=u'Область')
	city = models.ForeignKey(ShopCity,verbose_name=u'Город')
	address = models.CharField(u'адрес', max_length=300, help_text=u'Формат адреса: Красная площадь, 3, ГУМ, 3-я линия, 3-й этаж, 109012 - Москва')
	email = models.EmailField(u'Почта',blank=True,null=True,max_length=200)
	details = models.FileField(u'Реквизиты в pdf',upload_to='details_pdf',blank=True,null=True)
	details_name = models.CharField(u'название документа',blank=True,null=True,max_length=100)
	mapx = models.CharField(u'Х-координата',blank=True,null=True,max_length=100)
	mapy = models.CharField(u'Y-координата',blank=True,null=True,max_length=100)
	is_main = models.BooleanField(u'Главный магазин',default=False,help_text=u'Показ первым в координатах')
	unic_code = models.PositiveSmallIntegerField(default=1,verbose_name=u'Код для карты',help_text=u'Уникальный код для интерактивной карты')
	#place = models.ForeignKey('Place', verbose_name=u'Город')

	def __unicode__(self):
		return unicode(self.shop_name)


	def get_shop_phones(self):
		phones = ShopPhone.objects.filter(shop=self).order_by("pk")
		return phones

	class Meta:
		verbose_name = u'Адрес магазина'
		verbose_name_plural = u'Адреса магазинов'

class ShopPhone(models.Model):
	shop = models.ForeignKey(ShopAddress)
	number = models.CharField(u'Номер телефона',max_length=200)

	class Meta:
		verbose_name = u'Телефон'
		verbose_name_plural = u'Телефоны'

	def __unicode__(self):
		return self.number

class Office(models.Model):
	name = models.CharField(u'Название отдела', max_length=100)
	position = models.IntegerField(_(u"Position"), default=1000)

	class Meta:
		verbose_name = u'Отдел'
		verbose_name_plural = u'Отделы'

	def __unicode__(self):
		return self.name

class Manager(models.Model):
	shop = models.ForeignKey(ShopAddress,verbose_name=u'Магазин',help_text=u'Где работает менеджер')
	fio = models.CharField(u'ФИО',max_length=200)
	worker = models.CharField(u'Должность',max_length=200)
	vector = models.CharField(u'Направление',max_length=200,blank=True)
	email = models.EmailField(u'Почта',max_length=200,blank=True)
	active = models.BooleanField(u'Активен',default=True,help_text=u'Работает ли еще менеджер')
	phone = models.CharField(u'Личный телефон',max_length=200,blank=True)
	office = models.ManyToManyField(Office,verbose_name=u'Отдел')
	man_position = models.IntegerField(_(u"Position"), default=1000)

	class Meta:
		verbose_name = u'Менеджер'
		verbose_name_plural = u'Менеджеры'

	def __unicode__(self):
		return self.fio

	def get_manager_phones(self):
		phones = ManagerPhone.objects.filter(manager = self)
		return phones


class ManagerPhone(models.Model):
	manager = models.ForeignKey(Manager)
	number = models.CharField(u'Номер телефона',max_length=200)

	class Meta:
		verbose_name = u'Телефон'
		verbose_name_plural = u'Телефоны'

	def __unicode__(self):
		return self.number