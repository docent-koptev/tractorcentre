# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0003_auto_20150328_0103'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='beznal',
        ),
    ]
