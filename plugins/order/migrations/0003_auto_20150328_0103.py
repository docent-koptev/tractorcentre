# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0002_auto_20150327_1703'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='samovivoz',
        ),
        migrations.RemoveField(
            model_name='order',
            name='transport',
        ),
    ]
