# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0006_auto_20150328_0137'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='file',
            field=models.FileField(upload_to=b'documents_upload', null=True, verbose_name='\u0424\u0430\u0439\u043b', blank=True),
            preserve_default=True,
        ),
    ]
