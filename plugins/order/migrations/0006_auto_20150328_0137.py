# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0005_auto_20150328_0109'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='dop_phone',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0414\u043e\u043f.\u0442\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='phone',
            field=models.CharField(max_length=200, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='samovivoz_addr',
            field=models.CharField(max_length=200, null=True, verbose_name='\u041e\u0442\u043a\u0443\u0434\u0430 \u0437\u0430\u0431\u0440\u0430\u0442\u044c', blank=True),
            preserve_default=True,
        ),
    ]
