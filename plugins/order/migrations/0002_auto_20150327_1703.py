# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='account_number',
        ),
        migrations.RemoveField(
            model_name='order',
            name='bank_identification_code',
        ),
        migrations.RemoveField(
            model_name='order',
            name='bank_name',
        ),
        migrations.RemoveField(
            model_name='order',
            name='customer_email',
        ),
        migrations.RemoveField(
            model_name='order',
            name='customer_firstname',
        ),
        migrations.RemoveField(
            model_name='order',
            name='customer_lastname',
        ),
        migrations.RemoveField(
            model_name='order',
            name='depositor',
        ),
        migrations.RemoveField(
            model_name='order',
            name='invoice_address',
        ),
        migrations.RemoveField(
            model_name='order',
            name='invoice_city',
        ),
        migrations.RemoveField(
            model_name='order',
            name='invoice_company_name',
        ),
        migrations.RemoveField(
            model_name='order',
            name='invoice_country',
        ),
        migrations.RemoveField(
            model_name='order',
            name='invoice_firstname',
        ),
        migrations.RemoveField(
            model_name='order',
            name='invoice_lastname',
        ),
        migrations.RemoveField(
            model_name='order',
            name='invoice_phone',
        ),
        migrations.RemoveField(
            model_name='order',
            name='invoice_state',
        ),
        migrations.RemoveField(
            model_name='order',
            name='invoice_street',
        ),
        migrations.RemoveField(
            model_name='order',
            name='invoice_zip_code',
        ),
        migrations.RemoveField(
            model_name='order',
            name='message',
        ),
        migrations.RemoveField(
            model_name='order',
            name='pay_link',
        ),
        migrations.RemoveField(
            model_name='order',
            name='payment_method',
        ),
        migrations.RemoveField(
            model_name='order',
            name='requested_delivery_date',
        ),
        migrations.RemoveField(
            model_name='order',
            name='shipping_address',
        ),
        migrations.RemoveField(
            model_name='order',
            name='shipping_city',
        ),
        migrations.RemoveField(
            model_name='order',
            name='shipping_company_name',
        ),
        migrations.RemoveField(
            model_name='order',
            name='shipping_country',
        ),
        migrations.RemoveField(
            model_name='order',
            name='shipping_firstname',
        ),
        migrations.RemoveField(
            model_name='order',
            name='shipping_lastname',
        ),
        migrations.RemoveField(
            model_name='order',
            name='shipping_phone',
        ),
        migrations.RemoveField(
            model_name='order',
            name='shipping_price',
        ),
        migrations.RemoveField(
            model_name='order',
            name='shipping_state',
        ),
        migrations.RemoveField(
            model_name='order',
            name='shipping_street',
        ),
        migrations.RemoveField(
            model_name='order',
            name='shipping_zip_code',
        ),
        migrations.RemoveField(
            model_name='order',
            name='uuid',
        ),
        migrations.AddField(
            model_name='order',
            name='beznal',
            field=models.BooleanField(default=False, verbose_name='\u0411\u0435\u0437\u043d\u0430\u043b\u0438\u0447\u043d\u044b\u0439 \u0440\u0430\u0441\u0447\u0435\u0442'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='beznal_inn',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0418\u041d\u041d', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='beznal_kpp',
            field=models.CharField(max_length=200, null=True, verbose_name='\u041a\u041f\u041f', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='beznal_organization',
            field=models.CharField(max_length=200, null=True, verbose_name='\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u044f', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='dop_phone',
            field=models.EmailField(max_length=200, null=True, verbose_name='\u0414\u043e\u043f.\u0442\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='email',
            field=models.EmailField(default=1, max_length=200, verbose_name='Email'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='fio',
            field=models.CharField(default=1, max_length=200, verbose_name='\u0424\u0418\u041e'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='for_us',
            field=models.TextField(null=True, verbose_name='\u0421\u043e\u043e\u0449\u0435\u043d\u0438\u0435 \u043d\u0430\u043c', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='payd_nal',
            field=models.BooleanField(default=False, verbose_name='\u041e\u043f\u043b\u0430\u0442\u0430 \u043d\u0430\u043b\u0438\u0447\u043d\u044b\u043c\u0438'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='phone',
            field=models.EmailField(default=1, max_length=200, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='order',
            name='samovivoz',
            field=models.BooleanField(default=False, verbose_name='\u0421\u0430\u043c\u043e\u0432\u044b\u0432\u043e\u0437'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='samovivoz_addr',
            field=models.EmailField(max_length=200, null=True, verbose_name='\u041e\u0442\u043a\u0443\u0434\u0430 \u0437\u0430\u0431\u0440\u0430\u0442\u044c', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='transport',
            field=models.BooleanField(default=False, verbose_name='\u0414\u043e\u0441\u0442\u0430\u0432\u0438\u0442\u044c \u0442\u0440\u0430\u043d\u0441\u043f\u043e\u0440\u0442\u043d\u043e\u0439 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0435\u0439'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='transport_city',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0413\u043e\u0440\u043e\u0434', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='transport_corpus',
            field=models.CharField(max_length=200, null=True, verbose_name='\u041a\u043e\u0440\u043f\u0443\u0441', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='transport_house',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0414\u043e\u043c', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='transport_office',
            field=models.CharField(max_length=200, null=True, verbose_name='\u041e\u0444\u0438\u0441', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='transport_region',
            field=models.CharField(max_length=200, null=True, verbose_name='\u041e\u0431\u043b\u0430\u0441\u0442\u044c', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='order',
            name='transport_stroenie',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0421\u0442\u0440\u043e\u0435\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
    ]
