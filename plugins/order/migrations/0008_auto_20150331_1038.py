# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0007_order_file'),
    ]

    operations = [
        migrations.RenameField(
            model_name='orderitem',
            old_name='price_gross',
            new_name='price',
        ),
        migrations.RenameField(
            model_name='orderitem',
            old_name='product_price_net',
            new_name='product_price',
        ),
        migrations.RemoveField(
            model_name='orderitem',
            name='price_net',
        ),
        migrations.RemoveField(
            model_name='orderitem',
            name='product_price_gross',
        ),
        migrations.AlterField(
            model_name='order',
            name='message',
            field=models.TextField(null=True, verbose_name='\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435 \u043d\u0430\u043c', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='samovivoz_addr',
            field=models.CharField(help_text='\u0412 \u0441\u043b\u0443\u0447\u0430\u0435 \u0441\u0430\u043c\u043e\u0432\u044b\u0432\u043e\u0437\u0430', max_length=200, null=True, verbose_name='\u041e\u0442\u043a\u0443\u0434\u0430 \u0437\u0430\u0431\u0440\u0430\u0442\u044c', blank=True),
            preserve_default=True,
        ),
    ]
