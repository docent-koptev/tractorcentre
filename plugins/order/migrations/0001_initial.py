# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0016_auto_20150319_1855'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('core', '0002_shopaddress'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('session', models.CharField(max_length=100, verbose_name='Session', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('state', models.PositiveSmallIntegerField(default=0, verbose_name='State', choices=[(0, 'Submitted'), (1, 'Paid'), (2, 'Sent'), (3, 'Closed'), (4, 'Canceled'), (5, 'Payment Failed'), (6, 'Payment Flagged')])),
                ('state_modified', models.DateTimeField(auto_now_add=True, verbose_name='State modified')),
                ('price', models.FloatField(default=0.0, verbose_name='Price')),
                ('customer_firstname', models.CharField(max_length=50, verbose_name='firstname')),
                ('customer_lastname', models.CharField(max_length=50, verbose_name='lastname')),
                ('customer_email', models.CharField(max_length=50, verbose_name='email')),
                ('invoice_firstname', models.CharField(max_length=50, verbose_name='Invoice firstname')),
                ('invoice_lastname', models.CharField(max_length=50, verbose_name='Invoice lastname')),
                ('invoice_company_name', models.CharField(max_length=50, verbose_name='Invoice company name', blank=True)),
                ('invoice_street', models.CharField(max_length=100, verbose_name='Invoice street', blank=True)),
                ('invoice_zip_code', models.CharField(max_length=10, verbose_name='Invoice zip code')),
                ('invoice_city', models.CharField(max_length=50, verbose_name='Invoice city')),
                ('invoice_state', models.CharField(max_length=50, verbose_name='Invoice state')),
                ('invoice_phone', models.CharField(max_length=20, verbose_name='Invoice phone', blank=True)),
                ('invoice_address', models.TextField(verbose_name='\u0410\u0434\u0440\u0435\u0441\u0441 \u043f\u043b\u0430\u0442\u0435\u043b\u044c\u0449\u0438\u043a\u0430', blank=True)),
                ('shipping_firstname', models.CharField(max_length=50, verbose_name='Shipping firstname')),
                ('shipping_lastname', models.CharField(max_length=50, verbose_name='Shipping lastname')),
                ('shipping_company_name', models.CharField(max_length=50, verbose_name='Shipping company name', blank=True)),
                ('shipping_street', models.CharField(max_length=100, verbose_name='Shipping street', blank=True)),
                ('shipping_zip_code', models.CharField(max_length=10, verbose_name='Shipping zip code')),
                ('shipping_city', models.CharField(max_length=50, verbose_name='Shipping city')),
                ('shipping_state', models.CharField(max_length=50, verbose_name='Shipping state')),
                ('shipping_phone', models.CharField(max_length=20, verbose_name='Shipping phone', blank=True)),
                ('shipping_address', models.TextField(verbose_name='\u0410\u0434\u0440\u0435\u0441\u0441 \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438', blank=True)),
                ('shipping_price', models.FloatField(default=0.0, verbose_name='Shipping Price')),
                ('payment_method', models.PositiveSmallIntegerField(default=0, verbose_name='\u0421\u043f\u043e\u0441\u043e\u0431 \u043e\u043f\u043b\u0430\u0442\u044b', choices=[(0, '\u041d\u0430\u043b\u0438\u0447\u043d\u044b\u043c\u0438'), (2, '\u0411\u0435\u0437\u043d\u0430\u043b\u0438\u0447\u043d\u043e \u0447\u0435\u0440\u0435\u0437 \u0440\u043e\u0431\u043e\u043a\u0430\u0441\u0441\u0443 (+5% \u043a \u0441\u0442\u043e\u0438\u043c\u043e\u0441\u0442\u0438 \u0437\u0430\u043a\u0430\u0437\u0430)')])),
                ('account_number', models.CharField(max_length=30, verbose_name='Account number', blank=True)),
                ('bank_identification_code', models.CharField(max_length=30, verbose_name='Bank identication code', blank=True)),
                ('bank_name', models.CharField(max_length=100, verbose_name='Bank name', blank=True)),
                ('depositor', models.CharField(max_length=100, verbose_name='Depositor', blank=True)),
                ('message', models.TextField(verbose_name='Message', blank=True)),
                ('pay_link', models.TextField(verbose_name='pay_link', blank=True)),
                ('uuid', models.CharField(max_length=50, verbose_name='Id \u043d\u0430 moysklad', blank=True)),
                ('requested_delivery_date', models.DateTimeField(null=True, verbose_name='Delivery Date', blank=True)),
                ('invoice_country', models.ForeignKey(related_name='orders_invoice_country', blank=True, to='core.Country', null=True)),
                ('shipping_country', models.ForeignKey(related_name='orders_shipping_country', blank=True, to='core.Country', null=True)),
                ('user', models.ForeignKey(verbose_name='User', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('-created',),
                'verbose_name': '\u0437\u0430\u043a\u0430\u0437',
                'verbose_name_plural': '\u0437\u0430\u043a\u0430\u0437\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price_net', models.FloatField(default=0.0, verbose_name='Price net')),
                ('price_gross', models.FloatField(default=0.0, verbose_name='Price gross')),
                ('product_amount', models.FloatField(null=True, verbose_name='Product quantity', blank=True)),
                ('product_sku', models.CharField(max_length=100, verbose_name='Product SKU', blank=True)),
                ('product_name', models.CharField(max_length=255, verbose_name='Product name', blank=True)),
                ('product_price_net', models.FloatField(default=0.0, verbose_name='Product price net')),
                ('product_price_gross', models.FloatField(default=0.0, verbose_name='Product price gross')),
                ('order', models.ForeignKey(related_name='items', to='order.Order')),
                ('product', models.ForeignKey(blank=True, to='catalog.Product', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrderItemPropertyValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=100, verbose_name=b'Value', blank=True)),
                ('order_item', models.ForeignKey(related_name='properties', verbose_name='Order item', to='order.OrderItem')),
                ('property', models.ForeignKey(verbose_name='Property', to='catalog.Property')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
