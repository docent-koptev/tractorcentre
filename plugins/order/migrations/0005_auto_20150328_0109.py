# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0004_remove_order_beznal'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='for_us',
            new_name='message',
        ),
    ]
