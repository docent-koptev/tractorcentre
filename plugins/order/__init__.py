from robokassa.signals import result_received, fail_page_visited
from models import Order
from settings import PAID, PAYMENT_FAILED

def payment_received(sender, **kwargs):
    order = Order.objects.get(id=kwargs['InvId'])
    order.state = PAID
    order.save()
result_received.connect(payment_received)

def payment_failed(sender, **kwargs):
    order = Order.objects.get(id=kwargs['InvId'])
    order.state = PAYMENT_FAILED
    order.save()

fail_page_visited.connect(payment_failed)