# -*- coding: utf-8 -*-
# django imports
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.utils.dateformat import format
from plugins.checkout.settings import PAYMENT_METHOD, PAYMENT_METHOD_CASH,  PAYMENT_METHOD_CASHLESS_ROBOKASSA

from plugins.catalog.models import Product, Property, PropertyOption
from plugins.customer.utils import get_customer_by_user_or_session
from plugins.order.settings import ORDER_STATES, SUBMITTED

# other imports
import uuid

def get_unique_id_str():
    return str(uuid.uuid4())

class Order(models.Model):
    user = models.ForeignKey(User, verbose_name=_(u"User"), blank=True, null=True)
    session = models.CharField(_(u"Session"), blank=True, max_length=100)

    created = models.DateTimeField(_(u"Created"), auto_now_add=True)

    state = models.PositiveSmallIntegerField(_(u"State"), choices=ORDER_STATES, default=SUBMITTED)
    state_modified = models.DateTimeField(_(u"State modified"), auto_now_add=True)

    price = models.FloatField(_(u"Price"), default=0.0)

    #данные о покупателе
    fio = models.CharField(u"ФИО", max_length=200)
    email = models.EmailField(u"Email", max_length=200)
    phone = models.CharField(u"Телефон", max_length=200)
    dop_phone = models.CharField(u"Доп.телефон", max_length=200,blank=True,null=True)

    #Доставка самовывоз
    samovivoz_addr = models.CharField(u"Откуда забрать", max_length=200,blank=True,null=True,help_text=u'В случае самовывоза')

    #Доставка ТК
    transport_region = models.CharField(u"Область", max_length=200,blank=True,null=True)
    transport_city = models.CharField(u"Город", max_length=200,blank=True,null=True)
    transport_house = models.CharField(u"Дом", max_length=200,blank=True,null=True)
    transport_corpus= models.CharField(u"Корпус", max_length=200,blank=True,null=True)
    transport_stroenie= models.CharField(u"Строение", max_length=200,blank=True,null=True)
    transport_office= models.CharField(u"Офис", max_length=200,blank=True,null=True)

    #Оплата наличными
    payd_nal = models.BooleanField(u'Оплата наличными',default=False)
    
    #Оплата по безналу
    beznal_organization = models.CharField(u"Организация", max_length=200,blank=True,null=True)
    beznal_inn = models.CharField(u"ИНН", max_length=200,blank=True,null=True)
    beznal_kpp = models.CharField(u"КПП", max_length=200,blank=True,null=True)

    #for us
    message = models.TextField(u'Сообщение нам',blank=True,null=True)

    file = models.FileField(u'Файл',upload_to='documents_upload',blank=True,null=True)


    class Meta:
        ordering = ("-created", )
        verbose_name = u"заказ"
        verbose_name_plural = u'заказы'

    def __unicode__(self):
        return "%s (%s %s)" % (self.created.strftime("%x %X"), self.fio, self.email)

    def custom_order_number(self):
        return format(self.created, 'my') + ("%(num)04d" % {'num': self.id})
    custom_order_number.short_description = u'номер заказа'

class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name="items")

    price = models.FloatField(_(u"Итоговая стоимость товара"), default=0.0)

    product = models.ForeignKey(Product, blank=True, null=True)

    product_amount = models.FloatField(_(u"Количество"), blank=True, null=True)
    product_sku = models.CharField(_(u"Product SKU"), blank=True, max_length=100)
    product_name = models.CharField(_(u"Product name"), blank=True, max_length=255)
    product_price = models.FloatField(_(u"Цена продукта"), default=0.0)

    def __unicode__(self):
        return "%s" % self.product_name

    @property
    def amount(self):
        return self.product_amount

    def save(self, **kwargs):
        product = self.product
        self.product_sku = product.get_sku()
        self.product_name = product.get_name()

        product_price = product.get_price()
        self.product_amount = self.product_amount or 1
        super(OrderItem, self).save(**kwargs)

    def cena_v_kopeikah(self):
        return self.product_price * 100


class OrderItemPropertyValue(models.Model):
    order_item = models.ForeignKey(OrderItem, verbose_name=_(u"Order item"), related_name="properties")
    property = models.ForeignKey(Property, verbose_name = _(u"Property"))
    value = models.CharField("Value", blank=True, max_length=100)
