from plugins.cart import utils as cart_utils
from plugins.checkout.settings import PAYMENT_METHOD_CASH
#from plugins.checkout.utils import get_result_order_sum
from plugins.customer import utils as customer_utils
from plugins.order.models import Order, OrderItem, OrderItemPropertyValue

from annoying.functions import get_object_or_None
from plugins.customer.models import CustomerAttributes
#from plugins.core.models import ShopAddress
from plugins.contacts.models import ShopAddress

def add_order(request):
    customer = customer_utils.get_customer(request)
    customer_attributes = get_object_or_None(CustomerAttributes,customer=customer)
    order = None

    cart = cart_utils.get_cart(request)
    if cart is None:
        return order
    cart_costs = cart_utils.get_cart_costs(request, cart, total=False)

    if request.user.is_authenticated():
        user = request.user

    
    order = Order.objects.create(
        user = user,
        session = request.session.session_key,
        price =  cart_costs["price"],

        fio = customer_attributes.fio,
        email = customer_attributes.email,
        phone = customer_attributes.phone,
        dop_phone = customer_attributes.dop_phone,
    )

    #delivery
    if 'samovivoz' in request.POST:
        samovivoz_reg = get_object_or_None(ShopAddress,pk=int(request.POST['samovivoz']))
        if samovivoz_reg is not None:
            samovivoz_addr = samovivoz_reg.shop_name
            order.samovivoz_addr = samovivoz_addr
            order.save()

            
    else:
        order.samovivoz_addr = "-"
        order.transport_region = request.POST.get("region","")
        order.transport_city = request.POST.get("city","")
        order.transport_street = request.POST.get("street","")
        order.transport_house = request.POST.get("building","")
        order.transport_corpus = request.POST.get("corpus","")
        order.transport_stroenie = request.POST.get("builder","")
        order.transport_office = request.POST.get("office","")

    #payd
    payment_method = request.POST.get('payment_method')
    if payment_method == 'nal':
        order.payd_nal = True

    else:
        order.payd_nal = False
        order.beznal_organization = request.POST.get("organization","")
        order.beznal_inn = request.POST.get("inn","")
        order.beznal_kpp = request.POST.get("kpp","")

    
    order.save()
    for cart_item in cart.cartitem_set.all():
        order_item = OrderItem.objects.create(
            order=order,
            product = cart_item.product,
            product_amount = cart_item.amount,
            product_sku = cart_item.product.sku,
            product_name = cart_item.product.name,
            product_price = cart_item.product.get_price(),
            price = cart_item.get_price(),
        )

    cart.delete()
    request.session["order"] = order

    return order
