# -*- coding: utf-8 -*-
# django imports
from django.contrib import admin
from models import *

class OrderItemInlines(admin.TabularInline):
    model = OrderItem
    extra = 0
    raw_id_fields = ('product',)
  #  readonly_fields = ('product_price_gross', 'price_gross')
  #  fields = ('product',  'product_price_gross', 'product_amount', 'price_gross',)

class OrderAdmin(admin.ModelAdmin):
	inlines = [OrderItemInlines,]
	list_display = ('fio','state','email','phone','dop_phone','price','samovivoz_addr','payd_nal','created','state_modified')
	'''readonly_fields = (
		'fio','email','phone','dop_phone','price','user',
		'samovivoz_addr','transport_region','transport_city','transport_house','transport_corpus' ,'transport_stroenie','transport_office',
		'payd_nal','beznal_organization','beznal_inn','beznal_kpp','file',
		'message'
		)'''
	fieldsets = (
		('Информация о заказе (статусы)',{
            'fields': ('state','price',)
        }),
        ('Информация о покупателе',{
            'fields': ('user','fio','email', ('phone','dop_phone'),)
        }),

        ('Информация о доставке или самовывозе',{
            'fields': ('samovivoz_addr','transport_region','transport_city',('transport_house','transport_corpus' ,'transport_stroenie','transport_office'),)
        }),
        ('Информация об оплате',{
            'fields': ('payd_nal','beznal_organization','beznal_inn','beznal_kpp','file' ,)
        }),
        ('Послание от клиентов',{
            'fields': ('message',)
        }),
    )
admin.site.register(Order,OrderAdmin)
admin.site.register(OrderItem)