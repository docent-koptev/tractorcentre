# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shipping', '0002_shopaddress'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='deliveryregion',
            name='region',
        ),
        migrations.RemoveField(
            model_name='deliveryregion',
            name='shippingmethod',
        ),
        migrations.RemoveField(
            model_name='shippingmethod',
            name='regions',
        ),
        migrations.DeleteModel(
            name='DeliveryRegion',
        ),
        migrations.DeleteModel(
            name='ShippingMethod',
        ),
    ]
