# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='DeliveryRegion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price', models.FloatField(default=0.0, verbose_name='Price')),
                ('region', models.ForeignKey(verbose_name='\u0420\u0435\u0433\u0438\u043e\u043d', to='core.Region')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ShippingMethod',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='Name')),
                ('description', models.TextField(help_text='\u0414\u043b\u044f \u0444\u043e\u0440\u043c\u0430\u0442\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u044f \u0442\u0435\u043a\u0441\u0442\u0430 \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u0442\u0441\u044f \u044f\u0437\u044b\u043a \u0440\u0430\u0437\u043c\u0435\u0442\u043a\u0438 <a href="http://ru.wikipedia.org/wiki/Markdown" target="_blank">markdown</a>', verbose_name='Description', blank=True)),
                ('note', models.TextField(help_text='\u0414\u043b\u044f \u0444\u043e\u0440\u043c\u0430\u0442\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u044f \u0442\u0435\u043a\u0441\u0442\u0430 \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u0443\u0435\u0442\u0441\u044f \u044f\u0437\u044b\u043a \u0440\u0430\u0437\u043c\u0435\u0442\u043a\u0438 <a href="http://ru.wikipedia.org/wiki/Markdown" target="_blank">markdown</a>', verbose_name='Note', blank=True)),
                ('priority', models.IntegerField(default=0, verbose_name='Priority')),
                ('image', models.ImageField(upload_to=b'images', null=True, verbose_name='Image', blank=True)),
                ('active', models.BooleanField(default=True, verbose_name='Active')),
                ('price', models.FloatField(default=0.0, verbose_name='Price')),
                ('regions', models.ManyToManyField(to='core.Region', verbose_name='\u0420\u0435\u0433\u0438\u043e\u043d\u044b \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438', through='shipping.DeliveryRegion')),
            ],
            options={
                'ordering': ('priority',),
                'verbose_name': '\u0441\u043f\u043e\u0441\u043e\u0431 \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438',
                'verbose_name_plural': '\u0441\u043f\u043e\u0441\u043e\u0431\u044b \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='deliveryregion',
            name='shippingmethod',
            field=models.ForeignKey(verbose_name='\u0421\u043f\u043e\u0441\u043e\u0431 \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438', to='shipping.ShippingMethod'),
            preserve_default=True,
        ),
    ]
