# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from models import *

class ShopAddressAdmin(admin.ModelAdmin):
    list_display = ('shop_name', 'address',)

admin.site.register(ShopAddress,ShopAddressAdmin)
