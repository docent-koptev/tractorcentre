
# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _

class ShopAddress(models.Model):
    shop_name = models.CharField(u'название магазина', max_length=100)
    address = models.CharField(u'адрес', max_length=300, help_text=u'Формат адреса: Красная площадь, 3, ГУМ, 3-я линия, 3-й этаж, 109012 - Москва')
    #place = models.ForeignKey('Place', verbose_name=u'Город')

    #def __unicode__(self):
        #return '%s - %s' % (self.shop_name, self.place)

    class Meta:
        verbose_name = u'Адрес магазина'
        verbose_name_plural = u'Адреса магазинов'