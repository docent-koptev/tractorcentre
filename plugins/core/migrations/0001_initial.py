# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0016_auto_20150319_1855'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=2, verbose_name='Country code')),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
            ],
            options={
                'ordering': ('name',),
                'verbose_name_plural': 'Countries',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, verbose_name='Name')),
                ('order', models.PositiveIntegerField(null=True, blank=True)),
            ],
            options={
                'ordering': ('name',),
                'verbose_name_plural': 'Regions',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Shop',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30, verbose_name='Name')),
                ('shop_owner', models.CharField(max_length=100, verbose_name='Shop owner', blank=True)),
                ('from_email', models.EmailField(max_length=75, verbose_name='From e-mail address')),
                ('notification_emails', models.TextField(verbose_name='Notification email addresses')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('image', sorl.thumbnail.fields.ImageField(upload_to=b'shops', null=True, verbose_name='Image', blank=True)),
                ('google_analytics_id', models.CharField(max_length=20, verbose_name='Google Analytics ID', blank=True)),
                ('ga_site_tracking', models.BooleanField(default=False, verbose_name='Google Analytics Site Tracking')),
                ('ga_ecommerce_tracking', models.BooleanField(default=False, verbose_name='Google Analytics E-Commerce Tracking')),
                ('default_currency', models.CharField(default=b'RUR', max_length=30, verbose_name='Default Currency')),
                ('confirm_toc', models.BooleanField(default=False)),
                ('active', models.BooleanField(default=True, editable=False)),
                ('static_block', models.ForeignKey(related_name='shops', verbose_name='Static block', blank=True, to='catalog.StaticBlock', null=True)),
                ('user', models.ForeignKey(related_name='shops', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': '\u041c\u0430\u0433\u0430\u0437\u0438\u043d',
                'verbose_name_plural': '\u041c\u0430\u0433\u0430\u0437\u0438\u043d\u044b',
                'permissions': (('manage_shop', 'Manage shop'),),
            },
            bases=(models.Model,),
        ),
    ]
