# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20150330_1133'),
    ]

    operations = [
        migrations.AddField(
            model_name='shop',
            name='vacancy_email',
            field=models.EmailField(max_length=75, verbose_name='\u041f\u043e\u0447\u0442\u0430 \u0434\u043b\u044f \u043f\u043e\u043b\u0443\u0447\u0435\u043d\u0438\u044f \u0440\u0435\u0437\u044e\u043c\u0435', blank=True),
            preserve_default=True,
        ),
    ]
