# -*- coding: utf-8 -*-
# django imports
from django.contrib import admin
# lfs imports
from plugins.core.models import *

class ShopAddressInline(admin.TabularInline):
    model = ShopAddress

class ShopAdmin(admin.ModelAdmin):
	def has_add_permission(self, request):
		return False
	list_display = ('name', 'from_email', 'notification_emails', 'shop_owner','default_currency')
	fieldsets = (
        ('Основная информация о магазине',{
            'fields': ('name', 'shop_owner','default_currency'
                    )
        }),
        ('Адреса email',{
            'fields': ( 'from_email', 'notification_emails','vacancy_email',
                    )
        }),
    )
admin.site.register(Shop, ShopAdmin)
