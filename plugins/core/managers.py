# django imports
from django.db import models
from mptt.managers import TreeManager

class ActiveManager(models.Manager):
    """An extended manager to return active objects.
    """
    def active(self):
        return super(ActiveManager, self).get_query_set().filter(active=True)


class MPTTActiveManager(TreeManager):
    def active(self):
        return super(MPTTActiveManager, self).get_query_set().filter(active=True)