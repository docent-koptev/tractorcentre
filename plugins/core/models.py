# -*- coding: utf-8 -*-
# python imports
import re

# django imports
from django.contrib.auth.models import User
from django.core.cache import cache
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.core.urlresolvers import reverse

# lfs imports
from sorl.thumbnail.fields import ImageField
from plugins.core.fields.thumbs import ImageWithThumbsField
from plugins.catalog.models import StaticBlock


class Shop(models.Model):
    name = models.CharField(_(u"Name"), max_length=30)
    shop_owner = models.CharField(_(u"Shop owner"), max_length=100, blank=True)
    from_email = models.EmailField(_(u"From e-mail address"))
    vacancy_email = models.EmailField(_(u"Почта для получения резюме"),blank=True)
    notification_emails  = models.TextField(_(u"Notification email addresses"))

    description = models.TextField(_(u"Description"), blank=True)
    image = ImageField(_(u"Image"), upload_to="shops", blank=True, null=True)
    static_block = models.ForeignKey(StaticBlock, verbose_name=_(u"Static block"), blank=True, null=True, related_name="shops")

    google_analytics_id = models.CharField(_(u"Google Analytics ID"), blank=True, max_length=20)
    ga_site_tracking = models.BooleanField(_(u"Google Analytics Site Tracking"), default=False)
    ga_ecommerce_tracking = models.BooleanField(_(u"Google Analytics E-Commerce Tracking"), default=False)

    default_currency = models.CharField(_(u"Default Currency"), max_length=30, default="RUR")
    confirm_toc = models.BooleanField(default=False)

    user = models.ForeignKey(User, blank=True, null=True, related_name='shops')
    active = models.BooleanField(default=True, editable=False)

    class Meta:
        permissions = (("manage_shop", "Manage shop"),)
        verbose_name = u'Магазин'
        verbose_name_plural = u'Магазины'

    def __unicode__(self):
        return self.name


    def get_vacancy_email(self):
        if self.vacancy_email:
            return self.vacancy_email

    def get_notification_emails(self):
        """Returns the notification e-mail addresses as list
        """
        adresses = re.split("[\s,]+", self.notification_emails)
        return adresses

    def get_parent_for_portlets(self):
        """Implements contract for django-portlets. Returns always None as there
        is no parent for a shop.
        """
        return None


class ShopAddress(models.Model):
    shop = models.ForeignKey(Shop,verbose_name=u'Магазин')
    shop_name = models.CharField(u'название магазина', max_length=100)
    address = models.CharField(u'адрес', max_length=300, help_text=u'Формат адреса: Красная площадь, 3, ГУМ, 3-я линия, 3-й этаж, 109012 - Москва')
    #place = models.ForeignKey('Place', verbose_name=u'Город')

    #def __unicode__(self):
        #return '%s - %s' % (self.shop_name, self.place)

    class Meta:
        verbose_name = u'Адрес магазина'
        verbose_name_plural = u'Адреса магазинов'


