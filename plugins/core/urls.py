# -*- coding: utf-8 -*-
from django.conf.urls import *
from plugins.news.models import *

# Cart
urlpatterns = patterns('plugins.cart.views',
    url(r'^add-to-cart/(?P<product_id>\d*)$', "add_to_cart", name='add_to_cart'),
    url(r'^delete-cart-item/(?P<cart_item_id>\d*)$', "delete_cart_item", name="delete_cart_item"),
    url(r'^refresh-cart$', "refresh_cart"),
    url(r'^cart$', "cart", name="cart"),
)

# Checkout
urlpatterns += patterns('plugins.checkout.views',
    url(r'^checkout-dispatcher$', "checkout_dispatcher", name="checkout_dispatcher"),
    url(r'^checkout$', "one_page_checkout", name="checkout"),
    url(r'^thank-you$', "thank_you",name="thank_you"),

)

# Password reset and
urlpatterns += patterns('django.contrib.auth.views',
     url(r'^password-reset-done/$', "password_reset_done"),
     url(r'^password-reset-confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', "password_reset_confirm"),
     url(r'^password-reset-complete/$', "password_reset_complete",name="password_reset_complete"),
     url(r'^password-change/$', 'password_change', name="password_change"),
     url(r'^password-change/done/$', 'password_change_done', name="password_change_done"),
)

# logout
urlpatterns += patterns('django.contrib.auth.views',
     url(r'^logout/$', "logout", {'next_page': '/'}, name='logout'),
)

# Customer
urlpatterns += patterns('plugins.customer.views',
    url(r'^login/$',  "login", name="docent_login"),
    url(r'^my-account$', "account", name="docent_my_account"),
    url(r'^my-data$', "change_data", name="docent_my_data"),
    url(r'^my-orders$', "orders", name="docent_my_orders"),
    url(r'^my-orders/(?P<id>\d+)$', "order", name="docent_my_order"),
    url(r'^password-reset/$', "password_reset", name="password_reset"),
)

# Search
urlpatterns += patterns('plugins.search.views',
    url(r'^search$', "search", name="docent_search"),
    url(r'^livesearch$', "livesearch", name="docent_livesearch"),
)

# Admin Catalog
urlpatterns += patterns('plugins.catalog.views',
    url(r'^admin/create_sale$', "create_sale_view", name="ecko_admin_create_sale"),
    url(r'^admin/remove_sale$', "remove_sale_view", name="ecko_admin_remove_sale"),
    url(r'^admin/set_proprty_group_view$', "set_proprty_group_view", name="ecko_admin_set_proprty_group"),
)