# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import autoslug.fields


class Migration(migrations.Migration):

    dependencies = [
        ('videogallery', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='slug',
            field=autoslug.fields.AutoSlugField(default=1, unique=True, verbose_name='\u0421\u0441\u044b\u043b\u043a\u0430'),
            preserve_default=False,
        ),
    ]
