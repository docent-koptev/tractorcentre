# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import autoslug.fields


class Migration(migrations.Migration):

    dependencies = [
        ('videogallery', '0003_video_active'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='slug',
            field=autoslug.fields.AutoSlugField(unique=True, verbose_name='slug'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='video',
            name='video',
            field=models.CharField(help_text='\u0421\u0442\u0440\u043e\u0433\u0438\u0439 \u0444\u043e\u0440\u043c\u0430\u0442: https://www.youtube.com/watch?v=hlHoOygwIhg', max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441 \u0441\u0441\u044b\u043b\u043a\u0438'),
            preserve_default=True,
        ),
    ]
