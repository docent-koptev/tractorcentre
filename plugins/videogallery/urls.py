from django.conf.urls import url, patterns

urlpatterns = patterns('plugins.videogallery.views',
    url(r'^$', 'video_list', name='video_list'),
    url(r'^(?P<slug>[-\w]*)$', 'video_detail', name='video_detail'),
)