# -*- coding: utf-8 -*-
# django imports
from django.db import models
from autoslug import AutoSlugField

class Video(models.Model):
    title = models.CharField(u'Название', max_length=255, blank=True, default='')
    slug = AutoSlugField(u"slug", unique=True)
    video = models.CharField(u'Адрес ссылки',max_length=255,help_text=u'Строгий формат: https://www.youtube.com/watch?v=hlHoOygwIhg')
    active = models.BooleanField(u'Активность',default=True)
    description = models.TextField(u"Описание", blank=True,null=True)
    
    class Meta:
        verbose_name = u"Видео"
        verbose_name_plural = u'Видео'

    @property
    def get_youtube_id(self):
    	video_str = ''
    	if self.video:
    		if 'watch' in self.video:
    			video_str = self.video.split("v=")[1]
    	return video_str

    def __unicode__(self):
        return self.title