# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from django.shortcuts import get_object_or_404, redirect, render,HttpResponse

from sorl.thumbnail import get_thumbnail
from plugins.videogallery.models import *

class VideoAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug': ['title',]}
	list_display = ['title', 'admin_image_preview','video','active',]
	list_editable = ['video',]
	fieldsets = (
        ('Основное',{
            'fields': ('title','slug','description', 'active','video',)
        }),
    )

	def admin_image_preview(self, obj):
		return '<div class="video_index"><iframe width="200" height="100" src="http://img.youtube.com/vi/%s/3.jpg" frameborder="0" allowfullscreen> </iframe></div>' %obj.get_youtube_id
	admin_image_preview.short_description = u'превью'
	admin_image_preview.allow_tags = True

admin.site.register(Video, VideoAdmin)
