# -*- coding: utf-8 -*-
from django.http import Http404
from django.shortcuts import get_object_or_404,HttpResponse
from annoying.decorators import render_to
from plugins.videogallery.models import *
from django.utils.translation import ugettext_lazy as _
from datetime import datetime
from utils.helpers import Paginate
import urlparse

@render_to('video_list.html')
def video_list(request):
    video_list = Video.objects.filter(active=True)
    return { 'video_list': video_list, }

def video_detail(request, slug):
	if request.is_ajax():
		video = get_object_or_404(Video, slug=slug)
		return HttpResponse(video.get_youtube_id)
	raise Http404(u"Как вы сюда попали?")
