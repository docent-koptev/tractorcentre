# -*- coding: utf-8 -*-
# django imports
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.template import RequestContext
import json as simplejson
from django.utils.translation import ugettext_lazy as _

# lfs imports
from annoying.functions import get_object_or_None
from annoying.decorators import ajax_request
import plugins.catalog.utils

from plugins.caching.utils import lfs_get_object_or_404
from plugins.cart.models import *
from plugins.core.signals import cart_changed
from plugins.core import utils as core_utils
from plugins.catalog.settings import PRODUCT_WITH_VARIANTS
from plugins.customer import utils as customer_utils

from plugins.cart import utils as cart_utils

import json as simplejson

def cart(request, template_name="cart/cart.html"):
    """The main view of the cart.
    """
    cart = cart_utils.get_cart(request)


    # Cart costs
    cart_costs = cart_utils.get_cart_costs(request, cart)
    cart_price = cart_costs["price"]
    shopping_url = cart_utils.get_go_on_shopping_url(request)
    response = render_to_response(template_name, RequestContext(request, {
        "shopping_url": shopping_url,
        "cart_inline" : cart_inline(request),
        "cart_costs" : cart_costs,
        "cart_price" : cart_price,
    }))

    response['Cache-Control'] = 'no-store, no-cache, must-revalidate, max-age=0'
    response["Expires"] = 'Sat, 26 Jul 1997 05:00:00 GMT'
    return response

def cart_inline(request, template_name="cart/cart_inline.html"):
    cart = cart_utils.get_cart(request)
    shopping_url = cart_utils.get_go_on_shopping_url(request)
    if cart is None:
        return render_to_string(template_name, RequestContext(request, {
            "shopping_url" : shopping_url,
        }))



    # Cart costs
    cart_costs = cart_utils.get_cart_costs(request, cart)
    cart_price = cart_costs["price"]
    not_auth = (request.session.has_key('not_auth') and request.session.pop('not_auth')) or []
    
    return render_to_string(template_name, RequestContext(request, {
        "cart" : cart,
        "cart_costs" : cart_costs,
        "cart_price" : cart_price,
        "shopping_url" : shopping_url,
        "not_auth": not_auth
    }))

def add_to_cart(request, product_id=None):
    if product_id is None:
        product_id = request.REQUEST.get("product_id")

    product = get_object_or_None(Product, pk=product_id)
    if not product.is_active():
        raise Http404()

    try:
        quantity = float(request.POST.get("quantity", 1))
    except TypeError:
        quantity = 1

    cart = cart_utils.get_or_create_cart(request)


    try:
        cart_item = CartItem.objects.get(cart = cart, product = product)
    except ObjectDoesNotExist:
        cart_item = CartItem(cart=cart, product=product, amount=quantity)
        cart_item.save()
    else:
        cart_item.amount += quantity
        cart_item.save()

    cart_items = [cart_item]

    # Store cart items for retrieval within added_to_cart.
    request.session["cart_items"] = cart_items
    cart_changed.send(cart, request=request)

    # Update the customer's shipping method (if appropriate)
    customer = customer_utils.get_or_create_customer(request)
    # Save the cart to update modification date
    cart.save()

    cart = cart_utils.get_cart(request)
    count = 0
    if cart:
        count = cart.cartitem_set.count()
    # для определения окончания слова
    if (count % 100) > 20 or (count % 100) < 5 :
        count_modulo_10 = count % 10
    else:
        count_modulo_10 = 5
    # стоимость корзины
    cart_costs = cart_utils.get_cart_costs(request, cart)
    data = {}
    return render_to_response('cart/cart_products_counter.html', RequestContext(request, {
        'count': count, 
        'count_modulo_10': count_modulo_10, 
        "cart_costs": cart_costs
    }))


def delete_cart_item(request, cart_item_id):
    """Deletes the cart item with the given id.
    """
    lfs_get_object_or_404(CartItem, pk=cart_item_id).delete()

    cart = cart_utils.get_cart(request)
    cart_changed.send(cart, request=request)

    return HttpResponse(cart_inline(request))

@ajax_request
def refresh_cart(request):
    """Refreshes the cart after some changes has been taken place: the amount
    of a product or shipping/payment method.
    """
    cart = cart_utils.get_cart(request)
    customer = customer_utils.get_or_create_customer(request)

    # Update Amounts
    for item in cart.items():
        amount = request.POST.get("amount-cart-item_%s" % item.id, 0)
        try:
            amount = float(amount)
            # if item.product.manage_stock_amount and amount > item.product.stock_amount:
            #     amount = item.product.stock_amount
            #     if amount < 0:
            #         amount = 0
            #     request.session['cart_errors'] = [u'К сожалению товара "%(product)s" осталось в наличии %(amount)s ' %
            #                                       {"product": item.product.name, "amount" : amount}]
        except ValueError:
            amount = 1

        if item.product.active_packing_unit:
            item.amount = plugins.catalog.utils.calculate_real_amount(item.product, float(amount))
        else:
            item.amount = amount

        if amount == 0:
            item.delete()
        else:
            item.save()

    # IMPORTANT: We have to send the signal already here, because the valid
    # shipping methods might be dependent on the price.
    cart_changed.send(cart, request=request)
    customer.save()

    return {"html" : cart_inline(request),}



