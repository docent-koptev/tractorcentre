import re

# django imports
from django.core.cache import cache
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _

# lfs imports
import plugins.catalog.utils
from plugins.catalog.models import Product, Property, PropertyOption

class Cart(models.Model):
    user = models.ForeignKey(User, verbose_name=_(u"User"), blank=True, null=True)
    session = models.CharField(_(u"Session"), blank=True, max_length=100)
    creation_date = models.DateTimeField(_(u"Creation date"), auto_now_add=True)
    modification_date = models.DateTimeField(_(u"Modification date"), auto_now=True, auto_now_add=True)

    def items(self):
        items = CartItem.objects.filter(cart=self)
        return items

    @property
    def amount_of_items(self):
        """Returns the amount of items of the cart.
        """
        amount = 0
        for item in self.items():
            amount += item.amount
        return amount
    
    @property
    def have_items(self):
        if self.amount_of_items == 0:
            return False
        else:
            return True


    def get_name(self):
        cart_name = ""
        for cart_item in self.items.all():
            if cart_name.product is not None:
                cart_name = cart_name + cart_name.product.get_name() + ", "

        cart_name.strip(', ')
        return cart_name

    def get_price_gross(self):
        """Returns the total gross price of all items.
        """
        price = 0
        for item in self.items():
            price += item.get_price()

        return price


    def get_tax(self):
        """Returns the total tax of all items
        """
        tax = 0
        for item in self.items():
            tax += item.get_tax()

        return tax

    def get_item(self, product, properties):
        """Returns the item for passed product and properties or None if there
        is none.
        """
        for item in CartItem.objects.filter(cart=self, product=product):
            item_props = {}
            for pv in item.properties.all():
                item_props[unicode(pv.property.id)] = pv.value

            if item_props == properties:
                return item

        return None

class CartItem(models.Model):
    cart = models.ForeignKey(Cart, verbose_name=_(u"Cart"))
    product = models.ForeignKey(Product, verbose_name=_(u"Product"))
    amount = models.FloatField(_(u"Quantity"), blank=True, null=True)
    creation_date = models.DateTimeField(_(u"Creation date"), auto_now_add=True)
    modification_date = models.DateTimeField(_(u"Modification date"), auto_now=True, auto_now_add=True)

    class Meta:
        ordering = ['id']

    def get_price(self):
        product_price = self.product.get_price()
        return product_price * self.amount


class CartItemPropertyValue(models.Model):
    cart_item = models.ForeignKey(CartItem, verbose_name=_(u"Cart item"), related_name="properties")
    property = models.ForeignKey(Property, verbose_name = _(u"Property"))
    value = models.CharField("Value", blank=True, max_length=100)