# django imports
from django.core.exceptions import ObjectDoesNotExist
from django.core.cache import cache
from django.core.urlresolvers import reverse

# lfs imports
from plugins.utils.misc import get_first_or_None
from plugins.cart.models import CartItem, Cart
from plugins.core.signals import cart_changed

def get_cart_price(request, cart, total=False):
    """Returns price of the given cart.
    """
    return get_cart_costs(request, cart, total)["price"]

def get_cart_costs(request, cart, total=False):
    """Returns a dictionary with price and tax of the given cart:

        returns {
            "price" : the cart's price,
            "tax" : the cart's tax,
        }
    """
    if cart is None:
        return {"price" : 0}
    items = cart.items()
    cart_price = 0
    for item in items:
        cart_price += item.get_price()
    cart_costs = { "price" : cart_price }

    return cart_costs

def get_or_create_cart(request):
    """Returns the cart of the current user. If no cart exists it creates a new
    one first.
    """
    cart = get_cart(request)
    if cart is None:
        cart = create_cart(request)

    return cart

def create_cart(request):
    """Creates a cart for the current session and/or user.
    """
    cart = Cart(session = request.session.session_key)
    if request.user.is_authenticated():
        cart.user = request.user

    cart.save()
    return cart

def get_cart(request):
    """Returns the cart of the current customer or None.
    """
    if not request.session.exists(request.session.session_key):
        request.session.create()
    session_key = request.session.session_key
    user = request.user

    if user.is_authenticated():
        if not session_key:
            raise Exception('session key is None')
        cart = get_first_or_None(Cart.objects.filter(user = user))
        return cart
    else:
        if not session_key:
            raise Exception('session key is None')
        cart = get_first_or_None(Cart.objects.filter(session = session_key))
        return cart

def get_go_on_shopping_url(request):
    """Calculates the go on shopping url.
    """
    lc = request.session.get("last_category")
    if lc:
        return lc  #lc.get_absolute_url()
    else:
        return '' # reverse("home")

def update_cart_after_login(request):
    """Updates the cart after login.

    1. if there is no session cart, nothing has to be done.
    2. if there is a session cart and no user cart we assign the session cart
       to the current user.
    3. if there is a session cart and a user cart we add the session cart items
       to the user cart.
    """
    try:
        session_cart = Cart.objects.get(session = request.session.session_key)
        try:
            user_cart = Cart.objects.get(user = request.user)
        except ObjectDoesNotExist:
            session_cart.user = request.user
            session_cart.save()
        else:
            for session_cart_item in session_cart.items():
                try:
                    user_cart_item = CartItem.objects.get(cart = user_cart, product = session_cart_item.product)
                except ObjectDoesNotExist:
                    session_cart_item.cart = user_cart
                    session_cart_item.save()
                else:
                    user_cart_item.amount += session_cart_item.amount
                    user_cart_item.save()
            session_cart.delete()
            cart_changed.send(user_cart, request=request)
    except ObjectDoesNotExist:
        pass