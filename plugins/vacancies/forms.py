# -*- coding: utf-8 -*-
from django import forms
import magic

from plugins.vacancies.models import Rezume

class RequestVacancyForm(forms.ModelForm):
	vacancy = forms.CharField(label=u"Вакансия", max_length=220, widget=forms.TextInput(attrs={'class':'form-control','id':'vacancy'}))
	fio= forms.CharField(label=u"fio", max_length=220, widget=forms.TextInput(attrs={'class':'form-control','id':'fio'}))
	email = forms.EmailField(label=u'Email', required=False, widget=forms.TextInput(attrs={'class':'form-control','id':'email'}))
	phone = forms.CharField(label=u"phone", max_length=50, widget=forms.TextInput(attrs={'class':'form-control','id':'phone'}))
	file = forms.FileField(required=False)

	class Meta:
		model = Rezume
		fields = '__all__'

	def clean_file(self):
		if self.cleaned_data['file']:
			file = self.cleaned_data['file']
			mime = magic.from_buffer(file.read(), mime=True)
			if not mime == 'application/pdf':
				raise forms.ValidationError(u'Резюме дожлжно иметь формат PDF')
		return self.cleaned_data['file']