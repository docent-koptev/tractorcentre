# -*- coding: utf-8 -*-
import os
from django.db import models
from autoslug import AutoSlugField


class Vacancy(models.Model):
    name = models.CharField(verbose_name=u"Название",blank=True, max_length=400, unique=True)
    price = models.CharField(verbose_name=u"Уровень зп",blank=True, max_length=300)
    active = models.BooleanField(verbose_name=u"Активность", default=True)
    position = models.IntegerField(u"Position", default=1000)

    def get_all_requiremements(self):
        requirements = Requirements.objects.filter(vacancy=self)
        return requirements

    def get_all_duty(self):
        duty = Duty.objects.filter(vacancy=self)
        return duty

    def get_all_conditions(self):
        conditions = Conditions.objects.filter(vacancy=self)
        return conditions

    class Meta:
        ordering = ("position", )
        verbose_name = u'Вакансия'
        verbose_name_plural = u'Вакансии'

    def __unicode__(self):
        return self.name

class Requirements(models.Model):
    vacancy = models.ForeignKey(Vacancy,verbose_name=u'Вакансия')
    value = models.TextField(verbose_name=u"Описание",blank=True)
    
    class Meta:
        verbose_name = u'Требования'
        verbose_name_plural = u'Требования'

class Duty(models.Model):
    vacancy = models.ForeignKey(Vacancy,verbose_name=u'Вакансия')
    value = models.TextField(verbose_name=u"Описание",blank=True)

    class Meta:
        verbose_name = u'Функциональные обязанности'
        verbose_name_plural = u'Функциональные обязанности'

class Conditions(models.Model):
    vacancy = models.ForeignKey(Vacancy,verbose_name=u'Вакансия')
    value = models.TextField(verbose_name=u"Описание",blank=True)

    class Meta:
        verbose_name = u'Условия'
        verbose_name_plural = u'Условия'

class Rezume(models.Model):
    vacancy = models.CharField(verbose_name=u"Вакансия", max_length=500)
    fio = models.CharField(verbose_name=u"ФИО", max_length=500)
    email = models.CharField(verbose_name=u"Почта", max_length=500,blank=True)
    phone = models.CharField(verbose_name=u"Телефон", max_length=500)
    file = models.FileField(verbose_name=u'Резюме в PDF', blank=True,null=True,upload_to='rezume_pdf')

    class Meta:
        verbose_name = u'Резюме'
        verbose_name_plural = u'Резюме'

    @property
    def file_abs_path(self):
       return self.file.path
    
    @property
    def filename(self):
       return os.path.basename(self.file.name)

    def __unicode__(self):
        return self.vacancy