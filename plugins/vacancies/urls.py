from django.conf.urls import url, patterns
from django.views.generic import TemplateView

urlpatterns = patterns('plugins.vacancies.views',
	url(r'^$', 'vacancies_list', name='vacancies_list'),
	url(r'^vacancies-thanks/$', TemplateView.as_view(template_name="thanks.html"),name='vacancies_thanks'),
)
