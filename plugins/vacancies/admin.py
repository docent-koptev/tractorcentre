# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from django.db import models
from django.conf import settings
from django.forms import TextInput, Textarea
from plugins.vacancies.models import *


class RequirementsFormInline(admin.TabularInline):
    model = Requirements
    extra = 1
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows':3, 'cols':80})},
    }

class DutyFormInline(admin.TabularInline):
    model = Duty
    extra = 1
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows':3, 'cols':80})},
    }

class ConditionsFormInline(admin.TabularInline):
    model = Conditions
    extra = 1
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows':3, 'cols':80})},
    }

class VacancyAdmin(admin.ModelAdmin):
    inlines = [RequirementsFormInline,DutyFormInline, ConditionsFormInline]
    list_display = ('name','price','active','position',)
    list_editable = ('position',)

  

class RezumeAdmin(admin.ModelAdmin):
    list_display = ('vacancy','fio','email','phone',)
    readonly_fields = ('vacancy','fio','email','phone',)
    fieldsets = (
        (u'Контактная информация',{
            'fields': ('vacancy', 'fio', ('email'),'phone',
                    )
        }),

        (u'Резюме', {
            'fields': ( 'file',),
        }),

    )

admin.site.register(Rezume,RezumeAdmin)
admin.site.register(Vacancy, VacancyAdmin)
