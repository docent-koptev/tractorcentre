# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vacancies', '0002_vacancy_position'),
    ]

    operations = [
        migrations.CreateModel(
            name='Rezume',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vacancy', models.CharField(max_length=500, verbose_name='\u0412\u0430\u043a\u0430\u043d\u0441\u0438\u044f')),
                ('fio', models.CharField(max_length=500, verbose_name='\u0424\u0418\u041e')),
                ('email', models.CharField(max_length=500, verbose_name='\u041f\u043e\u0447\u0442\u0430', blank=True)),
                ('phone', models.CharField(max_length=500, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d')),
                ('file', models.FileField(upload_to=b'rezume_pdf', null=True, verbose_name='\u0420\u0435\u0437\u044e\u043c\u0435 \u0432 PDF', blank=True)),
            ],
            options={
                'verbose_name': '\u0420\u0435\u0437\u044e\u043c\u0435',
                'verbose_name_plural': '\u0420\u0435\u0437\u044e\u043c\u0435',
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='vacancy',
            options={'ordering': ('position',), 'verbose_name': '\u0412\u0430\u043a\u0430\u043d\u0441\u0438\u044f', 'verbose_name_plural': '\u0412\u0430\u043a\u0430\u043d\u0441\u0438\u0438'},
        ),
    ]
