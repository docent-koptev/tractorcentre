# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Conditions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=500, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
            ],
            options={
                'verbose_name': '\u0423\u0441\u043b\u043e\u0432\u0438\u044f',
                'verbose_name_plural': '\u0423\u0441\u043b\u043e\u0432\u0438\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Duty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=500, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
            ],
            options={
                'verbose_name': '\u0424\u0443\u043d\u043a\u0446\u0438\u043e\u043d\u0430\u043b\u044c\u043d\u044b\u0435 \u043e\u0431\u044f\u0437\u0430\u043d\u043d\u043e\u0441\u0442\u0438',
                'verbose_name_plural': '\u0424\u0443\u043d\u043a\u0446\u0438\u043e\u043d\u0430\u043b\u044c\u043d\u044b\u0435 \u043e\u0431\u044f\u0437\u0430\u043d\u043d\u043e\u0441\u0442\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Requirements',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=500, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
            ],
            options={
                'verbose_name': '\u0422\u0440\u0435\u0431\u043e\u0432\u0430\u043d\u0438\u044f',
                'verbose_name_plural': '\u0422\u0440\u0435\u0431\u043e\u0432\u0430\u043d\u0438\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Vacancy',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=400, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True)),
                ('price', models.CharField(max_length=300, verbose_name='\u0423\u0440\u043e\u0432\u0435\u043d\u044c \u0437\u043f', blank=True)),
                ('active', models.BooleanField(default=True, verbose_name='\u0410\u043a\u0442\u0438\u0432\u043d\u043e\u0441\u0442\u044c')),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': '\u0412\u0430\u043a\u0430\u043d\u0441\u0438\u044f',
                'verbose_name_plural': '\u0412\u0430\u043a\u0430\u043d\u0441\u0438\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='requirements',
            name='vacancy',
            field=models.ForeignKey(verbose_name='\u0412\u0430\u043a\u0430\u043d\u0441\u0438\u044f', to='vacancies.Vacancy'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='duty',
            name='vacancy',
            field=models.ForeignKey(verbose_name='\u0412\u0430\u043a\u0430\u043d\u0441\u0438\u044f', to='vacancies.Vacancy'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='conditions',
            name='vacancy',
            field=models.ForeignKey(verbose_name='\u0412\u0430\u043a\u0430\u043d\u0441\u0438\u044f', to='vacancies.Vacancy'),
            preserve_default=True,
        ),
    ]
