# -*- coding: utf-8 -*-
import json

from django.http import Http404
from django.shortcuts import get_object_or_404, render_to_response,redirect,HttpResponse
from annoying.decorators import render_to
from plugins.vacancies.models import *
from plugins.vacancies.forms import *
import plugins.core.utils
from django.utils.translation import ugettext_lazy as _
from datetime import datetime

from plugins.core.signals import vacancy_send


@render_to('vacancies_list.html')
def vacancies_list(request,**kwargs):
	vacancies_list = Vacancy.objects.filter(active=True).order_by("position")
	shop = plugins.core.utils.get_default_shop()
	vac_email = shop.get_vacancy_email()
	show_form = False
	if request.method == 'POST':
		form = RequestVacancyForm(request.POST,request.FILES)
		if form.is_valid():
			cd = form.cleaned_data
			newRezume = form.save()
			vacancy_send.send(sender=newRezume)
			return redirect("/vacancies/vacancies-thanks/")
		else:
			show_form = True
	else:
		form = RequestVacancyForm()

	return { 'vacancies_list': vacancies_list,'form':form,'show_form':show_form,"vac_email":vac_email, }

