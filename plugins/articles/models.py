# -*- coding: utf-8 -*-
import datetime
from django.contrib.sitemaps import Sitemap
from django.db import models
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail.fields import ImageField

class Article(models.Model):
    title = models.CharField(_(u'заголовок'), max_length=255)
    slug = models.SlugField(unique=True,verbose_name=u'Ссылка',help_text=u'формирует вид урл.Уникальный элемент')
    body = models.TextField(_(u'текст'), blank=True,help_text=u'Полный текст')
    short = models.TextField(_(u'кратко'), blank=True,help_text=u'Краткий текст.Анонс')
    image = ImageField(_(u'изображение'), upload_to='uploads/articles', blank=True)
    display = models.BooleanField(_(u'опубликовать'), default=True)
    last_modification = models.DateTimeField(_(u'дата последнего изменения'), blank=True,null=True)
    
    seo_title = models.CharField(u'seo title', max_length=200, blank=True, null=True)
    seo_description = models.CharField(u'Description', max_length=200, blank=True, null=True)
    seo_keywords = models.CharField(u'Keywords', max_length=200, blank=True, null=True)
    
    class Meta:
        ordering = ('-last_modification',)
        verbose_name = _(u'Статья компании')
        verbose_name_plural = _(u'Статьи компании')

    def __unicode__(self):
        return self.title


    def get_seo_title():
        if self.seo_title and self.seo_title is not None:
            return unicode(self.seo_title)
        return self.title

    def get_seo_description():
        if self.seo_description:
            return self.seo_description
        return None

    def get_seo_keywords():
        if self.seo_keywords:
            return self.seo_keywords
        return None


    @models.permalink
    def get_absolute_url(self):
        return 'article_detail', (), {'slug': self.slug}

class ArticleSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Article.objects.filter(display=True)