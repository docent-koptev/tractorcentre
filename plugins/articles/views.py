# -*- coding: utf-8 -*-
from django.http import Http404
from django.shortcuts import get_object_or_404
from annoying.decorators import render_to
from plugins.articles.models import Article
from django.utils.translation import ugettext_lazy as _
from datetime import datetime
from utils.helpers import Paginate

SORT_BY_DATE = (
    ('-last_modification', _(u'начиная с последних &darr;')),
    ('last_modification', _(u'начиная с первых &uarr;')),
)

SORT_PARAM = 'sort_news'

def get_sort(request):
    sort = request.GET.get(SORT_PARAM, request.session.get(SORT_PARAM, SORT_BY_DATE[0][0]))
    if sort:
        request.session['sort'] = sort
    return sort

@render_to('article_list.html')
def articles(request):
    articles_list = Article.objects.filter(display=True)
    articles = Paginate(request,articles_list,10,2)
    return { 'articles': articles,  }

@render_to('article_detail.html')
def article_detail(request, slug):
    article_item = get_object_or_404(Article, slug=slug)
    if article_item.display:
        return { 'article_item': article_item }
    raise Http404('No %s matches the given query.' % article_item._meta.object_name)
