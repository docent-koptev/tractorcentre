# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0003_auto_20150928_1519'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='seo_keywords',
            field=models.CharField(max_length=200, null=True, verbose_name='Keywords', blank=True),
            preserve_default=True,
        ),
    ]
