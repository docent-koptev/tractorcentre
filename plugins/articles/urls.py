from django.conf.urls import url, patterns

urlpatterns = patterns('plugins.articles.views',
    url(r'^$', 'articles', name='articles'),
    url(r'^(?P<slug>[-\w]*)$', 'article_detail', name='article_detail'),
)