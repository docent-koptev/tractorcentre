# -*- coding: utf-8 -*-
# django imports
from django.contrib import admin
from sorl.thumbnail.shortcuts import get_thumbnail
from plugins.manufacturer.models import *

class ManufacturerAdmin(admin.ModelAdmin):
    list_display = ('name', 'admin_image_preview', 'order', 'hide',)
    list_editable = ('hide', 'order',)
    list_filter = ('hide',)

    def admin_image_preview(self, obj):
        if obj.image_link:
            thumbnail = get_thumbnail(obj.image, '70')
            return '<a href="%s" target="blank"><image style="max-height:70px;max-width:70px" src="%s"/></a>' % (obj.image.url, thumbnail.url)
        return ''
    admin_image_preview.short_description = u'превью'
    admin_image_preview.allow_tags = True


class PartnerAdmin(admin.ModelAdmin):
    list_display = ('pk','name', 'admin_image_preview',  'link','hide','show_on_main',)
    list_editable = ('name','hide','link','show_on_main',)
    list_filter = ('hide','show_on_main',)
    list_display_links = ('pk',)

    def admin_image_preview(self, obj):
        if obj.image:
            thumbnail = get_thumbnail(obj.image, '70')
            return '<a href="%s" target="blank"><image style="max-height:70px;max-width:70px" src="%s"/></a>' % (obj.image.url, thumbnail.url)

    admin_image_preview.short_description = u'превью'
    admin_image_preview.allow_tags = True

admin.site.register(Manufacturer, ManufacturerAdmin)
admin.site.register(Partners, PartnerAdmin)