# -*- coding: utf-8 -*-
# django imports
from django.contrib.sitemaps import Sitemap
from django.db import models
from django.utils.translation import ugettext_lazy as _
from autoslug import AutoSlugField


class Manufacturer(models.Model):
    """The manufacturer is the unique creator of a product.
    """
    name = models.CharField(_(u"name"), max_length=50)
    hide = models.BooleanField(u'скрыть', default=0)
    desc = models.TextField(u'описание', blank=True)
    image = models.ImageField(u'картинка', blank=True, upload_to='brands')
    image_link = models.CharField(u'ссылка для картинки', blank=True, null=True, max_length=10000)
    order = models.IntegerField(u'позиция', default=1000, help_text=u'№ для сортировки брендов')
    uid = models.CharField(u'мойсклад идентификатор', max_length=50, blank=True,help_text=u'заполняется автоматически при синхронизации',null=True)

    class Meta:
        ordering = ('name', )
        verbose_name = u'Производитель'
        verbose_name_plural = u'Производители'

    def __unicode__(self):
        return self.name


class Partners(models.Model):
    name = models.CharField(_(u"name"), max_length=50)
    #slug = AutoSlugField(_(u"Slug"), unique=True)
    #description = models.TextField(_(u"Description"), blank=True)
    image = models.ImageField(u'картинка', blank=True, upload_to='brands')
    link = models.CharField(_(u"ссылка"), max_length=50,help_text=u'Начинаем с http://, если внешняя ссылка',blank=True,null=True)
    hide = models.BooleanField(u'скрыть', default=False)

    show_on_main = models.BooleanField(u'На главной',default=False)
    
    class Meta:
        ordering = ('name', )
        verbose_name = u'Партнер'
        verbose_name_plural = u'Партнеры'

    def __unicode__(self):
        return self.name

class BrandsSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.7

    def items(self):
        return Manufacturer.objects.filter(hide=False)