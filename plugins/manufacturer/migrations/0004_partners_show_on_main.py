# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manufacturer', '0003_auto_20150407_1140'),
    ]

    operations = [
        migrations.AddField(
            model_name='partners',
            name='show_on_main',
            field=models.BooleanField(default=False, verbose_name='\u041d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439'),
            preserve_default=True,
        ),
    ]
