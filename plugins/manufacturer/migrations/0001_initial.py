# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='name')),
                ('hide', models.BooleanField(default=0, verbose_name='\u0441\u043a\u0440\u044b\u0442\u044c')),
                ('desc', models.TextField(verbose_name='\u043e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('image', models.ImageField(upload_to=b'brands', verbose_name='\u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0430', blank=True)),
                ('image_link', models.CharField(max_length=10000, null=True, verbose_name='\u0441\u0441\u044b\u043b\u043a\u0430 \u0434\u043b\u044f \u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0438', blank=True)),
                ('order', models.IntegerField(default=1000, help_text='\u2116 \u0434\u043b\u044f \u0441\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0438 \u0431\u0440\u0435\u043d\u0434\u043e\u0432', verbose_name='\u043f\u043e\u0437\u0438\u0446\u0438\u044f')),
                ('uid', models.CharField(help_text='\u0437\u0430\u043f\u043e\u043b\u043d\u044f\u0435\u0442\u0441\u044f \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u0438 \u043f\u0440\u0438 \u0441\u0438\u043d\u0445\u0440\u043e\u043d\u0438\u0437\u0430\u0446\u0438\u0438', max_length=50, null=True, verbose_name='\u043c\u043e\u0439\u0441\u043a\u043b\u0430\u0434 \u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440', blank=True)),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': '\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u0442\u0435\u043b\u044c',
                'verbose_name_plural': '\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0438\u0442\u0435\u043b\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Partners',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name='name')),
                ('image', models.ImageField(upload_to=b'brands', verbose_name='\u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0430', blank=True)),
                ('hide', models.BooleanField(default=False, verbose_name='\u0441\u043a\u0440\u044b\u0442\u044c')),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': '\u041f\u0430\u0440\u0442\u043d\u0435\u0440',
                'verbose_name_plural': '\u041f\u0430\u0440\u0442\u043d\u0435\u0440\u044b',
            },
            bases=(models.Model,),
        ),
    ]
