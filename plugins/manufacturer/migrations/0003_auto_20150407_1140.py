# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manufacturer', '0002_partners_link'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partners',
            name='link',
            field=models.CharField(help_text='\u041d\u0430\u0447\u0438\u043d\u0430\u0435\u043c \u0441 http://, \u0435\u0441\u043b\u0438 \u0432\u043d\u0435\u0448\u043d\u044f\u044f \u0441\u0441\u044b\u043b\u043a\u0430', max_length=50, null=True, verbose_name='\u0441\u0441\u044b\u043b\u043a\u0430', blank=True),
            preserve_default=True,
        ),
    ]
