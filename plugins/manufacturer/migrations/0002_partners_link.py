# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manufacturer', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='partners',
            name='link',
            field=models.CharField(default=1, max_length=50, verbose_name='\u0441\u0441\u044b\u043b\u043a\u0430'),
            preserve_default=False,
        ),
    ]
