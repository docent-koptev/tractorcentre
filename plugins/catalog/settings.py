# -*- coding: utf-8 -*-

from django.utils.translation import gettext_lazy as _

ACTIVE_FOR_SALE_STANDARD = 0
ACTIVE_FOR_SALE_YES = 2
ACTIVE_FOR_SALE_NO = 3
ACTIVE_FOR_SALE_CHOICES = [
    (ACTIVE_FOR_SALE_STANDARD, _(u"Standard")),
    (ACTIVE_FOR_SALE_YES, _(u"Yes")),
    (ACTIVE_FOR_SALE_NO, _(u"No")),    
]

STANDARD_PRODUCT = "0"
PRODUCT_WITH_VARIANTS = "1"
VARIANT = "2"
CONFIGURABLE_PRODUCT = "3"

PRODUCT_TYPE_LOOKUP = {
    STANDARD_PRODUCT : _(u"Standard"),
    PRODUCT_WITH_VARIANTS : _(u"Product with variants"),
    VARIANT : _(u"Variant"),
}

PRODUCT_TYPE_CHOICES = [
    (STANDARD_PRODUCT, _(u"Standard")),
    (PRODUCT_WITH_VARIANTS, _(u"Product with variants")),
    (VARIANT, _(u"Variant")),
]

PRODUCT_TYPE_FORM_CHOICES = [
    (STANDARD_PRODUCT, _(u"Standard")),
    (PRODUCT_WITH_VARIANTS, _(u"Product with variants")),
    (CONFIGURABLE_PRODUCT, _(u"Configurable product")),
]


LIST = 0
SELECT = 1
VARIANTS_DISPLAY_TYPE_CHOICES = [
    (LIST, _(u"List")),
    (SELECT, _(u"Select")),
]

CONTENT_PRODUCTS = 1
CONTENT_CATEGORIES = 2

CONTENT_CHOICES = (
    (CONTENT_PRODUCTS, _(u"Products")),
    (CONTENT_CATEGORIES, _(u"Categories")),
)

DELIVERY_TIME_UNIT_HOURS  = 1
DELIVERY_TIME_UNIT_DAYS   = 2
DELIVERY_TIME_UNIT_WEEKS  = 3
DELIVERY_TIME_UNIT_MONTHS = 4

DELIVERY_TIME_UNIT_CHOICES = (
    (DELIVERY_TIME_UNIT_HOURS, _(u"hours")),
    (DELIVERY_TIME_UNIT_DAYS, _(u"days")),
    (DELIVERY_TIME_UNIT_WEEKS, _(u"weeks")),
    (DELIVERY_TIME_UNIT_MONTHS, _(u"months")),
)

DELIVERY_TIME_UNIT_SINGULAR = {
    DELIVERY_TIME_UNIT_HOURS : _(u"hour"),
    DELIVERY_TIME_UNIT_DAYS : _(u"day"),
    DELIVERY_TIME_UNIT_WEEKS : _(u"week"),
    DELIVERY_TIME_UNIT_MONTHS : _(u"month"),
}

PROPERTY_VALUE_TYPE_FILTER = 0
PROPERTY_VALUE_TYPE_DEFAULT = 1
PROPERTY_VALUE_TYPE_DISPLAY = 2
PROPERTY_VALUE_TYPE_VARIANT = 3

PROPERTY_VALUE_TYPE_CHOICES = (
    (PROPERTY_VALUE_TYPE_FILTER, _(u'Filter')),
    (PROPERTY_VALUE_TYPE_DEFAULT, _(u'Default')),
    (PROPERTY_VALUE_TYPE_DEFAULT, _(u'Display')),
    (PROPERTY_VALUE_TYPE_VARIANT, _(u'Variant')),
)

PROPERTY_NUMBER_FIELD = 1
PROPERTY_TEXT_FIELD = 2
PROPERTY_SELECT_FIELD = 3

PROPERTY_FIELD_CHOICES = (
    (PROPERTY_SELECT_FIELD, _(u"Select field")),
)

PROPERTY_STEP_TYPE_AUTOMATIC  = 1
PROPERTY_STEP_TYPE_FIXED_STEP = 2
PROPERTY_STEP_TYPE_MANUAL_STEPS = 3

PROPERTY_STEP_TYPE_CHOICES = (
    (PROPERTY_STEP_TYPE_AUTOMATIC,    _(u"Automatic")),
    (PROPERTY_STEP_TYPE_FIXED_STEP,   _(u"Fixed step")),
    (PROPERTY_STEP_TYPE_MANUAL_STEPS, _(u"Manual steps")),
)


CAT_PRODUCT_PATH   = "lfs/catalog/categories/product"   # category with products
CAT_CATEGORY_PATH  = "lfs/catalog/categories/category"  # category with subcategories
PRODUCT_PATH       = "lfs/catalog/products"   # product templates
IMAGES_PATH        = "/media/lfs/icons" # Path to template preview images

# template configuration for category display
CATEGORY_ONE = 1
CATEGORY_TWO = 2
CATEGORY_THREE = 3
CATEGORY_TEMPLATES = (
    (CATEGORY_ONE,   u"Шаблон табличный (Старый)"),
    (CATEGORY_TWO,   u"Обновленный шаблон (Новый)"),
    (CATEGORY_THREE, u"Шаблон основной категории (Дерево)"),
)

# template configuration for product display    
PRODUCT_TEMPLATES = (
    (0,{"file":"%s/%s" % (PRODUCT_PATH ,"product_inline.html"),
        "image":IMAGES_PATH+"/product_default.png",
        "name":_(u"Default template")
        },),
) 

CP_PRICE_SUM = 1
CP_PRICE_CALC = 2

CP_PRICES = (
    (CP_PRICE_SUM, ("Sume")),
    (CP_PRICE_CALC, ("Calculated")),
)
