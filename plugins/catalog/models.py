# -*- coding: utf-8 -*-
# python imports
import re
from django.contrib.sitemaps import Sitemap
from annoying.functions import get_object_or_None
import unidecode
from django.template.defaultfilters import slugify
MARKDOWN_HELP_TEXT = ''#u'Для форматирования текста используется язык разметки <a href="http://ru.wikipedia.org/wiki/Markdown" target="_blank">markdown</a>'
MARKDOWN_AND_META_HELP_TEXT = ''#u'Для форматирования текста используется язык разметки <a href="http://ru.wikipedia.org/wiki/Markdown" target="_blank">markdown</a>.<br />Можно использовать теги:"<name>" и "<short-description>" для вставки названия и описания товара'

# # django imports
from django.contrib.contenttypes import generic
from django.core.exceptions import ObjectDoesNotExist
from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.db import models
from django.core import urlresolvers
from django.db.utils import IntegrityError
from django.utils.translation import ugettext_lazy as _
from django.db.models import signals

# lfs imports
import plugins.catalog.utils
from plugins.core.managers import ActiveManager,MPTTActiveManager
from plugins.catalog.settings import *
from plugins.manufacturer.models import Manufacturer,Partners
from plugins.service.models import Service
from mptt.models import MPTTModel,TreeForeignKey
from autoslug import AutoSlugField

from sorl.thumbnail import ImageField, get_thumbnail
from listeners import checked_chilren_elements



class Category(MPTTModel):
    name = models.CharField(_(u"Name"), max_length=250)
    slug = AutoSlugField(_(u"Slug"), unique=True)
    parent = models.ForeignKey("self", verbose_name=_(u"Родительская категория"), blank=True, null=True)
    show_all_products = models.BooleanField(_(u"Show all products"), default=True)
    products = models.ManyToManyField("Product", verbose_name=_(u"Products"), blank=True, through='ProductCategory')
    short_description = models.TextField(_(u"Short description"), blank=True, help_text=MARKDOWN_HELP_TEXT)
    description = models.TextField(_(u"Description"), blank=True, help_text=MARKDOWN_HELP_TEXT)
    position = models.IntegerField(_(u"Position"), default=1000)
    exclude_from_navigation = models.BooleanField(_(u"Exclude from navigation"), default=False)
    static_block = models.ForeignKey("StaticBlock", verbose_name=_(u"Static block"), blank=True, null=True,
                                     related_name="categories")
    template = models.PositiveSmallIntegerField(_(u"Шаблон"), max_length=400,
                                                choices=CATEGORY_TEMPLATES,default=CATEGORY_ONE)

    is_share = models.BooleanField(u'На всех моих детей',default=False)
    active_formats = models.BooleanField(_(u"Active formats"), default=False)

    service = models.ForeignKey(Service,verbose_name=u'Сервис',blank=True,null=True,help_text=u'Выбираем,в случае если категория ссылается на сервис')
    level = models.PositiveSmallIntegerField(default=1)
    top_position = models.IntegerField(u'Сортировка',blank=True, null=True)
    display = models.BooleanField(u"Показывать", default=True,help_text=u'Управление активностью категории')
    display_on_top = models.BooleanField(u"Показывать в шапке", default=False,help_text=u'Верхнее меню категорий')
    display_on_left = models.BooleanField(u"Показывать слева на главной", default=False,help_text=u'Левое меню.Подкатегории')
    this_is_text_cat = models.BooleanField(u"Категория сервиса", default=False,help_text=u'Если категория ведет не на каталог,а в текстовую страницу')

    uid = models.CharField(u'ID группы', max_length=50, blank=True,help_text=u'Служебная информация',null=True)
    puid = models.CharField(u'ID родителя', max_length=50, blank=True,help_text=u'Служебная информация',null=True)
    
    #photos = generic.GenericRelation("filesandimages.AttachedImage", verbose_name=_(u"Images"), object_id_field="content_id", content_type_field="content_type")
    css = models.CharField(u'CSS класс', max_length=200, blank=True,help_text=u'Служебная информация',null=True)
    photo = models.ImageField(u'Картинка',upload_to='categories',blank=True,null=True,help_text=u'Изображение категории')
    photo_title = models.CharField(u'Тайтл картинки', max_length=200, blank=True,null=True)
    photo_alt = models.CharField(u'alt картинки', max_length=200, blank=True,null=True)
    hide_price = models.BooleanField(u'Скрыть цену',default=False,help_text=u'Скрывать цену для товаров данной категории')

    partners = models.ManyToManyField(Partners, verbose_name=_(u"Партнеры"), blank=True,null=True)


    seo_title = models.CharField(u'seo title', max_length=200, blank=True, null=True)
    seo_description = models.CharField(u'Description', max_length=200, blank=True, null=True)

    prioritet = models.CharField(u"Приоритет", blank=True,null=True,max_length=200)


    class Meta:
        verbose_name = u'Категория товара'
        verbose_name_plural = u'Категории товаров'


    def __unicode__(self):
        return self.name

    @property
    def content_type(self):
        """Returns the content type of the category as lower string.
        """
        return u"category"

    def save(self, *args, **kwargs):
        super(Category, self).save(*args, **kwargs)
        #tmp_slug = slugify(unidecode.unidecode(self.name))
        #self.slug = tmp_slug
        #super(Category, self).save(*args, **kwargs)
        all_childs = self.get_all_children()
        if self.template is not None and self.is_share:
            for child in all_childs:
                child.template = self.template
                child.save()

    def get_seo_title(self):
        if self.seo_title:
            return self.seo_title
        return self.name.lower().capitalize()

    def get_seo_description(self):
        if self.seo_description:
            return self.seo_description
        return ''

    def get_all_children(self):
        """Returns all child categories of the category.
        """

        def _get_all_children(category, children):
            for category in Category.objects.filter(parent=category.id):
                children.append(category)
                _get_all_children(category, children)

        children = []
        for category in Category.objects.filter(parent=self.id):
            children.append(category)
            _get_all_children(category, children)
        return children

    def get_images(self):
        """Returns all images of the category, including the main image.
        """
        images = self.photos.all()
        return images

    def get_image(self):
        """Returns the first image (the main image) of the product.
        """
        try:
            return self.get_images()[0].image
        except IndexError:
            return None

    def get_parents(self):

        parents = []
        category = self.parent
        while category is not None:
            parents.append(category)
            category = category.parent

        return parents

    def get_products(self):

        products = self.products.filter(active=True)

        return products

    def get_all_products(self):
        categories = [self]
        categories.extend(self.get_all_children())

        products = Product.objects.filter(active=True, categories__in=categories).distinct()

        return products

    def category_display_or_not(self):
        if len(self.get_products()) == 0:
            return False
        else:
            return True

    def has_products(self):
        if len(self.products.all()) > 0:
            return True
        else:
            return False
    def get_filtered_products(self, filters, sorting):
        """Returns products for this category filtered by passed filters sorted
        by passed sorted
        """
        return plugins.catalog.utils.get_filtered_products_for_category(
            self, filters, sorting)

    def get_static_block(self):
        block = self.static_block

        return block

    # 3rd party contracts
    def get_parent_for_portlets(self):
        """Returns the parent for portlets.
        """
        # TODO: Circular imports
        import lfs.core.utils

        return self.parent or lfs.core.utils.get_default_shop()


    def is_ext_template(self):
        """method to return the path of the category template
        """
        if self.template:
            return True
        return False

    def get_content(self):
        """try to find out which type of content the template is rendering,
        depending on its path.
        """
        if self.get_template_name() == None:
            return CONTENT_PRODUCTS
        if self.get_template_name().startswith(CAT_CATEGORY_PATH): # do we have category - templates
            return CONTENT_CATEGORIES
        return CONTENT_PRODUCTS

    def get_partners(self):
        return self.partners.all()

    @models.permalink
    def get_absolute_url(self):
        return ("catalog_product_level", (), {"slug": self.slug,})

signals.post_save.connect(checked_chilren_elements, sender=Category)





class CategoriesSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.7

    def items(self):
        return Category.objects.filter(display=True).exclude(this_is_text_cat=True)


class Country(models.Model):
    name = models.CharField(verbose_name=u'Страна',max_length=300)
    uuid = models.CharField(verbose_name=u'UUID',max_length=800)
    externalcode = models.CharField(verbose_name=u'externalcode',max_length=800)

    class Meta:
        ordering = ("name", )
        verbose_name = u'Страна'
        verbose_name_plural = u'Страны'

    def __unicode__(self):
        return "%s" % (self.name)

class Product(MPTTModel):

    name = models.CharField(_(u"Name"), max_length=255, blank=True)
    slug = AutoSlugField(_(u"Slug"), unique=True, populate_from='name',unique_with='sku',max_length=255, editable=True)
    sku = models.CharField(_(u"SKU"), blank=True, max_length=30,null=True)
    price = models.FloatField(_(u"Price"), default=0.0, help_text=u'не трогаем.заполняется автоматически')
    effective_price = models.FloatField(_(u"Price"), blank=True)
    price_unit = models.CharField(blank=True, max_length=20)
    unit = models.CharField(_(u"единицы"), blank=True, max_length=20, help_text=_(u'единицы измерения, например: "шт.", "по 10 в упаковке" и т.п.'))
    short_description = models.TextField(_(u"Short description"), blank=True)
    description = models.TextField(_(u"Description"), blank=True)
    images = generic.GenericRelation("filesandimages.AttachedImage", verbose_name=_(u"Images"),
                                     object_id_field="content_id", content_type_field="content_type")
    stock_amount = models.FloatField(_(u"Stock amount"), default=0,help_text=u'не трогаем.заполняется автоматически')

    related_products = models.ManyToManyField("self", verbose_name=_(u"Вас заинтересуют"), blank=True, null=True,
                                              symmetrical=False, related_name="reverse_related_products",limit_choices_to = {'active':True})

    for_sale = models.BooleanField(_(u"Распродажа"), default=False)
    for_sale_price = models.FloatField(_(u"Цена распродажи"), default=0.0)
    active = models.BooleanField(_(u"Active"), default=True)
    creation_date = models.DateTimeField(_(u"Creation date"), auto_now_add=True)

    # Stocks
    deliverable = models.BooleanField(_(u"Deliverable"), default=True)
    manual_delivery_time = models.BooleanField(_(u"Manual delivery time"), default=False)
    ordered_at = models.DateField(_(u"Ordered at"), blank=True, null=True)
    manage_stock_amount = models.BooleanField(_(u"Manage stock amount"), default=True)

    active_packing_unit = models.BooleanField(_(u"Active packing unit"), default=False)
    packing_unit = models.FloatField(_(u"Packing unit"), blank=True, null=True)
    packing_unit_unit = models.CharField(_(u"Unit"), blank=True, max_length=30)

    static_block = models.ForeignKey("StaticBlock", verbose_name=_(u"Static block"), blank=True, null=True,
                                     related_name="products")

    # Standard Products
    sub_type = models.CharField(_(u"Subtype"),
                                max_length=10, choices=PRODUCT_TYPE_CHOICES, default=STANDARD_PRODUCT,help_text=u'Для разграничения и удобства')

    # Varianted Products
    default_variant = models.ForeignKey("self", verbose_name=_(u"Default variant"), blank=True, null=True)
    variants_display_type = models.IntegerField(_(u"Variants display type"),
                                                choices=VARIANTS_DISPLAY_TYPE_CHOICES, default=LIST)

    # Product Variants
    variant_position = models.IntegerField(default=999)
    parent = models.ForeignKey("self", blank=True, null=True, verbose_name=u'Родитель', related_name="variants",help_text=u'Выбрать, если тип - товар с вариантами')
    active_name = models.BooleanField(_(u"Active name"), default=False)
    active_sku = models.BooleanField(_(u"Active SKU"), default=False)
    active_short_description = models.BooleanField(_(u"Active short description"), default=False)
    active_static_block = models.BooleanField(_(u"Active static bock"), default=False)
    active_description = models.BooleanField(_(u"Active description"), default=False)
    active_price = models.BooleanField(_(u"Active price"), default=False)
    active_for_sale = models.PositiveSmallIntegerField(_("Active for sale"), choices=ACTIVE_FOR_SALE_CHOICES,
                                                       default=ACTIVE_FOR_SALE_STANDARD)
    active_for_sale_price = models.BooleanField(_(u"Active for sale price"), default=False)
    active_images = models.BooleanField(_(u"Active Images"), default=False)
    active_related_products = models.BooleanField(_(u"Active related products"), default=False)
    active_accessories = models.BooleanField(_(u"Active accessories"), default=False)
    active_dimensions = models.BooleanField(_(u"Active dimensions"), default=False)
    template = models.PositiveSmallIntegerField(_(u"Product template"), blank=True, null=True, max_length=400,
                                                choices=PRODUCT_TEMPLATES)

    # Price calculation
    active_price_calculation = models.BooleanField(_(u"Active price calculation"), default=False)
    price_calculation = models.CharField(_(u"Price Calculation"), blank=True, max_length=100)

    # Manufacturer
    manufacturer = models.ForeignKey(Manufacturer,verbose_name=u'Фирма', blank=True, null=True,related_name="manufacturer")


    uid = models.CharField(u'Внешний код', max_length=50, blank=True,help_text=u'заполняется автоматически при синхронизации')
    catuid = models.CharField(u'Категория', max_length=50, blank=True,help_text=u'заполняется автоматически при синхронизации')
    average_score = models.FloatField(blank=True, null=True)

    # для связывания товара с категориями и свойствами
    categories = models.ManyToManyField("Category", blank=True, through='ProductCategory',verbose_name=u'Категории')
  
    # аналоги
    is_analog = models.BooleanField(default=False, editable=False)

    # Показ на главной
    is_new = models.BooleanField(u"Это новинка", default=False)
    is_hit = models.BooleanField(u"Акция", default=False)
   
    # Бесплатная доставка
    share = models.BooleanField(u'Бесплатная доставка', default=False)

    #likes = generic.GenericRelation("likes.Like", verbose_name=u"Likes",
                                   # object_id_field="content_id", content_type_field="content_type")


    seo_title = models.CharField(u'seo title', max_length=200, blank=True, null=True)
    seo_description = models.CharField(u'Description', max_length=200, blank=True, null=True)

    pdf = models.FileField(upload_to='pdf',verbose_name=u'PDF описание',blank=True,null=True)
    objects = ActiveManager()
    
    class Meta:
        ordering = ("name", )
        verbose_name = u'Товар трактороцентра'
        verbose_name_plural = u'Товары трактороцентра'

    def __unicode__(self):
        return "%s" % self.name



    @models.permalink
    def get_absolute_url(self):
        slug = self.slug
        if self.is_variant():
            slug = self.parent.slug
        return ("product", (), {"slug": slug})

    @property
    def get_change_url(self):
        return urlresolvers.reverse('admin:catalog_product_change', args=(self.pk,))

    @property
    def content_type(self):
        """Returns the content type of the product as lower string.
        """
        return u"product"


    def get_accessories(self):
        """Returns the ProductAccessories relationship objects - not the
        accessory (Product) objects.

        This is necessary to have also the default quantity of the relationship.
        """
        if self.is_variant() and not self.active_accessories:
            product = self.parent
        else:
            product = self

        pas = []
        for pa in ProductAccessories.objects.filter(product=product):
            if pa.accessory.is_active():
                pas.append(pa)

        return pas

    def has_accessories(self):
        """Returns True if the product has accessories.
        """
        return len(self.get_accessories()) > 0

    def get_categories(self, with_parents=False):
        """Returns the categories of the product.
        """
        object = self

        if with_parents:
            categories = []
            for category in object.categories.all():
                while category:
                    categories.append(category)
                    category = category.parent
            categories = categories
        else:
            categories = object.categories.all()
        return categories

    def get_category(self):
        """Returns the first category of a product.
        """

        try:
            return self.get_categories()[0]
        except IndexError:
            return None

    def product_have_hidden_cat(self):
        is_hidden = False
        cats = self.get_categories()
        for cat in cats:
            if cat.hide_price:
                is_hidden = True

        if not self.stock_amount > 0 or not self.price > 0:
            is_hidden = True
        return is_hidden

    def get_last_category(self):
        for cat in self.get_categories():
            if cat.is_leaf_node():
                return cat
        return None

    def get_parent_category(self):
        object = self
        try:
            return object.categories.filter(parent=None)
        except IndexError:
            return None

    def get_child_category(self):
        if self.is_variant():
            object = self.parent
        else:
            object = self

        try:
            return object.categories.all()
        except IndexError:
            return None

    def get_description(self):
        """Returns the description of the product. Takes care whether the
        product is a variant and description is active or not.
        """
        if self.is_variant():
            if self.active_description:
                description = self.description
                description = description.replace("%P", self.parent.description)
            else:
                description = self.parent.description
        else:
            description = self.description

        return description

    # TODO: Check whether there is a test case for that and write one if not.
    def get_for_sale(self):
        """Returns true if the product is for sale. Takes care whether the
        product is a variant.
        """
        if self.is_variant():
            if self.active_for_sale == ACTIVE_FOR_SALE_STANDARD:
                return self.parent.for_sale
            elif self.active_for_sale == ACTIVE_FOR_SALE_YES:
                return True
            else:
                return False
        else:
            return self.for_sale

    def get_short_description(self):
        """Returns the short description of the product. Takes care whether the
        product is a variant and short description is active or not.
        """
        if self.is_variant() and not self.active_short_description:
            return self.parent.short_description
        else:
            return self.short_description

    def get_image(self):
        """Returns the first image (the main image) of the product.
        """
        try:
            return self.get_images()[0].image
        except IndexError:
            return None
#            cache_key = 'default_image'
#            default_image = cache.get(cache_key)
#            if default_image is None:
#                default_image = Image.objects.filter(title='default_image')[0].image
#            return default_image

    def get_image_or_none(self):
        """Returns the first image (the main image) of the product or None
        """
        try:
            return self.get_images()[0].image
        except IndexError:
            return None

    def get_image_tag(self):
        image = self.get_image_or_none()
        if image:
            try:
                thumbnail = get_thumbnail(image, "60x60")
                return '<a href="%s" target="blank"><img style="max-height:60px;max-width:60px" src="%s"/></a>' % (
                    image.url, thumbnail.url)
            except IOError:
                return 'No such file'
        return ''

    get_image_tag.short_description = u'Изображение'
    get_image_tag.allow_tags = True

    def get_images(self):
        """Returns all images of the product, including the main image.
        """
        cache_key = "product-images-%s" % self.id
        images = cache.get(cache_key)

        if images is None:
            images = []
            if self.is_variant() and not self.active_images:
                object = self.parent
            else:
                object = self

            images = object.images.all()
            cache.set(cache_key, images)

        return images

    def get_sub_images(self):
        """Returns all images of the product, except the main image.
        """
        return self.get_images()[1:]

    def get_short_title(self):
        origin_title = self.name
        skobka_pos = origin_title.find("(")
        if skobka_pos:
            short_title = origin_title[:skobka_pos]
        else:
            short_title = origin_title
        return short_title



    # TODO: Check whether there is a test case for that and write one if not.
    def get_name(self):
        """Returns the name of the product. Takes care whether the product is a
        variant and name is active or not.
        """
        if self.is_variant():
            if self.active_name:
                name = self.name
                name = name.replace("%P", self.parent.name)
            else:
                name = self.parent.name
        else:
            name = self.name

        return name

    def get_option(self, property_id):
        """Returns the id of the selected option for property with passed id.
        """
        options = cache.get("productpropertyvalue%s" % self.id)
        if options is None:
            options = {}
            for pvo in self.property_values.all():
                options[pvo.property_id] = pvo.value
            cache.set("productpropertyvalue%s" % self.id, options)
        try:
            return options[property_id]
        except KeyError:
            return None

    def get_displayed_properties(self):
        """Returns properties with ``display_on_product`` is True.
        """
        cache_key = "displayed-properties-%s" % self.id

        properties = cache.get(cache_key)
        if properties:
            return properties

        properties = []
        for ppv in self.property_values.filter(property__display_on_product=True).order_by("property__position"):
            if ppv.property.is_select_field:
                try:
                    po = PropertyOption.objects.get(pk=int(float(ppv.value)))
                except (PropertyOption.DoesNotExist, ValueError):
                    continue
                else:
                    value = po.name
            else:
                value = ppv.value
            properties.append({
                "name": ppv.property.name,
                "title": ppv.property.title,
                "value": value,
                "unit": ppv.property.unit,
                })

        cache.set(cache_key, properties)
        return properties

    def get_various_properties(self):
        self_defined_props_ids = self.property_values.values('property').distinct()
        various_props_ids = ProductPropertyValue.objects.filter(parent_id=self.pk).exclude(
            property__pk__in=self_defined_props_ids).values('property').distinct()
        return Property.objects.filter(pk__in=various_props_ids)

    def get_variants_ids(self, values=None):
        if values:
            variants = self.get_variants()
            for value in values:
                variants = variants.filter(property_values__value=value)
            variants_ids = variants.values_list('pk', flat=True)
        else:
            variants_ids = self.get_variants().values_list('pk', flat=True)
        return variants_ids

    def get_property_variants(self, prop, values=None):
        values = values or []
        try:
            values = [int(value) for value in values]
        except TypeError:
            pass

        variants_ids = self.get_variants_ids(values)
        pvs = ProductPropertyValue.objects.filter(product__pk__in=variants_ids, property=prop)
        pvs = pvs.values('property', 'value').distinct()
        result = []
        for pv in pvs:
            if prop.is_select_field:
                option = prop.options.get(pk=pv['value'])
                selected = False
                if option.pk in values:
                    selected = True
                result.append({'name': option.name, 'value': option.pk, 'selected': selected})
            else:
                result.append({'name': pv.value, 'value': pv.value})
        return  result

    def get_various_properties_with_options(self):
        if self.has_variants():
            props = list(self.get_various_properties())
            for prop in props:
                prop.opts = self.get_property_variants(prop)
            return props
        return ''

    def get_variant_properties(self):
        """Returns the property value of a variant in the correct ordering
        of the properties.
        """
        cache_key = "variant-properties-%s" % self.id

        properties = cache.get(cache_key)
        if properties:
            return properties

        properties = []

        for ppv in self.property_values.filter(type=PROPERTY_VALUE_TYPE_VARIANT).order_by("property__position"):
            if ppv.property.is_select_field:
                try:
                    po = PropertyOption.objects.get(pk=int(float(ppv.value)))
                except PropertyOption.DoesNotExist:
                    continue
                else:
                    value = po.name
            else:
                value = ppv.value
            properties.append({
                "name": ppv.property.name,
                "title": ppv.property.title,
                "value": value,
                "unit": ppv.property.unit,
                })

        cache.set(cache_key, properties)

        return properties

    def has_option(self, property, option):
        """Returns True if the variant has the given property / option
        combination.
        """
        options = cache.get("productpropertyvalue%s" % self.id)
        if options is None:
            options = {}
            for pvo in self.property_values.all():
                options[pvo.property_id] = pvo.value
            cache.set("productpropertyvalue%s" % self.id, options)

        try:
            return options[property.id] == str(option.id)
        except KeyError:
            return False

    def _get_default_properties_price(self, object):
        """Returns the total price of all default properties.
        """
        price = 0
        for property in object.get_configurable_properties():
            if property.add_price:
                # Try to get the default value of the property
                try:
                    ppv = ProductPropertyValue.objects.get(product=self, property=property,
                                                           type=PROPERTY_VALUE_TYPE_DEFAULT)
                    po = PropertyOption.objects.get(pk=ppv.value)
                except (ObjectDoesNotExist, ValueError):
                    # If there is no explicit default value try to get the first
                    # option.
                    if property.required:
                        try:
                            po = property.options.all()[0]
                        except IndexError:
                            continue
                        else:
                            price += po.price
                else:
                    price += po.price

        return price

    def get_price(self):
        price = self.price
        return price

    def hide_price(self):
        if self.product_have_hidden_cat():
            return True
        return False

    def get_global_properties(self):
        """Returns all global properties for the product.
        """
        properties = []
        for property_group in self.property_groups.all():
            properties.extend(property_group.properties.all())

        return properties

    def get_local_properties(self):
        """Returns local properties of the product
        """
        return self.properties.order_by("productspropertiesrelation")

    def get_properties(self):
        """Returns local and global properties
        """
        properties = self.get_global_properties()
        properties.extend(self.get_local_properties())

        properties.sort(lambda a, b: cmp(a.position, b.position))

        return properties

    def get_property_select_fields(self):
        """Returns all properties which are `select types`.
        """
        # global
        properties = []
        for property_group in self.property_groups.all():
            properties.extend(
                property_group.properties.filter(type=PROPERTY_SELECT_FIELD).order_by("groupspropertiesrelation"))

        # local
        for property in self.properties.filter(type=PROPERTY_SELECT_FIELD).order_by("productspropertiesrelation"):
            properties.append(property)

        return properties

    def get_configurable_properties(self):
        """Returns all properties which are configurable.
        """
        # global
        properties = []
        for property_group in self.property_groups.all():
            properties.extend(property_group.properties.filter(configurable=True).order_by("groupspropertiesrelation"))

        # local
        for property in self.properties.filter(configurable=True).order_by("productspropertiesrelation"):
            properties.append(property)

        return properties

    def get_sku(self):
        """Returns the sku of the product. Takes care whether the product is a
        variant and sku is active or not.
        """
        if self.is_variant() and not self.active_sku:
            return self.parent.sku
        else:
            return self.sku

    def has_related_products(self):
        """Returns True if the product has related products.
        """
        return len(self.get_related_products()) > 0

    def get_related_products(self):
        """Returns the related products of the product.
        """
        cache_key = "related-products-%s" % self.id
        related_products = cache.get(cache_key)

        if related_products is None:
            if self.is_variant() and not self.active_related_products:
                related_products = self.parent.related_products.all()
            else:
                related_products = self.related_products.all()

            cache.set(cache_key, related_products)

        return related_products

    def get_default_variant(self):
        """Returns the default variant.

        This is either a selected variant or the first added variant. If the
        product has no variants it is None.
        """
        cache_key = "default-variant-%s" % self.id
        default_variant = cache.get(cache_key)

        if default_variant is not None:
            return default_variant

        if self.default_variant is not None:
            default_variant = self.default_variant
        else:
            try:
                default_variant = self.variants.filter(active=True)[0]
            except IndexError:
                return None

        cache.set(cache_key, default_variant)
        return default_variant

    def get_static_block(self):
        """Returns the static block of the product. Takes care whether the
        product is a variant and meta description are active or not.
        """
        cache_key = "product-static-block-%s" % self.id
        block = cache.get(cache_key)
        if block is not None:
            return block

        if self.is_variant() and not self.active_static_block:
            block = self.parent.static_block
        else:
            block = self.static_block

        cache.set(cache_key, block)

        return block

    def get_variants(self):
        """Returns the variants of the product.
        """
        return self.variants.filter(active=True).order_by("variant_position")

    def has_variants(self):
        """Returns True if the product has variants.
        """
        return len(self.get_variants()) > 0

    def get_variant(self, options):
        """Returns the variant with the given options or None.

        The format of the passed properties/options must be tuple as following:

            [property.id|option.id]
            [property.id|option.id]
            ...

        NOTE: These are strings as we get the properties/options pairs out of
        the request and it wouldn't make a lot of sense to convert them to
        objects and back to strings.
        """
        options.sort()
        options = "".join(options)
        for variant in self.variants.filter(active=True):
            temp = variant.property_values.all()
            temp = ["%s|%s" % (x.property.id, x.value) for x in temp]
            temp.sort()
            temp = "".join(temp)

            if temp == options:
                return variant

        return None

    def has_variant(self, options):
        """Returns true if a variant with given options already exists.
        """
        if self.get_variant(options) is None:
            return False
        else:
            return True

    # Dimensions
    def get_weight(self):
        """Returns weight of the product. Takes care whether the product is a
        variant and meta description are active or not.
        """
        if self.is_variant() and not self.active_dimensions:
            return self.parent.weight
        else:
            return self.weight

    def get_width(self):
        """Returns width of the product. Takes care whether the product is a
        variant and meta description are active or not.
        """
        if self.is_variant() and not self.active_dimensions:
            return self.parent.width
        else:
            return self.width

    def get_length(self):
        """Returns length of the product. Takes care whether the product is a
        variant and meta description are active or not.
        """
        if self.is_variant() and not self.active_dimensions:
            return self.parent.length
        else:
            return self.length

    def get_height(self):
        """Returns height of the product. Takes care whether the product is a
        variant and meta description are active or not.
        """
        if self.is_variant() and not self.active_dimensions:
            return self.parent.height
        else:
            return self.height

    def get_packing_info(self):
        """Returns the packing info of the product as list:
        """
        if self.is_variant():
            obj = self.parent
        else:
            obj = self

        return (obj.packing_unit, obj.packing_unit_unit)

    def is_standard(self):
        """Returns True if product is standard product.
        """
        return self.sub_type == STANDARD_PRODUCT

    def is_configurable_product(self):
        """Returns True if product is configurable product.
        """
        return self.sub_type == CONFIGURABLE_PRODUCT

    def is_product_with_variants(self):
        """Returns True if product is product with variants.
        """
        return self.sub_type == PRODUCT_WITH_VARIANTS

    def is_variant(self):
        """Returns True if product is variant.
        """
        return self.sub_type == VARIANT

    def is_active(self):
        """Returns the activity state of the product.
        """
        if self.is_variant():
            return self.active and self.parent.active
        else:
            return self.active

    def is_deliverable(self):
        """Returns the deliverable state of the product.
        """
        if self.is_variant():
            return self.parent.deliverable
        else:
            return self.deliverable

    def is_available(self):
        if self.stock_amount > 0:
            return True
        else:
            return False

    def is_wiki_mart(self):
        if self.to_wiki_mart is not None:
            return self.to_wiki_mart

        cache_key = 'to_wiki_mart_from_categories_%s' % self.pk
        to_wiki = cache.get(cache_key)

        if to_wiki is None:
            to_wiki = bool(sum((x for x in self.categories.values_list('to_wiki_mart', flat=True))))
            cache.set(cache_key, to_wiki)

        return to_wiki

    # 3rd party contracts
    def get_parent_for_portlets(self):
        """Returns the current category. This will add the portlets of the
        current category to the product portlets.
        """
        if self.is_variant():
            return self.parent
        else:
            # TODO Return the current category
            try:
                return self.categories.all()[0]
            except:
                return None

    def get_template_name(self):
        """
        method to return the path of the product template
        """
        if self.template is not None:
            id = int(self.template)
            return PRODUCT_TEMPLATES[id][1]["file"]
        return None

    def get_sale_percentage(self):
        if self.for_sale_price:
            return int((1 - self.for_sale_price / self.price) * 100)
        return None

    def is_topseller(self):
        if self.topseller_set.count() > 0:
            return True
        return False


    def try_create_product_prop_value(self, option, confirm=False):
        try:
            self.property_values.create(property=option.property, value=option.pk, type=0, confirm=confirm)
        except IntegrityError:
            pass


    def get_seo_title(self):
        if self.seo_title:
            return self.seo_title
        return self.name.lower().capitalize()

    def get_seo_description(self):
        if self.seo_description:
            return self.seo_description
        return ''

class ProductAttribute(models.Model):
    uuid = models.CharField(verbose_name=u'ID в мойсклад',max_length=300)
    name = models.CharField(verbose_name=u'Название',max_length=300)

    class Meta:
        verbose_name = u'Свойство товараs'
        verbose_name_plural = u'Свойства товараs'

    def __unicode__(self):
        return "%s" % (self.name)
    


class ProductsSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.6

    def items(self):
        return Product.objects.filter(active=True).exclude(sub_type=VARIANT).distinct()



class ProductAccessories(models.Model):
    product = models.ForeignKey("Product", verbose_name=_(u"Product"), related_name="productaccessories_product")
    accessory = models.ForeignKey("Product", verbose_name=_(u"Accessory"), related_name="productaccessories_accessory")
    position = models.IntegerField(_(u"Position"), default=999)
    quantity = models.FloatField(_(u"Quantity"), default=1)

    class Meta:
        ordering = ("position", )
        verbose_name_plural = "Product accessories"

    def __unicode__(self):
        return "%s -> %s" % (self.product.name, self.accessory.name)

    def get_price(self):
        """Returns the total price of the accessory based on the product price
        and the quantity in which the accessory is offered.
        """
        return self.accessory.get_price() * self.quantity


class PropertyGroup(models.Model):
    """Groups product properties together.

    Can belong to several products, products can have several groups

    **Attributes**:

    name
        The name of the property group.

    products
          The assigned products of the property group.
    """
    name = models.CharField(blank=True, max_length=50)
    products = models.ManyToManyField(Product, verbose_name=_(u"Products"), related_name="property_groups", blank=True)
    category = models.ForeignKey(Category,verbose_name=u'Категория',blank=True,null=True)
    
    class Meta:
        ordering = ("name", )
        verbose_name = u'Свойство'
        verbose_name_plural = u'Свойства'

    def __unicode__(self):
        return self.name

    def get_configurable_properties(self):
        """Returns all configurable properties of the property group.
        """
        return self.properties.filter(configurable=True)

    def get_filterable_properties(self):
        """Returns all filterable properties of the property group.
        """
        return self.properties.filter(filterable=True)





class Property(models.Model):
   
    name = models.CharField(_(u"Name"), max_length=100, help_text=u'Латинскими буквами')
    title = models.CharField(_(u"Title"), max_length=100, help_text=u'Отображаемое название')
    groups = models.ManyToManyField(PropertyGroup, verbose_name=_(u"Group"), blank=True, null=True,
                                    through="GroupsPropertiesRelation", related_name="properties")
    products = models.ManyToManyField(Product, verbose_name=_(u"Products"), blank=True, null=True,
                                      through="ProductsPropertiesRelation", related_name="properties")
    position = models.IntegerField(_(u"Position"), blank=True, null=True)
    unit = models.CharField(_(u"Unit"), blank=True, max_length=15)
    display_on_product = models.BooleanField(_(u"Display on product"), default=True)
    local = models.BooleanField(default=False)
    filterable = models.BooleanField(default=True,verbose_name=u'Выводить в фильтре')
    display_no_results = models.BooleanField(_(u"Display no results"), default=False)
    configurable = models.BooleanField(default=False)
    type = models.PositiveSmallIntegerField(_(u"Type"), choices=PROPERTY_FIELD_CHOICES, default=PROPERTY_SELECT_FIELD)
    price = models.FloatField(_(u"Price"), blank=True, null=True)
    display_price = models.BooleanField(_(u"Display price"), default=True)
    add_price = models.BooleanField(_(u"Add price"), default=True)

    # Number input field
    unit_min = models.FloatField(_(u"Min"), blank=True, null=True)
    unit_max = models.FloatField(_(u"Max"), blank=True, null=True)
    unit_step = models.FloatField(_(u"Step"), blank=True, null=True)
    decimal_places = models.PositiveSmallIntegerField(_(u"Decimal places"), default=0)

    required = models.BooleanField(default=False)

    step_type = models.PositiveSmallIntegerField(_(u"Step type"), choices=PROPERTY_STEP_TYPE_CHOICES,
                                                 default=PROPERTY_STEP_TYPE_AUTOMATIC)
    step = models.IntegerField(_(u"Step"), blank=True, null=True)

    uid = models.CharField(max_length=50, blank=True)

    class Meta:
        verbose_name = u'Значение свойства'
        verbose_name_plural = u'Значения свойства'
        ordering = ["position"]

    def __unicode__(self):
        return self.title

    @property
    def is_select_field(self):
        return self.type == PROPERTY_SELECT_FIELD

    @property
    def is_text_field(self):
        return self.type == PROPERTY_TEXT_FIELD

    @property
    def is_number_field(self):
        return self.type == PROPERTY_NUMBER_FIELD

    @property
    def is_range_step_type(self):
        return self.step_type == PROPERTY_STEP_TYPE_FIXED_STEP

    @property
    def is_automatic_step_type(self):
        return self.step_type == PROPERTY_STEP_TYPE_AUTOMATIC

    @property
    def is_steps_step_type(self):
        return self.step_type == PROPERTY_STEP_TYPE_MANUAL_STEPS

    def is_valid_value(self, value):
        """Returns True if given value is valid for this property.
        """
        if self.is_number_field:
            try:
                float(value)
            except ValueError:
                return False
        return True


class FilterStep(models.Model):
    property = models.ForeignKey(Property, verbose_name=_(u"Property"), related_name="steps")
    start = models.FloatField()

    class Meta:
        ordering = ["start"]

    def __unicode__(self):
        return "%s %s" % (self.property.name, self.start)


class GroupsPropertiesRelation(models.Model):

    group = models.ForeignKey(PropertyGroup, verbose_name=_(u"Group"), related_name="groupproperties")
    property = models.ForeignKey(Property, verbose_name=_(u"Property"))
    position = models.IntegerField(_(u"Position"), default=999)
    #no_yandex = models.BooleanField(verbose_name=u'Не выводить в яндекс и фильтр',help_text=u'Будет показываться только в характеристиках товара в карточке')

    class Meta:
        verbose_name = u'Группа свойств'
        verbose_name_plural = u'Группы свойств'
        ordering = ("position", )
        unique_together = ("group", "property")
        auto_created = Property


class ProductsPropertiesRelation(models.Model):
    product = models.ForeignKey(Product, verbose_name=_(u"Product"), related_name="productsproperties")
    property = models.ForeignKey(Property, verbose_name=_(u"Property"))
    position = models.IntegerField(_(u"Position"), default=999)

    class Meta:
        ordering = ("position", )
        unique_together = ("product", "property")


class PropertyOption(models.Model):
    property = models.ForeignKey(Property, verbose_name=_(u"Property"), related_name="options")

    name = models.CharField(_(u"Name"), max_length=100)
    price = models.FloatField(_(u"Price"), blank=True, null=True, default=0.0)
    position = models.IntegerField(_(u"Position"), default=99)
    description = models.TextField(_(u"Description"), blank=True, help_text=MARKDOWN_HELP_TEXT)
    short = models.CharField(verbose_name = u'Короткое название', blank=True, null=True,max_length=250)

    uid = models.CharField(max_length=50, blank=True)
    childs = models.ManyToManyField('self', blank=True, symmetrical=False, related_name='parents')
    date = models.DateField(blank=True, null=True)

    images = generic.GenericRelation("filesandimages.AttachedImage", verbose_name=_(u"Images"),
                                     object_id_field="content_id", content_type_field="content_type")

    class Meta:
        ordering = ["position"]
        verbose_name = u'Значение свойства'
        verbose_name_plural = u'Значения свойств'

    def childs_to_string(self):
        return ', '.join([dict.get('name') for dict in self.childs.values()])

    def childs_ids(self):
        return ', '.join([dict.get('id') for dict in self.childs.values()])


    def parents_to_string(self):
        return ', '.join([dict.get('name') for dict in self.parents.values()])

    def __unicode__(self):
        return self.name

    def get_image(self):
        """Returns the first image (the main image) of the product.
        """
        try:
            return self.images.all()[0].image
        except IndexError:
            return None


class ProductPropertyValue(models.Model):
    product = models.ForeignKey(Product, verbose_name=_(u"Product"), related_name="property_values")
    parent_id = models.IntegerField(_(u"Parent"), blank=True, null=True)
    property = models.ForeignKey("Property", verbose_name=_(u"Property"), related_name="property_values")
    value = models.CharField(_(u"Value"), blank=True, max_length=100)
    value_as_float = models.FloatField(_(u"Value as float"), blank=True, null=True)
    type = models.PositiveSmallIntegerField(_(u"Type"), choices=PROPERTY_VALUE_TYPE_CHOICES,
                                            default=PROPERTY_VALUE_TYPE_FILTER)
    # Поле хранит состояние прошла ли связь модерацию или нет
    confirm = models.NullBooleanField(u'Подтверждено', null=True)

    class Meta:
        unique_together = ("product", "property", "value", "type")

    def __unicode__(self):
        return "%s/%s: %s" % (self.product.name, self.property.name, self.value)

    def save(self, force_insert=False, force_update=False, *args, **kw):
        """Overwritten to save the parent id for variants. This is used to count
        the entries per filter. See catalog/utils/get_product_filters for more.
        """
        if self.product.is_variant():
            self.parent_id = self.product.parent.id
        else:
            self.parent_id = self.product.id

        try:
            float(self.value)
        except ValueError:
            pass
        else:
            self.value_as_float = self.value

        super(ProductPropertyValue, self).save(force_insert, force_update, *args, **kw)


def update_or_create_pv(value, product, prop):
    pv = get_object_or_None(ProductPropertyValue, property=prop, product=product)
    if isinstance(value, (str, unicode)):
        value = value.lstrip()
    if value:
        if pv:
            pv.value = value
            pv.save()
        else:
            pv = product.property_values.create(property=prop, value=value, type=PROPERTY_VALUE_TYPE_FILTER)
            pv.save()
    elif pv:
        pv.delete()

class StaticBlock(models.Model):
    name = models.CharField(_(u"Name"), max_length=30)
    display_files = models.BooleanField(_(u"Display files"), default=True)
    html = models.TextField(_(u"HTML"), blank=True, help_text=MARKDOWN_HELP_TEXT)
    files = generic.GenericRelation("filesandimages.AttachedFile", verbose_name=_(u"Files"),
                                    object_id_field="content_id", content_type_field="content_type",related_query_name='generic_images')

    def __unicode__(self):
        return self.name


class ProductCategory(models.Model):
    category = models.ForeignKey(Category)
    product = models.ForeignKey(Product)

    class Meta:
        db_table = 'catalog_product_category'
        auto_created = Product


class TechAttrs(models.Model):
    name = models.CharField(_(u'заголовок'), max_length=255)

    def __unicode__(self):
        return  self.name

    class Meta:
        verbose_name = u'Значение'
        verbose_name_plural = u'Значения'


class ProductTechInfo(models.Model):
    """техническая информация для продуктов
    """
    product = models.ForeignKey(Product, verbose_name=_(u"товар"), related_name="techinfobad")
    name = models.ForeignKey(TechAttrs, verbose_name=_(u"Характеристика"), related_name="ProductTechInfoBad")
    value_full = models.TextField(_(u'Элемент'), blank=True)
    value = models.TextField(_(u'Описание'), blank=True)
    position = models.SmallIntegerField(_(u'позиция'), default=999)

    def __unicode__(self):
        return  u"%s" % (self.name)

    class Meta:
        ordering = ('position',)
        verbose_name = _(u'техническая информация')
        verbose_name_plural = _(u'техническая информация')


class ProductTechInfoOtherAttrs(models.Model):
    """прочая техническая информация для продуктов
    """
    product = models.ForeignKey(Product, verbose_name=_(u"товар"), related_name="techinfo")
    name = models.ForeignKey(TechAttrs, verbose_name=_(u"Характеристика"), related_name="ProductTechInfo")
    value_full = models.TextField(_(u'Элемент'), blank=True)
    value = models.TextField(_(u'Описание'), blank=True)
    position = models.SmallIntegerField(_(u'позиция'), default=999)

    def __unicode__(self):
        return  u"%s" % (self.name)

    class Meta:
        ordering = ('position',)
        verbose_name = _(u'Другой параметр')
        verbose_name_plural = _(u'Другие параметры')