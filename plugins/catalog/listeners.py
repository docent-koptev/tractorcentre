# django imports
from plugins.catalog.models import *

def checked_chilren_elements(sender, instance, created, **kwargs):
	childrens = instance.get_all_children()
	for child in childrens:
		if instance.hide_price:
			child.hide_price = True
			child.save()

