# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.admin.filterspecs import ChoicesFilterSpec, BooleanFieldFilterSpec
from django.utils.encoding import smart_unicode
from django.utils.translation import gettext_lazy as _
from ecko.catalog.models import *


class PropertyOptionFilterSpec(ChoicesFilterSpec):
    def __init__(self, request, **kwargs):
        self.lookup_kwarg = 'property_values__value'
        self.lookup_val = request.GET.get(self.lookup_kwarg, None)
        self.lookup_choices = PropertyOption.objects.filter(property__pk=kwargs.get('property_id'))

    def choices(self, cl):
        yield { 'selected': self.lookup_val is None,
                'query_string': cl.get_query_string({}, [self.lookup_kwarg,]),
                'display': u'Все'}
        for option in self.lookup_choices:
            yield  {'selected': smart_unicode(option.pk) == self.lookup_val,
                    'query_string': cl.get_query_string({self.lookup_kwarg: option.pk}),
                    'display': option.name }

class SeasonFilterSpec(PropertyOptionFilterSpec):
    def __init__(self, request):
        prop = Property.objects.get(name='year')
        super(SeasonFilterSpec, self).__init__(request, property_id = prop.pk)

    def title(self):
        return u'Сезону'


class VisibleTypeFilterSpec(PropertyOptionFilterSpec):
    def __init__(self, request):
        prop = Property.objects.get(name='visible_type')
        super(VisibleTypeFilterSpec, self).__init__(request, property_id = prop.pk)
#        self.lookup_kwarg = 'type1c__visible_type__pk'
#        self.lookup_val = request.GET.get(self.lookup_kwarg, None)

    def title(self):
        return u'Отображаемому Типу'


class HaveCategoriesFilterSpec(BooleanFieldFilterSpec):
    def __init__(self, request, **kwargs):
        self.lookup_kwarg = '%s__isnull' % 'categories'
        self.lookup_kwarg2 = '%s__isnull' % 'categories'
        self.lookup_val = request.GET.get(self.lookup_kwarg, None)
        self.lookup_val2 = request.GET.get(self.lookup_kwarg2, None)

    def title(self):
        return u'без категорий'

    def choices(self, cl):
        for k, v in ((_('All'), None), (_('Yes'), True), (_('No'), False)):
            yield {'selected': self.lookup_val == v and not self.lookup_val2,
                   'query_string': cl.get_query_string(
                           {self.lookup_kwarg: v},
                       [self.lookup_kwarg2]),
                   'display': k}





