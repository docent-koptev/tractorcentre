# -*- coding: utf-8 -*-
# django imports
from django.contrib import admin
from django.contrib.auth.decorators import user_passes_test
from django import forms, template
from django.conf.urls import *
from django.contrib.contenttypes.models import ContentType
from django.forms.widgets import Textarea
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse, resolve
from django.conf import settings

from django.db.models import Count
from django.template import RequestContext
from django.shortcuts import render_to_response, render
from django.core.exceptions import ObjectDoesNotExist

# lfs imports
#from markitup.widgets import AdminMarkItUpWidget
from annoying.functions import get_object_or_None
from plugins.core.signals import product_changed
from plugins.core.widgets.tree import MpttTreeWidget
from plugins.catalog.models import *
from plugins.catalog.forms import SaleForm, ProductVariantForm, VariantsFormSet, VariantsFormSetExtra0, SelectPropertyGroupForm
from plugins.catalog.settings import PROPERTY_SELECT_FIELD, PROPERTY_TEXT_FIELD, PROPERTY_NUMBER_FIELD, PROPERTY_VALUE_TYPE_FILTER, VARIANT, PRODUCT_WITH_VARIANTS
from mptt.forms import TreeNodeMultipleChoiceField
from sorl.thumbnail.admin import AdminImageMixin
from helpers.filesandimages.admin import AttachedImageInline
from django.utils.functional import curry
from django.contrib.admin import SimpleListFilter
from docent.seo import SEOMetadata
from rollyourown.seo.admin import get_inline
from django.forms.models import BaseModelFormSet
from ckeditor.widgets import CKEditorWidget

from tinymce.widgets import TinyMCE

from feincms.admin import tree_editor
is_admin = lambda u: u.is_superuser or u.is_staff

class ProductForm(forms.ModelForm):
    short_description = forms.CharField(widget=CKEditorWidget(),label=u'Короткое описание', required=False)
    description = forms.CharField(widget=CKEditorWidget(),label=u'Полное описание', required=False)
    categories = TreeNodeMultipleChoiceField(queryset=Category.objects.all(), required=False,widget=MpttTreeWidget)

    class Meta:
        model = Product
        fields = "__all__"

class CategoryForm(forms.ModelForm):
    description = forms.CharField(required=False,widget=TinyMCE(attrs={'cols': 80, 'rows': 30}))

    class Meta:
        model = Category
        fields = "__all__"

class CategoryAdmin(tree_editor.TreeEditor):
    form = CategoryForm
    list_display = ('name', 'display','template','hide_price','display_on_top','display_on_left','uid','puid','this_is_text_cat','top_position','prioritet','css',)
    list_editable = ('top_position','template','display','hide_price','display_on_top','display_on_left','this_is_text_cat','css',)
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ('name', 'slug','description',)
    raw_id_fields = ('parent', 'products',)
    filter_horizontal = ('partners',)
    #readonly_fields = ('puid','uid','css',)
    list_per_page = 600
    fieldsets = (
        ('Основное',{
            'fields': ('name','slug','description', 'parent','photo','photo_title','photo_alt',)
        }),

        ('Надстройки',{
            'fields': ('display','display_on_top','display_on_left','this_is_text_cat','service' ,'hide_price','top_position','template','is_share',)
        }),

        ('Для разработчика',{
            'fields': ('css','uid','puid','prioritet')
        }),
        ('Партнеры',{
            'fields': ('partners','products',)
        }),
        ('SEO',{
            'fields': ('seo_title','seo_description',)
        }),
    )
    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "property_options":
            kwargs["queryset"] = PropertyOption.objects.order_by('property')
        return super(CategoryAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)





''' product technical information '''

class TechInfoForm(forms.ModelForm):
    class Meta:
        model = ProductTechInfo
        fields = ("name", "value",  "position", )
        widgets = {
            'value': Textarea(attrs={'rows': 2}),
        }

class ProductTechInfoInline(admin.TabularInline):
    model = ProductTechInfo
    extra = 1
    form = TechInfoForm


class TechOtherInfoForm(forms.ModelForm):
    class Meta:
        model = ProductTechInfoOtherAttrs
        fields = ("name", "value",  "position", )
        widgets = {
            'value': Textarea(attrs={'rows': 2}),
        }

class ProductTechOtherInfoInline(admin.TabularInline):
    model = ProductTechInfoOtherAttrs
    extra = 1
    form = TechOtherInfoForm


''' end  product technical information '''


# Product actions -------------------



def bind_property_options_to_product(modeladmin, request, queryset):
    request.session['back_to_list_url'] = request.get_full_path()
    properties = Property.objects.all()
    return render_to_response("admin/catalog/product/bind_property_options_to_product.html", locals(), context_instance=template.RequestContext(request))
bind_property_options_to_product.short_description = u"Свойства — назначить свойства товарам"

# /// Product actions -------------------
class CategoryListFilter(SimpleListFilter):
    template = 'admin/catalog/product/filter.html'
    title = u'Категории товаров'
 
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'category'
 
    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        list_tuple = []
        for category in Category.objects.all():
            #print category
            list_tuple.append((category.id, category.name))
        return list_tuple
 
    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or 'other')
        # to decide how to filter the queryset.
        if self.value():
            return queryset.filter(category__id=self.value())
        else:
            return queryset

class ProductAdmin(admin.ModelAdmin):
    form = ProductForm
    inlines = [ProductTechInfoInline, ProductTechOtherInfoInline, AttachedImageInline,]
    prepopulated_fields = {"slug": ("name", )}
    search_fields = ('id','name', 'slug', 'sku', 'id', 'uid')
    list_filter = ('active',)
    list_display = ( 'name','slug','get_image_tag', 'active', 'price','stock_amount','sku',)
    list_display_links = ('name',)
    filter_horizontal = ('related_products',)
    readonly_fields = ('active','uid')
    raw_id_fields = ('parent',)
    list_filter = ('active', 
                   'sub_type',)
    list_editable = ('slug',)
    save_on_top = True

    fieldsets = (
        ('Основная информация о продукте',{
            'fields': ('name', 'slug', 'sku', 'uid',  ('active'),'parent',
                    )
        }),
        ('Цена и остаток на складе', {
            'fields': ( 'price','stock_amount',),
        }),

        ('Описание продукта', {
            'fields': ('description','unit','pdf',),
        }),
        ('Связи', {
            'fields': ('categories', ),
        }),
        
        ('SEO',{
            'fields': ('seo_title','seo_description',)
        }),

    )




class ProductAccessoriesAdmin(admin.ModelAdmin):
    """
    """


class StaticBlockAdmin(admin.ModelAdmin):
    """"""
#    formfield_overrides = {models.TextField: {'widget': AdminMarkItUpWidget}}



class PropertyInline(admin.TabularInline):
    model = Property.groups.through

class PropertyGroupAdmin(admin.ModelAdmin):
    fields = ('name','category')
    inlines = [PropertyInline,]
    save_on_top = True


class PropertyOptionInline(admin.TabularInline):
    model = PropertyOption
    fields = ('name','short','position',)

class PropertyAdmin(admin.ModelAdmin):
    list_display = ['title', 'name', 'position', 'display_on_product', 'filterable']
    list_editable = ['position',]
    inlines = [PropertyOptionInline,]
    fieldsets = (
        (None, {
            'fields': (('title', 'name', ), 'position', ('display_on_product', 'filterable'),)
        }),
    )
    prepopulated_fields = {"name": ("title", )}
    save_on_top = True



class PropertyOptionAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'property')
    list_filter = ('property',)
    search_fields = ('name', 'property__name')
    ordering = ('property',)
    fields = ('property', 'name', 'position', 'description', 'date')
    inlines = [AttachedImageInline,]
    save_on_top = True

    def get_object(self, request, object_id):
        queryset = self.queryset(request)
        self.obj = queryset.get(pk=object_id)
        return super(PropertyOptionAdmin, self).get_object(request, object_id)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "childs" and hasattr(self, 'obj'):
            kwargs["queryset"] = PropertyOption.objects.exclude(pk=self.obj.pk).order_by('property')
        return super(PropertyOptionAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)



class ProductPropertyValueAdmin(admin.ModelAdmin):
    raw_id_fields = ('product',)



admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductAccessories, ProductAccessoriesAdmin)
admin.site.register(StaticBlock, StaticBlockAdmin)
admin.site.register(PropertyGroup, PropertyGroupAdmin)
admin.site.register(PropertyOption, PropertyOptionAdmin)
admin.site.register(ProductPropertyValue, ProductPropertyValueAdmin)
admin.site.register(FilterStep)
admin.site.register(Property, PropertyAdmin)
admin.site.register(Country)
admin.site.register(ProductAttribute)
admin.site.register(TechAttrs)
admin.site.register(ProductTechInfo)
admin.site.register(ProductTechInfoOtherAttrs)