# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0008_auto_20150304_1657'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='color',
        ),
        migrations.RemoveField(
            model_name='product',
            name='country',
        ),
        migrations.RemoveField(
            model_name='product',
            name='photo_list',
        ),
        migrations.RemoveField(
            model_name='product',
            name='place',
        ),
        migrations.RemoveField(
            model_name='product',
            name='size',
        ),
    ]
