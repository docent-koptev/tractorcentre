# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0026_category_photo_alt'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='template',
            field=models.PositiveSmallIntegerField(blank=True, max_length=400, null=True, verbose_name='\u0428\u0430\u0431\u043b\u043e\u043d', choices=[(1, 'base'), (2, 'te')]),
            preserve_default=True,
        ),
    ]
