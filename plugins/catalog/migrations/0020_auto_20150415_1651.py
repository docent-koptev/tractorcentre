# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0019_auto_20150415_1645'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='seo_description',
            field=models.CharField(max_length=200, null=True, verbose_name='Description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='seo_title',
            field=models.CharField(max_length=200, null=True, verbose_name='seo title', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='seo_description',
            field=models.CharField(max_length=200, null=True, verbose_name='Description', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='seo_title',
            field=models.CharField(max_length=200, null=True, verbose_name='seo title', blank=True),
            preserve_default=True,
        ),
    ]
