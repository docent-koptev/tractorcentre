# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0021_category_priority'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='priority',
            field=models.CharField(max_length=200, null=True, verbose_name='\u041f\u0440\u0438\u043e\u0440\u0438\u0442\u0435\u0442', blank=True),
            preserve_default=True,
        ),
    ]
