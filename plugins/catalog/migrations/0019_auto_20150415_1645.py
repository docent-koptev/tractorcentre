# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0018_auto_20150407_1103'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='seo_description',
            field=models.CharField(max_length=200, null=True, verbose_name='SEO seo_description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='category',
            name='seo_title',
            field=models.CharField(max_length=200, null=True, verbose_name='SEO title', blank=True),
            preserve_default=True,
        ),
    ]
