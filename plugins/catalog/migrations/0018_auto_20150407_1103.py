# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0017_category_partners'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='partners',
            field=models.ManyToManyField(to='manufacturer.Partners', null=True, verbose_name='\u041f\u0430\u0440\u0442\u043d\u0435\u0440\u044b', blank=True),
            preserve_default=True,
        ),
    ]
