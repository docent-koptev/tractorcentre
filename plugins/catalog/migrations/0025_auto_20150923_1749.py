# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0024_auto_20150525_1619'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='photo_title',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0422\u0430\u0439\u0442\u043b \u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0438', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='product',
            name='is_hit',
            field=models.BooleanField(default=False, verbose_name='\u0410\u043a\u0446\u0438\u044f'),
            preserve_default=True,
        ),
    ]
