# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0005_auto_20150304_1637'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='css',
            field=models.CharField(help_text='\u0421\u043b\u0443\u0436\u0435\u0431\u043d\u0430\u044f \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f', max_length=200, null=True, verbose_name='css \u043a\u043b\u0430\u0441\u0441', blank=True),
            preserve_default=True,
        ),
    ]
