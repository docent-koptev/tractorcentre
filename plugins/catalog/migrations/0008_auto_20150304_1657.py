# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0007_auto_20150304_1642'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='display_on_left',
            field=models.BooleanField(default=False, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0441\u043b\u0435\u0432\u0430 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='category',
            name='display_on_top',
            field=models.BooleanField(default=False, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u0448\u0430\u043f\u043a\u0435'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='css',
            field=models.CharField(help_text='\u0421\u043b\u0443\u0436\u0435\u0431\u043d\u0430\u044f \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f', max_length=200, null=True, verbose_name='CSS \u043a\u043b\u0430\u0441\u0441', blank=True),
            preserve_default=True,
        ),
    ]
