# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0010_auto_20150305_1151'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductTechInfoOtherAttrs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value_full', models.TextField(verbose_name='\u042d\u043b\u0435\u043c\u0435\u043d\u0442', blank=True)),
                ('value', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True)),
                ('position', models.SmallIntegerField(default=999, verbose_name='\u043f\u043e\u0437\u0438\u0446\u0438\u044f')),
                ('name', models.ForeignKey(related_name='ProductTechInfo', verbose_name='\u0425\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a\u0430', to='catalog.TechAttrs')),
                ('product', models.ForeignKey(related_name='techinfo', verbose_name='\u0442\u043e\u0432\u0430\u0440', to='catalog.Product')),
            ],
            options={
                'ordering': ('position',),
                'verbose_name': '\u0414\u0440\u0443\u0433\u043e\u0439 \u043f\u0430\u0440\u0430\u043c\u0435\u0442\u0440',
                'verbose_name_plural': '\u0414\u0440\u0443\u0433\u0438\u0435 \u043f\u0430\u0440\u0430\u043c\u0435\u0442\u0440\u044b',
            },
            bases=(models.Model,),
        ),
    ]
