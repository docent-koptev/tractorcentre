# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0012_auto_20150305_1248'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='this_is_text_cat',
            field=models.BooleanField(default=False, verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f \u0442\u0435\u043a\u0441\u0442\u043e\u0432\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='product',
            name='parent',
            field=models.ForeignKey(related_name='variants', blank=True, to='catalog.Product', help_text='\u0412\u044b\u0431\u0440\u0430\u0442\u044c, \u0435\u0441\u043b\u0438 \u0442\u0438\u043f - \u0442\u043e\u0432\u0430\u0440 \u0441 \u0432\u0430\u0440\u0438\u0430\u043d\u0442\u0430\u043c\u0438', null=True, verbose_name='\u0420\u043e\u0434\u0438\u0442\u0435\u043b\u044c'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='product',
            name='sub_type',
            field=models.CharField(default=b'0', help_text='\u0414\u043b\u044f \u0440\u0430\u0437\u0433\u0440\u0430\u043d\u0438\u0447\u0435\u043d\u0438\u044f \u0438 \u0443\u0434\u043e\u0431\u0441\u0442\u0432\u0430', max_length=10, verbose_name='Subtype', choices=[(b'0', 'Standard'), (b'1', 'Product with variants'), (b'2', 'Variant')]),
            preserve_default=True,
        ),
    ]
