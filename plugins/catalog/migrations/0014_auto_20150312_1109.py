# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0013_auto_20150306_1334'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='product',
            options={'ordering': ('name',), 'verbose_name': '\u0422\u043e\u0432\u0430\u0440 \u0442\u0440\u0430\u043a\u0442\u043e\u0440\u043e\u0446\u0435\u043d\u0442\u0440\u0430', 'verbose_name_plural': '\u0422\u043e\u0432\u0430\u0440\u044b \u0442\u0440\u0430\u043a\u0442\u043e\u0440\u043e\u0446\u0435\u043d\u0442\u0440\u0430'},
        ),
        migrations.AddField(
            model_name='category',
            name='hide_price',
            field=models.BooleanField(default=False, help_text='\u0421\u043a\u0440\u044b\u0432\u0430\u0442\u044c \u0446\u0435\u043d\u0443 \u0434\u043b\u044f \u0442\u043e\u0432\u0430\u0440\u043e\u0432 \u0434\u0430\u043d\u043d\u043e\u0439 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438', verbose_name='\u0421\u043a\u0440\u044b\u0442\u044c \u0446\u0435\u043d\u0443'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='display',
            field=models.BooleanField(default=True, help_text='\u0423\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u0435 \u0430\u043a\u0442\u0438\u0432\u043d\u043e\u0441\u0442\u044c\u044e \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438', verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='display_on_left',
            field=models.BooleanField(default=False, help_text='\u041b\u0435\u0432\u043e\u0435 \u043c\u0435\u043d\u044e.\u041f\u043e\u0434\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438', verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0441\u043b\u0435\u0432\u0430 \u043d\u0430 \u0433\u043b\u0430\u0432\u043d\u043e\u0439'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='display_on_top',
            field=models.BooleanField(default=False, help_text='\u0412\u0435\u0440\u0445\u043d\u0435\u0435 \u043c\u0435\u043d\u044e \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0439', verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0432 \u0448\u0430\u043f\u043a\u0435'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='this_is_text_cat',
            field=models.BooleanField(default=False, help_text='\u0415\u0441\u043b\u0438 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f \u0432\u0435\u0434\u0435\u0442 \u043d\u0435 \u043d\u0430 \u043a\u0430\u0442\u0430\u043b\u043e\u0433,\u0430 \u0432 \u0442\u0435\u043a\u0441\u0442\u043e\u0432\u0443\u044e \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443', verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f \u0442\u0435\u043a\u0441\u0442\u043e\u0432\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b'),
            preserve_default=True,
        ),
    ]
