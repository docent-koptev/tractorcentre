# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manufacturer', '__first__'),
        ('catalog', '0016_auto_20150319_1855'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='partners',
            field=models.ManyToManyField(to='manufacturer.Manufacturer', null=True, verbose_name='\u041f\u0430\u0440\u0442\u043d\u0435\u0440\u044b', blank=True),
            preserve_default=True,
        ),
    ]
