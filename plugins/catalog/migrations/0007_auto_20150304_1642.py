# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0006_category_css'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='photo',
            field=models.ImageField(help_text='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438', upload_to=b'categories', null=True, verbose_name='\u041a\u0430\u0440\u0442\u0438\u043d\u043a\u0430', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='puid',
            field=models.CharField(help_text='\u0421\u043b\u0443\u0436\u0435\u0431\u043d\u0430\u044f \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f', max_length=50, null=True, verbose_name='ID \u0440\u043e\u0434\u0438\u0442\u0435\u043b\u044f', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='uid',
            field=models.CharField(help_text='\u0421\u043b\u0443\u0436\u0435\u0431\u043d\u0430\u044f \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f', max_length=50, null=True, verbose_name='ID \u0433\u0440\u0443\u043f\u043f\u044b', blank=True),
            preserve_default=True,
        ),
    ]
