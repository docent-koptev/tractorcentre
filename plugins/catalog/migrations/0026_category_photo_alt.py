# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0025_auto_20150923_1749'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='photo_alt',
            field=models.CharField(max_length=200, null=True, verbose_name='alt \u043a\u0430\u0440\u0442\u0438\u043d\u043a\u0438', blank=True),
            preserve_default=True,
        ),
    ]
