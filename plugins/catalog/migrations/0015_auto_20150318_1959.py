# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0002_auto_20150318_1745'),
        ('catalog', '0014_auto_20150312_1109'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='service',
            field=models.ForeignKey(blank=True, to='service.Service', help_text='\u0412\u044b\u0431\u0438\u0440\u0430\u0435\u043c,\u0432 \u0441\u043b\u0443\u0447\u0430\u0435 \u0435\u0441\u043b\u0438 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f \u0441\u0441\u044b\u043b\u0430\u0435\u0442\u0441\u044f \u043d\u0430 \u0441\u0435\u0440\u0432\u0438\u0441', null=True, verbose_name='\u0421\u0435\u0440\u0432\u0438\u0441'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='this_is_text_cat',
            field=models.BooleanField(default=False, help_text='\u0415\u0441\u043b\u0438 \u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f \u0432\u0435\u0434\u0435\u0442 \u043d\u0435 \u043d\u0430 \u043a\u0430\u0442\u0430\u043b\u043e\u0433,\u0430 \u0432 \u0442\u0435\u043a\u0441\u0442\u043e\u0432\u0443\u044e \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443', verbose_name='\u041a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u044f \u0441\u0435\u0440\u0432\u0438\u0441\u0430'),
            preserve_default=True,
        ),
    ]
