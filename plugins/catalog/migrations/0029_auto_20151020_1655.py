# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0028_auto_20151020_1648'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='is_share',
            field=models.BooleanField(default=False, verbose_name='\u041d\u0430 \u0432\u0441\u0435\u0445 \u043c\u043e\u0438\u0445 \u0434\u0435\u0442\u0435\u0439'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='template',
            field=models.PositiveSmallIntegerField(default=1, max_length=400, verbose_name='\u0428\u0430\u0431\u043b\u043e\u043d', choices=[(1, 'Base'), (2, 'Extended (Denis Veselov pres.)'), (3, 'Old Version')]),
            preserve_default=True,
        ),
    ]
