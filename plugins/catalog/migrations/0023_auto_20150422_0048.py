# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0022_auto_20150421_1751'),
    ]

    operations = [
        migrations.RenameField(
            model_name='category',
            old_name='priority',
            new_name='prioritet',
        ),
    ]
