# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0027_auto_20151020_1544'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='template',
            field=models.PositiveSmallIntegerField(default=1, max_length=400, verbose_name='\u0428\u0430\u0431\u043b\u043e\u043d', choices=[(1, 'Base'), (2, 'Extended (Denis Veselov pres.)')]),
            preserve_default=True,
        ),
    ]
