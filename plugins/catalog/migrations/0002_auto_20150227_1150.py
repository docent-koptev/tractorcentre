# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='category_cols',
        ),
        migrations.RemoveField(
            model_name='category',
            name='product_cols',
        ),
        migrations.RemoveField(
            model_name='category',
            name='product_rows',
        ),
        migrations.AlterField(
            model_name='category',
            name='puid',
            field=models.CharField(help_text='\u0421\u043b\u0443\u0436\u0435\u0431\u043d\u0430\u044f \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f', max_length=50, null=True, verbose_name='\u0418\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u0440\u043e\u0434\u0438\u0442\u0435\u043b\u044f \u0433\u0440\u0443\u043f\u043f\u044b', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='uid',
            field=models.CharField(help_text='\u0421\u043b\u0443\u0436\u0435\u0431\u043d\u0430\u044f \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f', max_length=50, null=True, verbose_name='\u0418\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u0433\u0440\u0443\u043f\u043f\u044b', blank=True),
            preserve_default=True,
        ),
    ]
