from django import template
from django.contrib.auth.decorators import user_passes_test
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, render
from plugins.catalog.forms import SaleForm, SelectPropertyGroupForm
from plugins.catalog.models import Product

is_admin = lambda u: u.is_superuser

@user_passes_test(is_admin)
def create_sale_view(request):
    if request.method == 'POST':
        products_ids = request.POST.getlist('products')
        products = Product.objects.filter(pk__in = products_ids)
        form = SaleForm(request.POST)
        if form.is_valid():
            sale = form.cleaned_data['sale']
            for product in products:
                product.for_sale = True
                product.for_sale_price = round(product.price - (product.price/100)*sale, 2)
                product.save()
        else:
            return render_to_response("admin/catalog/product/create_sale.html",
                                      { 'form': form, 'queryset': products },
                                      context_instance=template.RequestContext(request))

    back_url = request.session.get('back_to_list_url') or reverse('admin:catalog_product_changelist')
    return HttpResponseRedirect(redirect_to=back_url)

@user_passes_test(is_admin)
def remove_sale_view(request):
    if request.method == 'POST':
        products_ids = request.POST.getlist('products')
        products = Product.objects.filter(pk__in = products_ids)
#        form = SaleForm(request.POST)
#        if form.is_valid():
#            sale = form.cleaned_data['sale']
        for product in products:
            product.for_sale = False
            product.for_sale_price = 0
            product.save()
#        else:
#            return render_to_response("admin/catalog/product/remove_sale.html",
#                                      { 'queryset': products },
#                                      context_instance=template.RequestContext(request))

    back_url = request.session.get('back_to_list_url') or reverse('admin:catalog_product_changelist')
    return HttpResponseRedirect(redirect_to=back_url)

@user_passes_test(is_admin)
def set_proprty_group_view(request):
    if request.method == "POST":
        products_ids = request.POST.getlist('products')
        products = Product.objects.filter(pk__in = products_ids)

        form = SelectPropertyGroupForm(request.POST)
        if form.is_valid():
            group = form.cleaned_data['property_group']
            for product in products:
                product.property_groups.clear()
                product.property_groups.add(group)
        else:
            return render(request, 'admin/catalog/product/set_proprty_group.html', {
                'form': form, 'queryset': products
            })
    back_url = request.session.get('back_to_list_url') or reverse('admin:catalog_product_changelist')
    return HttpResponseRedirect(redirect_to=back_url)


