# -*- coding: utf-8 -*-
from django import forms
from plugins.catalog.models import Product, PropertyGroup

class SaleForm(forms.Form):
    sale = forms.FloatField(label=u'Скидка %')

class SelectPropertyGroupForm(forms.Form):
    property_group = forms.ModelChoiceField(queryset=PropertyGroup.objects.all())

FIELDS = ( "variant_position", )

class ProductVariantForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = FIELDS

VariantsFormSetExtra0 = forms.models.modelformset_factory(Product, fields=FIELDS, extra=0)
VariantsFormSet = forms.models.modelformset_factory(Product, fields=FIELDS)

class RequestPriceForm(forms.Form):
	name = forms.CharField(label=u"Имя", required=False, max_length=50,widget=forms.TextInput(attrs={'class':'form-control',}))
	phone= forms.CharField(label=u"Телефон", max_length=50,widget=forms.TextInput(attrs={'class':'form-control phone','type':'tel',}))
	email = forms.EmailField(label=u'Email', required=False,widget=forms.TextInput(attrs={'class':'form-control',}))