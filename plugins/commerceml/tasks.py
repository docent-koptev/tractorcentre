# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
import os

from django.conf import settings
from django.core.management import call_command
from django.http import HttpRequest
from django.utils import translation

from plugins.commerceml.utils import XMLParser
from celery.task import PeriodicTask, Task
from celery.task.schedules import crontab

from celery.task import periodic_task
from datetime import timedelta
from celery import task

@task(name='commerceml.xmlparser')
def parser():
	parser = XMLParser()