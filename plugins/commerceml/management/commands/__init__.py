# -*- coding: utf-8 -*-
from plugins.mail.utils import send_mail
import sys, traceback

def except_handler(func):
    def wrapper(self, *args, **kwargs):
        try:
            self.stdout.write('\n\r'.join(func(self, *args, **kwargs)))
        except Exception as e:
            self.stderr.write(e.message)
            send_mail(u'Ошибка при синхронизации с моим складом', to_admin=True, template='syncerror.html', context={
                'traceback': traceback.format_exc()
            })
    return wrapper