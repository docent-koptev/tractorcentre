# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from plugins.commerceml.management.commands import except_handler


class Command(BaseCommand):
    args = ''
    help = u'Получить XML без картинок и описания'

    @except_handler
    def handle(self, *args, **options):
        from plugins.commerceml.utils import GetFileFromFtp
        return GetFileFromFtp().get_small_file()