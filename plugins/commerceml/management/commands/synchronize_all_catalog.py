# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from plugins.commerceml.management.commands import except_handler


class Command(BaseCommand):
    args = ''
    help = u'Синхронизация товаров'

    def handle(self, *args, **options):
        from plugins.commerceml.utils import XMLParser
        return XMLParser()