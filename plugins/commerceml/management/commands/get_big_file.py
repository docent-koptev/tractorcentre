# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from plugins.commerceml.management.commands import except_handler


class Command(BaseCommand):
    args = ''
    help = u'Получить XML с картинками и описанием'

    @except_handler
    def handle(self, *args, **options):
        from plugins.commerceml.utils import GetFileFromFtp
        return GetFileFromFtp().get_big_file()