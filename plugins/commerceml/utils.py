# -*- coding: utf-8 -*-
import os
import sys
import base64 

from django.contrib.contenttypes.models import ContentType
import xml.etree.ElementTree as ET
from ftplib import FTP

from plugins.catalog.models import *
from annoying.functions import get_object_or_None
from helpers.filesandimages.models import AttachedImage

from filer.models.filemodels import File
from django.conf import settings

import unidecode
from django.template.defaultfilters import slugify

from django.db import connection, transaction



class GetFileFromFtp(object):
	def __init__(self):
		IP = '79.175.42.135'
		PORT = '21'
		self.ftp = FTP()
		self.ftp.connect(IP,PORT)
		self.ftp.login('7656feed','7656feed')
		self.ftp.cwd('/7656feed')
		self.files = self.ftp.nlst()

	#get file from tractorocentr server
	def getbinary(self, ftp, filename, outfile=None):
		# fetch a binary file
		if outfile is None:
			outfile = settings.MEDIA_ROOT + "/1c/"
		f = open(outfile + filename,"wr")
		ftp.retrbinary("RETR " + filename, f.write)
		f.close()
		return f


	#get small file,only text without photo and description
	def get_small_file(self):
		small_file = self.files[0]
		for file in self.files:
			if self.ftp.size(file) < self.ftp.size(small_file):
				small_file = file
		fetch_file = self.getbinary(self.ftp,small_file)

	#get big file: photo with description
	def get_big_file(self):
		big_file = self.files[0]
		for file in self.files:
			if self.ftp.size(file) > self.ftp.size(big_file):
				big_file = file
		fetch_file = self.getbinary(self.ftp,big_file)

	def test(self):
		outFile = None
		if len(os.listdir(settings.MEDIA_ROOT + "/1c/")) >= 2:
			big_file = os.listdir(settings.MEDIA_ROOT + "/1c/")[0]
			for file in os.listdir(settings.MEDIA_ROOT + "/1c/"):
				if os.stat(settings.MEDIA_ROOT + "/1c/" + file).st_size > os.stat(settings.MEDIA_ROOT + "/1c/" + big_file).st_size:
					big_file = os.stat(settings.MEDIA_ROOT + "/1c/" + file)
			outFile = big_file
		else:
			outFile = os.listdir(settings.MEDIA_ROOT + "/1c/")[0]
		print outFile




class XMLParser(object):
	def __init__(self):
		outFile = None
		dir_of_1c = os.listdir(settings.MEDIA_ROOT + "/1c/")
		remove = False
		if len(dir_of_1c) > 0:
			if len(dir_of_1c) >= 2:
				big_file = dir_of_1c[0]
				for file in dir_of_1c:
					if os.stat(settings.MEDIA_ROOT + "/1c/" + file).st_size > os.stat(settings.MEDIA_ROOT + "/1c/" + big_file).st_size:
						big_file = file
				outFile = big_file
			else:
				outFile = dir_of_1c[0]

			FILE = settings.MEDIA_ROOT + "/1c/" + str(outFile)
			if FILE:
				tree = ET.parse(FILE)
				xml_catalog_tmp = tree.getroot()
				xml_catalog = self.remove_namespace(xml_catalog_tmp,'http://www.tc.org')
				xml_catalog_categories = xml_catalog.find(u'Группы')
				xml_catalog_products = xml_catalog.find(u'Остатки')
				if xml_catalog_categories and xml_catalog_products:
					self.import_categories(xml_catalog_categories)
					self.import_products(xml_catalog_products)
					self.swith_off_cat_no_product()
					remove = True

			if remove:
				if os.path.isfile(FILE):
					os.remove(FILE)



	def remove_namespace(self,doc, namespace):
		ns = u'{%s}' % namespace
		nsl = len(ns)
		for elem in doc.getiterator():
			if elem.tag.startswith(ns):
				elem.tag = elem.tag[nsl:]
		return doc
	

	def reset_for_cat(self):
		cursor = connection.cursor()
		cursor.execute("SELECT setval('catalog_category_id_seq', (SELECT MAX(id) FROM catalog_category)+1)")
		return True

	def reset_for_product(self):
		cursor = connection.cursor()
		cursor.execute("SELECT setval('catalog_product_id_seq', (SELECT MAX(id) FROM catalog_pruduct)+1)")
		return True

	def assign_categories(self,element):
		""" assign root category for it's childrens """
		find_cat = None
		if element.puid is not None and element.puid != '':
			try:
				find_cat = get_object_or_None(Category, uid=element.puid)
			except Exception as e:
				print 'error on_ ' + element.puid
		if find_cat is not None:
			element.parent = find_cat
			element.save()

	def import_categories(self,xml_catalog, parent=0,all=True):
		if all:
			cats = Category.objects.all().exclude(display_on_top=True)
			for cat in cats:
				if not cat.this_is_text_cat and cat.pk != 1343:
					cat.display = False
					cat.save()
				print "display false for cat-" + cat.name

		created_cats = 0
		updated_cats = 0

		cat_name = None
		cat_parent_code = None
		cat_code = None

		#cat_priority = 999
		for element in xml_catalog:
			cat_priority = 1000
			exist_category = None
			if element.find(u'Наименование') is not None:
				cat_name = element.find(u'Наименование').text.replace("_","").replace("__","").strip('_\t\n\r')
			
			if element.find(u'КодГруппы') is not None:
				cat_parent_code = element.find(u'КодГруппы').text.strip(' \t\n\r')

			if  element.find(u'КодЭлемента') is not None:
				cat_code = element.find(u'КодЭлемента').text.strip(' \t\n\r')

			if element.find(u'Приоритет') is not None:
				try:
					cat_priority = element.find(u'Приоритет').text.strip(' \t\n\r')
				except Exception as e:
					cat_priority = 1000

			if cat_priority == '0':
				cat_priority = 1000
			if cat_code is not None:
				exist_category = get_object_or_None(Category, uid=cat_code)
				if exist_category:
					exist_category.prioritet = cat_priority
					exist_category.name = cat_name
					if exist_category.display_on_top:
						exist_category.name = cat_name.lower().capitalize()
					exist_category.puid = cat_parent_code
					exist_category.display = True
					exist_category.save()
					print "updated cat-" + exist_category.name
					updated_cats = updated_cats + 1
				else:
					tmp_slug = slugify(unidecode.unidecode(cat_name))
					self.reset_for_cat()
					cat = Category.objects.create(name=cat_name,slug=tmp_slug, display=True, uid=cat_code,puid=cat_parent_code or None,prioritet=int(cat_priority))
					self.reset_for_cat()
					created_cats = created_cats + 1
					print "created cat-" + cat.name
				print cat_priority
		print "-----------------------------"
		print "In this seasson created categories: %s" % created_cats
		print "and updated categories: %s" % updated_cats

		for created_cat in Category.objects.all():
			if created_cat.puid is not None:
				self.assign_categories(created_cat)
		Category.objects.rebuild()

	def assign_categories_to_product(self,product,category):
		'''
			function get all ancestors and 
			assign it to current product
		'''
		if category is not None:
			cat_list = category.get_ancestors(include_self=True)
			for cat in cat_list:
				if cat is not None and product is not None:
					if cat not in product.categories.all():
						product.categories.add(cat)
						product.save()

	def create_or_update_image(self,resource,product,delete=False):
		contentt = ContentType.objects.get(pk=15)
		if delete:
			exist_yet_or_no = list(AttachedImage.objects.filter(content_type=contentt,content_id=product.pk))
			if len(exist_yet_or_no) > 0:
				for p in exist_yet_or_no:
					p.delete()
		
		else:
			''' function  create png image from base64 source '''
			imgdata = base64.b64decode(resource)
			filename = str(product.pk) + '.png'
			abs_path = settings.MEDIA_ROOT + "/generic/images/"
			generic_path = 'generic/images/'

			abs_save_path = abs_path + filename
			generic_path_image = generic_path + filename

			with open(abs_save_path, 'wb') as f:
				f.write(imgdata)
				f.close()
			exist_yet = list(AttachedImage.objects.filter(content_type=contentt,content_id=product.pk,image=generic_path_image))
			if len(exist_yet) > 0:
				for p in exist_yet:
					p.image = generic_path_image #image with same name,but content is not same
					p.save()
			else:
				AttachedImage.objects.create(content_type=contentt,content_id=product.pk,image=generic_path_image)
			return generic_path_image
		return None

	def create_or_update_pdf(self,resource,product):
		''' function  create pdf  from base64 source '''
		pdfdata = base64.b64decode(resource)
		filename = str(product.pk) + '.pdf'
		abs_path = settings.MEDIA_ROOT + "/pdf/"
		
		abs_save_path = abs_path + filename
		with open(abs_save_path, 'wb') as f:
			f.write(pdfdata)
			f.close()
		product.pdf = 'pdf/' + filename
		product.save()

	def import_products(self,xml_catalog,parent=0,reset = True):
		created_products = 0
		updated_products = 0

		if reset:
			products = Product.objects.all()
			for product in products:
				product.active = False
				product.save()

		for element in xml_catalog:
			nomenklatura = element.find(u'Номенклатура')
			product_name = ''
			product_sku = None
			product_uid = ''
			product_group_code = None
			product_stock_amount = ''
			product_price = ''
			product_unit = ''
			product_image = None
			product_description = ''
			product_pdf = None
			attr_dict = {}

			group = None

			for el in nomenklatura:
				if el.tag == u'Наименование':
					try:
						product_name = el.text.strip(' \t\n\r')
					except Exception as e:
						pass
				if el.tag == u'Артикул':
					try:
						product_sku = el.text or ''
					except Exception as e:
						pass
				if el.tag == u'КодЭлемента':
					try:
						product_uid = el.text.strip(' \t\n\r')
					except Exception as e:
						pass

				if el.tag == u'КодГруппы':
					try:
						product_group_code = el.text.strip(' \t\n\r')
					except Exception as e:
						pass

				if el.tag == u'Описание':
					try:
						product_description = el.text.replace("\n","<br>").replace("\t","&nbsp;").strip('_\t\n\r')
					except Exception as e:
						pass
				
				if el.tag == u'Картинка':
					try:
						product_image = el.text.strip(' \t\n\r')
					except Exception as e:
						pass
				
				if el.tag == u'ДопОписание':
					try:
						product_pdf = el.text.strip(' \t\n\r')
					except Exception as e:
						pass

				if el.tag == u'НоменклатураСвойство':
					attr_name = el.find(u"НаименованиеСвойства").text.strip(' \t\n\r')
					attr_value = el.find(u"ЗначениеСвойства").text.strip(' \t\n\r')
					attr_dict[attr_name] = attr_value
			product_stock_amount = element.find(u'Остаток').text.strip(' \t\n\r')
			product_price = element.find(u'Цена').text.strip(' \t\n\r')
			try:
				product_unit = element.find(u'ЕдиницаИзмерения').text.strip(' \t\n\r')
			except Exception as e:
				pass

			if product_group_code and product_group_code != '' and product_group_code is not None:
				try:
					group = get_object_or_None(Category,uid=product_group_code)
				except Exception:
					pass

			product = get_object_or_None(Product,uid=product_uid)
			if product is not None:
				product.name = product_name
				product.sku = product_sku
				product.stock_amount = product_stock_amount
				product.price = product_price
				product.unit = product_unit
				product.active = True
				product.description = product_description
				product.save()
				print "updated product-" + unicode(product.name)
				updated_products = updated_products + 1
			else:
				if product_name != '':
					tmp_slug = slugify(unidecode.unidecode(product_name))
					if tmp_slug is not None and tmp_slug != '':
						#self.reset_for_product()
						product = Product.objects.create(active=True,slug=tmp_slug,name=product_name, 
							uid=product_uid, sku=product_sku, price=float(product_price),
							effective_price=float(product_price), 
							stock_amount=product_stock_amount,
							description=product_description)
						print "created product-" + unicode(product.name)
						created_products = created_products + 1
			for attr_name,attr_value in attr_dict.items():
				attr,created = TechAttrs.objects.get_or_create(name=unicode(attr_name),defaults={'name': unicode(attr_name)})
				tech_attr_for_product,created = ProductTechInfo.objects.get_or_create(product=product,name=attr,defaults={'value': attr_value})
			#assign product to categories
			self.assign_categories_to_product(product,group)

			if product_image is not None:
				#assign image for product
				image = self.create_or_update_image(product_image,product)

			if product_pdf is not None:
				pdf = self.create_or_update_pdf(product_pdf,product)
		
		print "In this session created products: %s" % created_products
		print "and updated products: %s"	% updated_products


	def swith_off_cat_no_product(self):
		cats = Category.objects.all().exclude(this_is_text_cat=True)
		for cat in cats:
			if not cat.has_products():
				cat.display = False
				cat.save()

