# -*- coding: utf-8 -*-
from django.contrib.sitemaps import Sitemap
from django.db import models
from datetime import datetime
from plugins.catalog.models import Product
from plugins.customer.models import Customer

class Review(models.Model):
    product = models.ForeignKey(Product,verbose_name=u'Продукт')
    who = models.ForeignKey(Customer,verbose_name=u'От кого отзыв')
    body = models.TextField(u'Текст отзыва',)
    publish = models.BooleanField(u'опубликовать', default=False)
    posted_date = models.DateTimeField(u'Дата добавления',default=datetime.now())

    class Meta:
        verbose_name = u'Отзыв'
        verbose_name_plural = u'Отзывы'


    def __unicode__(self):
        return '%s' %unicode(self.who)

