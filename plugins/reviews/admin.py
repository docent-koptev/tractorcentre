# -*- coding: utf-8 -*-
from django.contrib import admin
from plugins.reviews.models import *

class ReviewAdmin(admin.ModelAdmin):
	list_display = ('who','body','publish','posted_date')
	readonly_fields = ('product','who',)
admin.site.register(Review, ReviewAdmin)