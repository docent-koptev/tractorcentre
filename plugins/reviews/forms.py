# -*- coding: utf-8 -*-
# django imports
from django.forms import ModelForm, Textarea,TextInput
from django.contrib.auth.models import User
from plugins.reviews.models import Review
from django.utils.translation import ugettext_lazy as _


class ReviewForm(ModelForm):
	class Meta:
		model = Review
		fields = ("body",)
		widgets = {
			'body':Textarea(attrs={'cols': 68, 'rows': 8,'placeholder':u'Ваш комментарий....',}),
		}
		