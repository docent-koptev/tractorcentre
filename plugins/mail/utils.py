# -*- coding: utf-8 -*-
# django imports
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.core.mail import EmailMultiAlternatives,EmailMessage
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _

# lfs imports
import plugins.core.utils

def send_order_sent_mail(order):
    """Sends an order has been sent mail to the shop customer
    """
    shop = plugins.core.utils.get_default_shop()

    subject = _(u"Your order has been received")
    from_email = shop.from_email
    to = [order.customer_email]
    bcc = shop.get_notification_emails()

    # text
    text = render_to_string("mail/order_sent_mail.txt", {"order" : order})
    mail = EmailMultiAlternatives(
        subject=subject, body=text, from_email=from_email, to=to, bcc=bcc)

    # html
    html = render_to_string("mail/order_sent_mail.html", {
        "order" : order
    })

    mail.attach_alternative(html, "text/html")
    mail.send()

def send_order_received_mail(order):
    """Sends an order received mail to the shop customer.

    Customer information is taken from the provided order.
    """
    shop = plugins.core.utils.get_default_shop()

    subject = u'Ваш заказ №%s принят' % order.custom_order_number()
    from_email = shop.from_email
    to = [order.email]
    bcc = shop.get_notification_emails()

    # text
    text = render_to_string("mail/order_received_mail.txt", {"order" : order})
    mail = EmailMultiAlternatives(
        subject=subject, body=text, from_email=from_email, to=to, bcc=bcc)

    # html
    html = render_to_string("mail/order_received_mail.html", {
        "order" : order
    })

    mail.attach_alternative(html, "text/html")
    mail.send()

def send_customer_added(user,password=None):
    """Sends a mail to a newly registered user.
    """
    shop = plugins.core.utils.get_default_shop()

    from_email = shop.from_email
    to = [user.email,]
    bcc = shop.get_notification_emails()

    domain = Site.objects.get_current().domain
    # text
    text = render_to_string("mail/new_user_mail.txt", {
        "user" : user, "shop" : shop, "domain": domain,"password":password })
    
    # subject    
    subject = render_to_string("mail/new_user_mail_subject.txt", {
        "user" : user, "shop" : shop,"password":password})
    
    mail = EmailMultiAlternatives(
        subject=subject, body=text, from_email=from_email, to=to, bcc=bcc)

    # html
    html = render_to_string("mail/new_user_mail.html", {
        "user" : user, "shop" : shop, "domain": domain,"password":password
    })

    mail.attach_alternative(html, "text/html")
    mail.send(fail_silently=True)

def send_review_added(review):
    """Sends a mail to shop admins that a new review has been added
    """
    shop = plugins.core.utils.get_default_shop()

    subject = _(u"New review has been added")
    from_email = shop.from_email
    to = shop.get_notification_emails()

    ctype = ContentType.objects.get_for_id(review.content_type_id)
    product = ctype.get_object_for_this_type(pk=review.content_id)

    # text
    text = render_to_string("mail/review_added_mail.txt", {
        "review" : review,
        "product" : product,
    })

    mail = EmailMultiAlternatives(
        subject=subject, body=text, from_email=from_email, to=to)

    # html
    html = render_to_string("mail/review_added_mail.html", {
        "site" : "http://%s" % Site.objects.get(id=settings.SITE_ID),
        "review" : review,
        "product" : product,
    })

    mail.attach_alternative(html, "text/html")
    mail.send(fail_silently=True)


from django.contrib.auth.models import User


class MailException(Exception):
    pass


def send_mail(subject='', to=None, to_admin=False, template='', context=None, fail_silently=False):
    if to_admin:
        to = [ email for name, email in settings.ADMINS ]

    if not to:
        raise MailException('to emails not defined')

    email = EmailMultiAlternatives(subject=subject,
                                   from_email=settings.SERVER_EMAIL, to=to)

    if not context:
        context = {}

    if not template:
        html = subject
    else:
        html = render_to_string(template, context)

    email.attach_alternative(html, 'text/html')
    email.send(fail_silently=fail_silently)

def send_mail_to_order_call(phone,name=None):
    """Sends a mail to shop admins that add new call  to order"""
    shop = plugins.core.utils.get_default_shop()

    subject = _(u"Поступила заявка на звонок")
    from_email = shop.from_email
    to = shop.get_notification_emails()


    # text
    text = render_to_string("mail/order_call.txt", {
        "phone" : phone,
        "name" : name
    })

    mail = EmailMultiAlternatives(
        subject=subject, body=text, from_email=from_email, to=to)

    mail.send(fail_silently=True)


def send_request_to_product_price(product,name,phone,email=None):
    shop = plugins.core.utils.get_default_shop()

    subject = _(u"Поступила заявка на товар")
    from_email = shop.from_email
    to = shop.get_notification_emails()


    # text
    text = render_to_string("mail/request_product_price.txt", {
        "phone" : phone,
        "name" : name,
        "email":email,
        "product":product,
    })

    mail = EmailMultiAlternatives(
        subject=subject, body=text, from_email=from_email, to=to)

    mail.send(fail_silently=True)



def send_request_to_vacancy(rezume):
    shop = plugins.core.utils.get_default_shop()
    subject = _(u"Поступила заявка на вакансию")
    from_email = shop.from_email
    to = list(shop.get_vacancy_email())

    text = render_to_string("mail/request_to_vacancy.txt", {"rezume":rezume,})
    mail = EmailMultiAlternatives(subject=subject, body=text, from_email=from_email, to=to)

    if rezume.file:
        file = open(rezume.file_abs_path)
        mail.attach(rezume.filename, file.read())
        file.close()
    mail.send(fail_silently=True)
