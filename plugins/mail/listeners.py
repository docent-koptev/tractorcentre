# lfs imports
from plugins.core.signals import customer_added, order_submitted, order_sent, vacancy_send
from plugins.mail import utils as mail_utils

def order_sent_listener(sender, **kwargs):
    """Listen to order payed signal.
    """
    order = sender.get("order")
    mail_utils.send_order_sent_mail(order)
order_sent.connect(order_sent_listener)

def order_submitted_listener(sender, **kwargs):
    """Listen to order submitted signal.
    """
    order = sender
    mail_utils.send_order_received_mail(order)
order_submitted.connect(order_submitted_listener)

def customer_added_listener(sender, password=None,**kwargs):
    """Listens to customer added signal.
    """
    mail_utils.send_customer_added(sender,password)
customer_added.connect(customer_added_listener)

def vacancy_send_listener(sender,**kwargs):
    rezume = sender
    mail_utils.send_request_to_vacancy(rezume)
vacancy_send.connect(vacancy_send_listener)