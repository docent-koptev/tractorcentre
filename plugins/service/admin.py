# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from django.db import models
from django import forms
from plugins.service.models import Service
from sorl.thumbnail import get_thumbnail
from sorl.thumbnail.admin import AdminImageMixin
from ckeditor.widgets import CKEditorWidget

from docent.seo import SEOMetadata
from rollyourown.seo.admin import get_inline


class ServiceForm(forms.ModelForm):
    body = forms.CharField(widget=CKEditorWidget(),label=u'Текст')
    class Meta:
        model = Service
        fields = "__all__"

class ServiceAdmin(AdminImageMixin, admin.ModelAdmin):
    inlines = [get_inline(SEOMetadata),]
    form = ServiceForm
    list_display = ['title', 'admin_image_preview', 'display', 'last_modification','position',]
    list_editable = ['display','position',]
    search_fields = ['title', 'body', ]
    list_filter = ['display',]
    readonly_fields = ['last_modification',]

    prepopulated_fields = {'slug': ['title',]}
    fieldsets = (
        (None,{
            'fields': ('title', 'slug', 'body',  'image', 'last_modification','display',  )
        }),
    )

    def admin_image_preview(self, obj):
        if obj.image:
            thumbnail = get_thumbnail(obj.image, '70')
            return '<a href="%s" target="_blank"><image style="max-height:70px;max-width:70px" src="%s"/></a>' % (obj.image.url, thumbnail.url)
        return ''
    admin_image_preview.short_description = u'превью'
    admin_image_preview.allow_tags = True

admin.site.register(Service, ServiceAdmin)