# -*- coding: utf-8 -*-
from django.http import Http404
from django.shortcuts import get_object_or_404
from annoying.decorators import render_to
from plugins.service.models import Service
from django.utils.translation import ugettext_lazy as _
from datetime import datetime

@render_to('service_detail.html')
def service(request, slug=''):
    all_services = Service.objects.filter(display=True)
    if not slug:
        service_item = Service.objects.all().order_by("position")[0]
    else:
        service_item = get_object_or_404(Service, slug=slug)
    if service_item.display:
        return { 'service_item': service_item,'all_services':all_services, }
    raise Http404('No %s matches the given query.' % service_item._meta.object_name)
