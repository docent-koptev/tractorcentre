from django.conf.urls import url, patterns

urlpatterns = patterns('plugins.service.views',
	#url(r'^$', 'service', name='service_start),
    url(r'^(?P<slug>[-\w]*)$', 'service', name='service'),
)