# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='service',
            options={'ordering': ('position',), 'verbose_name': '\u0441\u0435\u0440\u0432\u0438\u0441 \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438', 'verbose_name_plural': '\u0441\u0435\u0440\u0432\u0438\u0441\u044b \u043a\u043e\u043c\u043f\u0430\u043d\u0438\u0438'},
        ),
        migrations.AddField(
            model_name='service',
            name='position',
            field=models.IntegerField(default=999, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f'),
            preserve_default=True,
        ),
    ]
