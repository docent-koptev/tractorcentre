# -*- coding: utf-8 -*-
import datetime
from django.contrib.sitemaps import Sitemap
from django.db import models
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail.fields import ImageField

class Service(models.Model):
    title = models.CharField(_(u'заголовок'), max_length=255)
    slug = models.SlugField(unique=True,verbose_name=u'Ссылка',help_text=u'формирует вид урл cервиса.Уникальный элемент')
    body = models.TextField(_(u'текст сервиса'), blank=True,help_text=u'Полный текст сервиса')
    image = ImageField(_(u'изображение'), upload_to='uploads/news', blank=True)
    display = models.BooleanField(_(u'опубликовать'), default=True)
    last_modification = models.DateTimeField(_(u'дата последнего изменения'), blank=True)
    position = models.IntegerField(verbose_name=u'Позиция',default=999)

    class Meta:
        ordering = ('position',)
        verbose_name = _(u'сервис компании')
        verbose_name_plural = _(u'сервисы компании')

    def __unicode__(self):
        return self.title

    def save(self, force_insert=False, force_update=False, *args, **kw):
        self.last_modification = datetime.datetime.now()
        super(Service, self).save(*args, **kw)

    @models.permalink
    def get_absolute_url(self):
        return 'service', (), {'slug': self.slug}

class ServiceSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Service.objects.filter(display=True)