from django.conf.urls import url, patterns

urlpatterns = patterns('plugins.photogallery.views',
    url(r'^$', 'album_list', name='album_list'),
    url(r'^(?P<slug>[-\w]*)$', 'album_detail', name='album_detail'),
)