# -*- coding: utf-8 -*-
# django imports
from django.db import models
from autoslug import AutoSlugField
from django_resized import ResizedImageField

class Album(models.Model):
    name = models.CharField(u"Название альбома", max_length=250)
    slug = AutoSlugField(u"Ссылка", unique=True)
    description = models.TextField(u"Описание", blank=True, null=True)
    image = ResizedImageField(u'Картинка', upload_to='gallery/album',blank=True,null=True,help_text=u'Обложка альбома')
    active = models.BooleanField(u"Активность", default=True)

    class Meta:
        verbose_name = u"Альбом"
        verbose_name_plural = u'Альбомы'

    def __unicode__(self):
        return self.name


class Photo(models.Model):
    album = models.ForeignKey(Album, verbose_name=u'Альбом', related_name='photos',help_text=u'Обязательно выбирайте альбом')
    image = ResizedImageField(u'Фото', upload_to='gallery/photo',help_text=u'Для загрузки нескольких фото удерживайте ctrl')
    title = models.CharField(u'Название', max_length=255, blank=True, default='')

    class Meta:
        verbose_name = u"Фотография"
        verbose_name_plural = u'Фотографии'

    def __unicode__(self):
        return self.title