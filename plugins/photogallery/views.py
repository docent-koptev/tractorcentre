# -*- coding: utf-8 -*-
from django.http import Http404
from django.shortcuts import get_object_or_404
from annoying.decorators import render_to
from plugins.photogallery.models import *
from django.utils.translation import ugettext_lazy as _
from datetime import datetime
from utils.helpers import Paginate


@render_to('album_list.html')
def album_list(request):
    album_list = Album.objects.filter(active=True)
    return { 'album_list': album_list, }

@render_to('album_detail.html')
def album_detail(request, slug):
    album = get_object_or_404(Album, slug=slug)
    photo_list = Photo.objects.filter(album=album)
    return { 'photo_list': photo_list,'album':album,}
