# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms
from django.shortcuts import get_object_or_404, redirect, render,HttpResponse

from sorl.thumbnail import get_thumbnail

from plugins.photogallery.models import *
from plugins.photogallery.widgets import MultiFileInput

class AlbumAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug': ['name',]}
	list_display = ['name', 'admin_image_preview', 'active',]

	def admin_image_preview(self, obj):
		if obj.image:
			thumbnail = get_thumbnail(obj.image, '70')
			return '<a href="%s" target="_blank"><image style="max-height:70px;max-width:70px" src="%s"/></a>' % (obj.image.url, thumbnail.url)
		return ''
	admin_image_preview.short_description = u'превью'
	admin_image_preview.allow_tags = True
 
class PhotoAdminForm(forms.ModelForm):
 
    class Meta:
        model = Photo
        widgets = {'image':MultiFileInput}
        fields = "__all__"
 
class PhotoAdmin(admin.ModelAdmin):
	form = PhotoAdminForm
	list_display = ['id','title','admin_image_preview','album',]
	list_editable = ['title',]
	list_filter = ['album',]
	
	fieldsets = (
        ('Альбом и фотографии',{
            'fields': ('album','image',)
        }),
    )

	def admin_image_preview(self, obj):
		if obj.image:
			thumbnail = get_thumbnail(obj.image, '70')
			return '<a href="%s" target="_blank"><image style="max-height:70px;max-width:70px" src="%s"/></a>' % (obj.image.url, thumbnail.url)
		return ''
	admin_image_preview.short_description = u'превью'
	admin_image_preview.allow_tags = True

	def add_view(self, request, *args, **kwargs):
		images = request.FILES.getlist('image',[])
		is_valid = PhotoAdminForm(request.POST, request.FILES).is_valid()

		if request.method == 'GET' or len(images)<=1 or not is_valid:
			return super(PhotoAdmin, self).add_view(request, *args, **kwargs)
		for image in images:
			album_id=request.POST['album']
			album = Album.objects.get(pk=int(album_id))
			try:
				photo = Photo(album=album, image=image)
				photo.save()
			except Exception, e:
				messages.error(request, smart_str(e)) 

		return redirect('/admin/photogallery/photo/')
	 
admin.site.register(Album, AlbumAdmin)
admin.site.register(Photo, PhotoAdmin)