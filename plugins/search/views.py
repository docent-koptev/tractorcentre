# django imports
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.loader import render_to_string
import json

from plugins.catalog.models import *
from plugins.catalog.settings import STANDARD_PRODUCT, PRODUCT_WITH_VARIANTS, VARIANT
from docent.views import get_product_sorter_params

from utils.helpers import Paginate
from annoying.decorators import render_to

def get_query(q):
    return Q(active=True) & (Q(name__icontains=q) | Q(manufacturer__name__icontains=q))

def livesearch(request, template_name="search/livesearch_results.html"):
    """
    """
    q = request.GET.get("q", "")
    q = unicode(q.strip())

    if q == "":
        result = json.dumps({
            "state" : "failure",
        })
    else:
        # Products
        temp = Product.objects.filter(get_query(q)).exclude(categories__isnull=True)
        total = len(temp)
        products = temp[0:5]

        if products:
            products = render_to_string(template_name, RequestContext(request, {
                "products" : products,
                "q" : q,
                "total" : total,
            }))

            result = json.dumps({
                "state" : "success",
                "products" : products,
            })
        else:
            result = json.dumps({
            "state" : "failure",
            })

    return HttpResponse(result, content_type='application/json')

def search(request):
    count_show, page = get_product_sorter_params(request)
    template="search/search_results.html"
    ref = request.META.get('HTTP_REFERER','/')
    if ref == 'http://%s/' % request.get_host():
        ref = None

    q = request.GET.get("q", "")
    q = q.strip()

    if q:
        products_list= Product.objects.filter(get_query(q))
        products = Paginate(request,products_list,count_show)
        filter_list = {}
        tech_info = ProductTechInfo.objects.filter(product__in=list(products_list)).order_by("name")
        for p in tech_info:
            filter_list[p.value] = p.pk
        tech_info = tech_info.filter(pk__in=list(filter_list.values()))

        if request.is_ajax():
            params = None
            template = 'catalog/catalog_ajax.html'
            if 'data' in request.POST:
                params = request.POST['data']
            if params is not None:
                data = params.encode("utf-8")
                filters_elements = json.loads(data)
                products_pks = []
                products_pks_tmp = []
                v = {}
                for key, value in sorted(filters_elements.iteritems()):
                    v.setdefault(value, []).append(key)
                for key, value in v.iteritems():
                    for p in value:
                        tech_info = ProductTechInfo.objects.filter(name__name=unicode(key),value=unicode(p))
                        for ps in tech_info:
                            products_pks_tmp.append(ps.product.pk)
                if products_pks_tmp:
                    dict_len = len(v)
                    for el in products_pks_tmp:
                        count = products_pks_tmp.count(el)
                        if count == dict_len:
                            products_pks.append(el)


                products_list = products_list.filter(pk__in=products_pks)
                products = Paginate(request,products_list,count_show)
            return render_to_response(template,locals(),context_instance=RequestContext(request))
        total = 0
        if products:
            total += len(list(products_list))

        
    return render_to_response(template,locals(),context_instance=RequestContext(request))
