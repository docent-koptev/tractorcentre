# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _

CHECKOUT_TYPE_SELECT = 0
CHECKOUT_TYPE_ANON = 1
CHECKOUT_TYPE_AUTH = 2
CHECKOUT_TYPES = (
    (CHECKOUT_TYPE_SELECT, _(u"Anonymous and Authenticated")),
    (CHECKOUT_TYPE_ANON,   _(u"Anonymous only")),
    (CHECKOUT_TYPE_AUTH,   _(u"Authenticated only")),
)

PAYMENT_METHOD_CASH = 0
#PAYMENT_METHOD_CASHLESS = 1
PAYMENT_METHOD_CASHLESS_ROBOKASSA = 2
PAYMENT_METHOD = (
    (PAYMENT_METHOD_CASH, u'Наличными'),
    #(PAYMENT_METHOD_CASHLESS, u'Безналично'),
    (PAYMENT_METHOD_CASHLESS_ROBOKASSA, u'Безналично через робокассу (+5% к стоимости заказа)'),
)