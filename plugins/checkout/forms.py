# -*- coding: utf-8 -*-
# python imports
from datetime import datetime

# django imports
from django import forms
from django.contrib.auth.models import User
from django.db.models import Q
from django.forms.util import ErrorList
from django.utils.encoding import force_unicode
from django.utils.html import escape, conditional_escape
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.models import get_current_site

# lfs imports
from plugins.catalog.models import Product
from plugins.checkout.settings import PAYMENT_METHOD
from plugins.core.utils import get_default_shop

class MainFormCustomer(forms.Form):
    fio = forms.CharField(label=_(u"ФИО"), max_length=250,required=True,widget=forms.TextInput(attrs={'class':'form-control'}))
    email = forms.EmailField(label=_(u"Эл.почта"), max_length=250,required=True,widget=forms.TextInput(attrs={'class':'form-control'}))
    phone = forms.CharField(label=_(u"Телефон"), max_length=250,required=True,widget=forms.TextInput(attrs={'class':'form-control'}))
    dop_phone = forms.CharField(label=_(u"Доп.телефон"), required=False,max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}))

class ProcessForPreparingForm(forms.Form):
    region = forms.CharField(label=_(u"Область"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Начните вводить...'}))
    city = forms.CharField(label=_(u"Город"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Начните вводить...'}))
    street = forms.CharField(label=_(u"Улица"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Начните вводить...'}))
    building = forms.CharField(label=_(u"Дом"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Начните вводить...'}))
    corpus = forms.CharField(label=_(u"Корпус"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}),required=False)
    builder = forms.CharField(label=_(u"Строение"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}),required=False)
    office = forms.CharField(label=_(u"Офис"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}),required=False)

class PaydForm(forms.Form):
    organization = forms.CharField(label=_(u"Организация"), max_length=250,  widget=forms.TextInput(attrs={'class':'form-control'}),required=True)
    inn = forms.CharField(label=_(u"ИНН"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}),required=True)
    kpp = forms.CharField(label=_(u"КПП"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}),required=True)
    file = forms.FileField(required=True)


class FormForLk(forms.Form):
    fio = forms.CharField(label=_(u"ФИО"), max_length=250,required=True,widget=forms.TextInput(attrs={'class':'form-control'}))
    email = forms.EmailField(label=_(u"Эл.почта"), max_length=250,required=True,widget=forms.TextInput(attrs={'class':'form-control'}))
    phone = forms.CharField(label=_(u"Телефон"), max_length=250,required=True,widget=forms.TextInput(attrs={'class':'form-control'}))
    dop_phone = forms.CharField(label=_(u"Доп.телефон"), required=False,max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}))

    organization = forms.CharField(label=_(u"Организация"), max_length=250,  widget=forms.TextInput(attrs={'class':'form-control'}),required=False)
    inn = forms.CharField(label=_(u"ИНН"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}),required=False)
    kpp = forms.CharField(label=_(u"КПП"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}),required=False)

    region = forms.CharField(label=_(u"Область"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}),required=False)
    city = forms.CharField(label=_(u"Город"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}),required=False)
    street = forms.CharField(label=_(u"Улица"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}),required=False)
    building = forms.CharField(label=_(u"Дом"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}),required=False)
    corpus = forms.CharField(label=_(u"Корпус"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}),required=False)
    builder = forms.CharField(label=_(u"Строение"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}),required=False)
    office = forms.CharField(label=_(u"Офис"), max_length=250,widget=forms.TextInput(attrs={'class':'form-control'}),required=False)


class OneClickCheckoutForm(forms.Form):
    phone = forms.CharField(max_length=20)
    product = forms.ModelChoiceField(queryset=Product.objects.filter(active=True))