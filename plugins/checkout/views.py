# -*- coding: utf-8 -*-
# django imports
#from django.contrib.auth.forms import AuthenticationForm
import json
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.http import Http404
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404, redirect,HttpResponse
from django.template.loader import render_to_string
from django.template import RequestContext
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.http import require_POST

from annoying.decorators import render_to, ajax_request
from annoying.functions import get_object_or_None
from plugins.checkout.settings import PAYMENT_METHOD_CASHLESS_ROBOKASSA, PAYMENT_METHOD_CASH
import plugins.core.utils
import plugins.core.signals
import plugins.order.utils

from plugins.core.signals import order_submitted
from plugins.cart import utils as cart_utils
from plugins.checkout.forms import *
from plugins.customer import utils as customer_utils
from plugins.customer.models import CustomerAttributes
from plugins.order import utils as order_utils
from plugins.order.models import Order, OrderItem
from plugins.contacts.models import ShopAddress

def checkout_dispatcher(request):
    """Dispatcher to display the correct checkout form
    """
    cart = cart_utils.get_cart(request)

    if cart is None or not cart.items():
        return HttpResponseRedirect(reverse("cart"))

    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse("checkout"))
    else:
        request.session['not_auth'] = True
        return HttpResponseRedirect(reverse("cart"))

def cart_inline(request, template_name="checkout/checkout_cart_inline.html"):
    cart = cart_utils.get_cart(request)
    cart_costs = cart_utils.get_cart_costs(request, cart)
    cart_price = cart_costs["price"] 

    return render_to_string(template_name, RequestContext(request, {
        "cart" : cart,
        "cart_price" : cart_price,
    }))


def one_page_checkout(request,template_name="checkout/one_page_checkout.html"):

    customer = customer_utils.get_or_create_customer(request)
    customer_attributes = customer_utils.get_or_create_customer_attributes(request)
    cart = cart_utils.get_cart(request)
    cart_costs = cart_utils.get_cart_costs(request, cart)
    cart_price = cart_costs["price"] 
    shops = ShopAddress.objects.all()
    
    main_form = MainFormCustomer(initial={
        'fio': customer_attributes.fio,
        'email':customer_attributes.email,
        'phone':customer_attributes.phone,
        'dop_phone':customer_attributes.dop_phone,
        })

    paymentForm = PaydForm(initial={
        'organization': customer_attributes.organization,
        'inn':customer_attributes.inn,
        'kpp':customer_attributes.kpp,
        })

    delivForm = ProcessForPreparingForm(initial={
        'region': customer_attributes.region,
        'city':customer_attributes.city,
        'street':customer_attributes.street,
        'building':customer_attributes.house,
        'corpus':customer_attributes.corpus,
        'builder':customer_attributes.stroenie,
        'office':customer_attributes.office,
        })

    if request.is_ajax():
        response = {'status':0,'errors':None}
        form_errors = {}
        main_form = MainFormCustomer(request.POST)
        if not main_form.is_valid():
            for k in main_form.errors:
                form_errors[k] = main_form.errors[k][0]
        else:
            cd = main_form.cleaned_data
            if customer_attributes is None:
                #create customer attrs
                CustomerAttributes.objects.create(
                    fio = cd.get("fio"),
                    email = cd.get("email"),
                    phone = cd.get("phone"),
                    dop_phone = cd.get("dop_phone"),
                    customer = customer)
            else:
                #update customer info 
                customer_attributes.fio = cd.get("fio")
                customer_attributes.email = cd.get("email")
                customer_attributes.phone = cd.get("phone")
                customer_attributes.dop_phone = cd.get("dop_phone")
                customer_attributes.save()

        if 'obtaining' in request.POST:
            obtaining = request.POST.get('obtaining')
            if obtaining == 'transport':
                obtainingForm = ProcessForPreparingForm(request.POST)
                if not obtainingForm.is_valid():
                    for k in obtainingForm.errors:
                        form_errors[k] = obtainingForm.errors[k][0]
                else:
                    cd = obtainingForm.cleaned_data

                    customer_attributes.region = cd.get("region")
                    customer_attributes.city = cd.get("city")
                    customer_attributes.street = cd.get("street")
                    customer_attributes.house = cd.get("building")
                    customer_attributes.corpus = cd.get("corpus","")
                    customer_attributes.stroenie = cd.get("builder","")
                    customer_attributes.office = cd.get("office","")

                    customer_attributes.save()

        if 'payment_method' in request.POST:
            payment_method = request.POST.get("payment_method")
            if payment_method == 'beznal':
                paymentForm = PaydForm(request.POST,request.FILES)
                if not paymentForm.is_valid():
                    for k in paymentForm.errors:
                        form_errors[k] = paymentForm.errors[k][0]
                else:
                    cd = paymentForm.cleaned_data
                    customer_attributes.organization  = cd.get("organization")             
                    customer_attributes.inn = cd.get("inn")
                    customer_attributes.kpp = cd.get("kpp")
                    
                    customer_attributes.save()

        if len(form_errors) > 0:
            response = {'status':1,'errors':form_errors}
            return HttpResponse(json.dumps(response))
        else:
            order = order_utils.add_order(request)
            order_submitted.send(sender=order,order=order, request=request)
            response = {'status':100,'errors':form_errors}

        return HttpResponse(json.dumps(response))

    response = render_to_response(template_name, RequestContext(request, {
        "cart": cart,
        "customer": customer,
        "cart_inline" : cart_inline(request),
        "cart_price" :cart_price,
        "shops":shops,
        "customer_attributes":customer_attributes,
        "paymentForm":paymentForm,
        "main_form":main_form,
        "delivForm":delivForm,
    }))
    return response


def empty_page_checkout(request, template_name="checkout/empty_page_checkout.html"):
    return render_to_response(template_name, RequestContext(request, {
        "shopping_url" : reverse("home"),
    }))

def thank_you(request, template_name="checkout/thank_you_page.html"):
    """Displays a thank you page ot the customer
    """
    order = request.session.get("order")
    return render_to_response(template_name, RequestContext(request, {
        "order" : order,
    }))


