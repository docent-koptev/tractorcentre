# -*- coding: utf-8 -*-
from django.contrib import admin
from plugins.customer.models import Customer,CustomerAttributes


class CustomerAttributesline(admin.StackedInline):
	fieldsets = (
        ('Основное',{
            'fields': ('fio','email','phone', 'dop_phone',)
        }),
        ('Адрес доставки',{
            'fields': ('region','city','street', 'house','corpus','stroenie','office')
        }),
       	('Реквизиты (для юридических лиц)',{
            'fields': ('organization','inn','kpp',)
        }),
    )
	model = CustomerAttributes
	def get_extra(self, request, obj=None, **kwargs):
		extra = 0
		return extra

class CustomerAdmin(admin.ModelAdmin):
	list_display = ('pk','user','urik',)
	list_filter = ('user',)
	readonly_fields = ('user','session',)
	inlines = [CustomerAttributesline,]


admin.site.register(Customer, CustomerAdmin)