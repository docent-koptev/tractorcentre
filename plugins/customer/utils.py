# django imports
import string
import random
from django.core.exceptions import ObjectDoesNotExist
from annoying.functions import get_object_or_None
from plugins.customer.models import Customer,CustomerAttributes

def get_or_create_customer(request):
    customer = get_customer(request)
    if customer is None:
        customer = create_customer(request)
    if 'urik' in request.POST:
        customer.urik = True
        customer.save()
    return customer

def create_customer(request):
    customer = Customer(session = request.session.session_key)
    if request.user.is_authenticated():
        customer.user = request.user
        if 'urik' in request.POST:
            customer.urik = True
    customer.save()
    return customer

def get_customer(request):
    session_key = request.session.session_key
    user = request.user

    return get_customer_by_user_or_session(user, session_key)
            
def update_customer_after_login(request):
    """Updates the customer after login.
    
    1. If there is no session customer, nothing has to be done.
    2. If there is a session customer and no user customer we assign the session 
       customer to the current user.
    3. If there is a session customer and a user customer we copy the session 
       customer information to the user customer and delete the session customer
    """
    try:
        session_customer = Customer.objects.get(session = request.session.session_key)
        try:
            user_customer = Customer.objects.get(user = request.user)
        except ObjectDoesNotExist:
            session_customer.user = request.user
            session_customer.save()
        else:
            user_customer.save()
            session_customer.delete()
    except ObjectDoesNotExist:
        pass


def get_customer_by_user_or_session(user=None, session_key=None):
    """Returns the customer for the given request (which means for the current
    logged in user/or the session user).
    """
    if user and user.is_authenticated():
        try:
            return Customer.objects.get(user = user)
        except ObjectDoesNotExist:
            return None
    elif session_key:
        try:
            return Customer.objects.get(session = session_key)
        except ObjectDoesNotExist:
            return None

def pass_generator(size=12, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def get_or_create_customer_attributes(request):
    customer = get_or_create_customer(request)
    customer_attributes = get_object_or_None(CustomerAttributes,customer=customer)
    if customer_attributes is None:
        customer_attributes = CustomerAttributes.objects.create()
        customer_attributes.customer = customer
        customer_attributes.save()
    return customer_attributes

