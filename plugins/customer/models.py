# -*- coding: utf-8 -*-
# django imports
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _

class Customer(models.Model):
    user = models.OneToOneField(User, blank=True, null=True,verbose_name=u'Почта пользователя')
    session = models.CharField(blank=True, max_length=100)
    urik = models.BooleanField(u'Юридическое лицо',default=False)

    class Meta:
        verbose_name = u"Покупатель"
        verbose_name_plural = u'Покупатели'

    def __unicode__(self):
        return "%s/%s" % (self.user, self.session)

class CustomerAttributes(models.Model):
    customer = models.ForeignKey(Customer, verbose_name=_(u"Customer"), blank=True, null=True, related_name="addresses")
    
    fio = models.CharField(u"ФИО", max_length=50,blank=True, null=True)
    email = models.EmailField(u'Почта', blank=True, null=True, max_length=50)
    phone = models.CharField(u'Телефон',blank=True,null=True,max_length=200)
    dop_phone = models.CharField(u'Доп.телефон',blank=True,null=True,max_length=200)

    region = models.CharField(u'Регион',blank=True,null=True,max_length=200)
    city = models.CharField(u'Город',blank=True,null=True,max_length=200)
    street = models.CharField(u'Улица',blank=True,null=True,max_length=200)
    house = models.CharField(u'Дом',blank=True,null=True,max_length=200)
    corpus = models.CharField(u'Корпус',blank=True,null=True,max_length=200)
    stroenie = models.CharField(u'Строение',blank=True,null=True,max_length=200)
    office = models.CharField(u'Офис',blank=True,null=True,max_length=200)

    organization = models.CharField(u'Организация',blank=True,null=True,max_length=200)
    inn = models.CharField(u'ИНН',blank=True,null=True,max_length=200)
    kpp = models.CharField(u'КПП',blank=True,null=True,max_length=200)

    class Meta:
        verbose_name = u"Информация о покупателе"
        verbose_name_plural = u'Информация о покупателях'

