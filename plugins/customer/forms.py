# -*- coding: utf-8 -*-
# django imports
from django import forms
from django.contrib.auth.models import User
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import get_user_model

from django.contrib.auth import authenticate

class RegisterForm(forms.Form):
    email = forms.EmailField(label=_(u"E-mail"), widget=forms.TextInput(attrs={'class': 'text','placeholder':'E-mail'}), max_length=50)
    urik =  forms.BooleanField(required=False)
    
    def clean_email(self):
        """Validates that the entered e-mail is unique.
        """
        email = self.cleaned_data.get("email")
        if email and User.objects.filter(Q(email=email) | Q(username=email)).count() > 0:
            raise forms.ValidationError(
                _(u"That email address is already in use."))

        return email

class AuthenticationForm(forms.Form):
    username = forms.CharField(label=_("Username"), max_length=30,widget=forms.TextInput(attrs={'placeholder': 'E-mail *'}))
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput(attrs={'placeholder': 'Пароль *'}))

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        self.user_cache = None
        super(AuthenticationForm, self).__init__(*args, **kwargs)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username, password=password)
            if self.user_cache is None:
                raise forms.ValidationError(_(u"Пользователь не найден. Проверьте правильность ввода логина и пароля"))
            elif not self.user_cache.is_active:
                raise forms.ValidationError(_("This account is inactive."))
        self.check_for_test_cookie()
        return self.cleaned_data

    def check_for_test_cookie(self):
        if self.request and not self.request.session.test_cookie_worked():
            raise forms.ValidationError(
                _("Your Web browser doesn't appear to have cookies enabled. "
                  "Cookies are required for logging in."))

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache


class CompleteReg(forms.Form):
    User = get_user_model()
    username = forms.RegexField(label=u'Имя пользователя', max_length=30, min_length=4,
                                required=True, regex=r'^[\w.@+-]+$')
    email = forms.EmailField(label=u'Email', required=True)


    def __init__(self, user_id, *args, **kwargs):
        super(CompleteReg, self).__init__(*args, **kwargs)
        self.user_id = user_id

    def clean_username(self):
        if self.cleaned_data['username']:
            try: 
                u = User.objects.exclude(id=self.user_id).get(username=self.cleaned_data['username'])
            # if username is unique - it's ok
            except User.DoesNotExist: u = None

            if u is not None:
                raise forms.ValidationError(u'Пользователь с таким именем уже зарегистрирован')
        return self.cleaned_data['username']

    def clean_email(self):
        if self.cleaned_data['email']:
            try: u = User.objects.exclude(id=self.user_id).get(email=self.cleaned_data['email'])
            # if email is unique - it's ok
            except User.DoesNotExist: u = None

            if u is not None:
                raise forms.ValidationError(u'Пользователь с этим адресом уже зарегистрирован')
        return self.cleaned_data['email']

class EmailForm(forms.Form):
    email = forms.EmailField(label=_(u"E-mail"), max_length=50, widget=forms.TextInput(attrs={'class': 'text','placeholder':'Введите новый email'}))

class OrderCallPhone(forms.Form):
    """Form to order phone call"""
    name = forms.CharField(label=u"Имя", max_length=50,widget=forms.TextInput(attrs={'class':'form-control',}))
    phone= forms.CharField(label=u"Телефон", max_length=50,widget=forms.TextInput(attrs={'class':'form-control phone','type':'tel',}))

