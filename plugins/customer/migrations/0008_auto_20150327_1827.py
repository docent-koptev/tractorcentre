# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0007_auto_20150327_1725'),
    ]

    operations = [
        migrations.AddField(
            model_name='customerattributes',
            name='city',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0413\u043e\u0440\u043e\u0434', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customerattributes',
            name='corpus',
            field=models.CharField(max_length=200, null=True, verbose_name='\u041a\u043e\u0440\u043f\u0443\u0441', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customerattributes',
            name='house',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0414\u043e\u043c', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customerattributes',
            name='office',
            field=models.CharField(max_length=200, null=True, verbose_name='\u041e\u0444\u0438\u0441', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customerattributes',
            name='region',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0420\u0435\u0433\u0438\u043e\u043d', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customerattributes',
            name='street',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0423\u043b\u0438\u0446\u0430', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customerattributes',
            name='stroenie',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0421\u0442\u0440\u043e\u0435\u043d\u0438\u0435', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='customerattributes',
            name='fio',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0424\u0418\u041e', blank=True),
            preserve_default=True,
        ),
    ]
