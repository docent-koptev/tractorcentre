# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0005_auto_20150327_1722'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customer',
            name='dop_phone',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='email',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='inn',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='kpp',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='organization',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='phone',
        ),
    ]
