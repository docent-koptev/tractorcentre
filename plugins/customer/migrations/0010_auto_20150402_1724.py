# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0009_auto_20150328_1609'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='customerattributes',
            options={'verbose_name': '\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f \u043e \u043f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u0435', 'verbose_name_plural': '\u0418\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f \u043e \u043f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u044f\u0445'},
        ),
        migrations.AlterField(
            model_name='customer',
            name='user',
            field=models.OneToOneField(null=True, blank=True, to=settings.AUTH_USER_MODEL, verbose_name='\u041f\u043e\u0447\u0442\u0430 \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f'),
            preserve_default=True,
        ),
    ]
