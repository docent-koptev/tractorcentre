# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0002_auto_20150326_1753'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customer',
            name='selecder_delivery_region',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='selected_country',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='selected_invoice_address',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='selected_region',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='selected_shipping_address',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='selected_shipping_method',
        ),
    ]
