# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0006_auto_20150327_1724'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customerattributes',
            name='apart',
        ),
        migrations.RemoveField(
            model_name='customerattributes',
            name='city',
        ),
        migrations.RemoveField(
            model_name='customerattributes',
            name='company_name',
        ),
        migrations.RemoveField(
            model_name='customerattributes',
            name='country',
        ),
        migrations.RemoveField(
            model_name='customerattributes',
            name='flat',
        ),
        migrations.RemoveField(
            model_name='customerattributes',
            name='home',
        ),
        migrations.RemoveField(
            model_name='customerattributes',
            name='intercom',
        ),
        migrations.RemoveField(
            model_name='customerattributes',
            name='korp',
        ),
        migrations.RemoveField(
            model_name='customerattributes',
            name='lastname',
        ),
        migrations.RemoveField(
            model_name='customerattributes',
            name='region',
        ),
        migrations.RemoveField(
            model_name='customerattributes',
            name='state',
        ),
        migrations.RemoveField(
            model_name='customerattributes',
            name='street',
        ),
        migrations.RemoveField(
            model_name='customerattributes',
            name='zip_code',
        ),
        migrations.AddField(
            model_name='customerattributes',
            name='dop_phone',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0414\u043e\u043f.\u0442\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customerattributes',
            name='inn',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0418\u041d\u041d', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customerattributes',
            name='kpp',
            field=models.CharField(max_length=200, null=True, verbose_name='\u041a\u041f\u041f', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customerattributes',
            name='organization',
            field=models.CharField(max_length=200, null=True, verbose_name='\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u044f', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='customerattributes',
            name='email',
            field=models.EmailField(max_length=50, null=True, verbose_name='\u041f\u043e\u0447\u0442\u0430', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='customerattributes',
            name='phone',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
            preserve_default=True,
        ),
    ]
