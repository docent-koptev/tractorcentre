# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('shipping', '__first__'),
        ('core', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('firstname', models.CharField(max_length=50, verbose_name='Firstname')),
                ('lastname', models.CharField(max_length=50, verbose_name='Lastname', blank=True)),
                ('company_name', models.CharField(max_length=50, null=True, verbose_name='Company name', blank=True)),
                ('zip_code', models.CharField(max_length=10, verbose_name='Zip code', blank=True)),
                ('city', models.CharField(max_length=50, verbose_name='City', blank=True)),
                ('street', models.CharField(max_length=100, verbose_name='Street', blank=True)),
                ('home', models.CharField(max_length=10, verbose_name='\u0414\u043e\u043c', blank=True)),
                ('korp', models.CharField(max_length=10, verbose_name='\u041a\u043e\u0440\u043f\u0443\u0441', blank=True)),
                ('intercom', models.CharField(max_length=10, verbose_name='\u0414\u043e\u043c\u043e\u0444\u043e\u043d', blank=True)),
                ('flat', models.CharField(max_length=10, verbose_name='\u042d\u0442\u0430\u0436', blank=True)),
                ('apart', models.CharField(max_length=10, verbose_name='\u041a\u0432\u0430\u0440\u0442\u0438\u0440\u0430', blank=True)),
                ('state', models.CharField(max_length=50, verbose_name='State', blank=True)),
                ('phone', models.CharField(max_length=20, verbose_name='Phone', blank=True)),
                ('email', models.EmailField(max_length=50, null=True, verbose_name='E-Mail', blank=True)),
                ('country', models.ForeignKey(verbose_name='Country', blank=True, to='core.Country', null=True)),
            ],
            options={
                'verbose_name': '\u0430\u0434\u0440\u0435\u0441',
                'verbose_name_plural': '\u0430\u0434\u0440\u0435\u0441\u0430',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('session', models.CharField(max_length=100, blank=True)),
                ('selecder_delivery_region', models.ForeignKey(verbose_name='Selected delivery region', blank=True, to='shipping.DeliveryRegion', null=True)),
                ('selected_country', models.ForeignKey(verbose_name='Selected country', blank=True, to='core.Country', null=True)),
                ('selected_invoice_address', models.ForeignKey(related_name='selected_invoice_address', verbose_name='Selected invoice address', blank=True, to='customer.Address', null=True)),
                ('selected_region', models.ForeignKey(verbose_name='Selected region', blank=True, to='core.Region', null=True)),
                ('selected_shipping_address', models.ForeignKey(related_name='selected_shipping_address', verbose_name='Selected shipping address', blank=True, to='customer.Address', null=True)),
                ('selected_shipping_method', models.ForeignKey(related_name='selected_shipping_method', verbose_name='Selected shipping method', blank=True, to='shipping.ShippingMethod', null=True)),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': '\u043f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u044c',
                'verbose_name_plural': '\u043f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='address',
            name='customer',
            field=models.ForeignKey(related_name='addresses', verbose_name='Customer', blank=True, to='customer.Customer', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='address',
            name='region',
            field=models.ForeignKey(verbose_name='\u0420\u0435\u0433\u0438\u043e\u043d', blank=True, to='core.Region', null=True),
            preserve_default=True,
        ),
    ]
