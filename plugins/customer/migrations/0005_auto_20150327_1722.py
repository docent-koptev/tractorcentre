# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_shopaddress'),
        ('customer', '0004_auto_20150326_1757'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomerAttributes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fio', models.CharField(max_length=50, verbose_name='Firstname')),
                ('lastname', models.CharField(max_length=50, verbose_name='Lastname', blank=True)),
                ('company_name', models.CharField(max_length=50, null=True, verbose_name='Company name', blank=True)),
                ('zip_code', models.CharField(max_length=10, verbose_name='Zip code', blank=True)),
                ('city', models.CharField(max_length=50, verbose_name='City', blank=True)),
                ('street', models.CharField(max_length=100, verbose_name='Street', blank=True)),
                ('home', models.CharField(max_length=10, verbose_name='\u0414\u043e\u043c', blank=True)),
                ('korp', models.CharField(max_length=10, verbose_name='\u041a\u043e\u0440\u043f\u0443\u0441', blank=True)),
                ('intercom', models.CharField(max_length=10, verbose_name='\u0414\u043e\u043c\u043e\u0444\u043e\u043d', blank=True)),
                ('flat', models.CharField(max_length=10, verbose_name='\u042d\u0442\u0430\u0436', blank=True)),
                ('apart', models.CharField(max_length=10, verbose_name='\u041a\u0432\u0430\u0440\u0442\u0438\u0440\u0430', blank=True)),
                ('state', models.CharField(max_length=50, verbose_name='State', blank=True)),
                ('phone', models.CharField(max_length=20, verbose_name='Phone', blank=True)),
                ('email', models.EmailField(max_length=50, null=True, verbose_name='E-Mail', blank=True)),
                ('country', models.ForeignKey(verbose_name='Country', blank=True, to='core.Country', null=True)),
                ('customer', models.ForeignKey(related_name='addresses', verbose_name='Customer', blank=True, to='customer.Customer', null=True)),
                ('region', models.ForeignKey(verbose_name='\u0420\u0435\u0433\u0438\u043e\u043d', blank=True, to='core.Region', null=True)),
            ],
            options={
                'verbose_name': '\u0430\u0434\u0440\u0435\u0441',
                'verbose_name_plural': '\u0430\u0434\u0440\u0435\u0441\u0430',
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='address',
            name='country',
        ),
        migrations.RemoveField(
            model_name='address',
            name='customer',
        ),
        migrations.RemoveField(
            model_name='address',
            name='region',
        ),
        migrations.DeleteModel(
            name='Address',
        ),
    ]
