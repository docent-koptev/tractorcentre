# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0010_auto_20150402_1724'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='urik',
            field=models.BooleanField(default=False, verbose_name='\u042e\u0440\u0438\u0434\u0438\u0447\u0435\u0441\u043a\u043e\u0435 \u043b\u0438\u0446\u043e'),
            preserve_default=True,
        ),
    ]
