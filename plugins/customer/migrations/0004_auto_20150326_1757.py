# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0003_auto_20150326_1753'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='customer',
            options={'verbose_name': '\u041f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u044c', 'verbose_name_plural': '\u041f\u043e\u043a\u0443\u043f\u0430\u0442\u0435\u043b\u0438'},
        ),
        migrations.AddField(
            model_name='customer',
            name='dop_phone',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0414\u043e\u043f.\u0442\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customer',
            name='email',
            field=models.EmailField(max_length=50, null=True, verbose_name='\u041f\u043e\u0447\u0442\u0430', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customer',
            name='inn',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0418\u041d\u041d', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customer',
            name='kpp',
            field=models.CharField(max_length=200, null=True, verbose_name='\u041a\u041f\u041f', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customer',
            name='organization',
            field=models.CharField(max_length=200, null=True, verbose_name='\u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0446\u0438\u044f', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='customer',
            name='phone',
            field=models.CharField(max_length=200, null=True, verbose_name='\u0422\u0435\u043b\u0435\u0444\u043e\u043d', blank=True),
            preserve_default=True,
        ),
    ]
