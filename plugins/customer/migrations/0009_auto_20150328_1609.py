# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0008_auto_20150327_1827'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customerattributes',
            name='fio',
            field=models.CharField(max_length=50, null=True, verbose_name='\u0424\u0418\u041e', blank=True),
            preserve_default=True,
        ),
    ]
