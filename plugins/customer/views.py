# -*- coding: utf-8 -*-
from urlparse import urlparse

## django imports
from django import http
from django.contrib import messages, auth
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render_to_response,redirect,HttpResponse
from django.template import RequestContext
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm

## plugins imports
from plugins.core import utils as core_utils
from plugins.core.signals import customer_added
from plugins.core.monkeys import lfs_login
from plugins.customer.forms import EmailForm, RegisterForm
from plugins.order.models import Order
from plugins.customer import utils as customer_utils
from plugins.customer.forms import AuthenticationForm
from plugins.customer.utils import pass_generator
from plugins.customer.models import CustomerAttributes
from plugins.checkout.forms import *

from forms import CompleteReg
import json

def login(request):
    shop = core_utils.get_default_shop()
    redirect_to = request.REQUEST.get("next")

    response = {'status':-1,'errors':None,'next':''}
    if request.POST.get("action") == "login":
        login_form = AuthenticationForm(data=request.POST)

        if login_form.is_valid():
            lfs_login(request, login_form.get_user())
            cd = login_form.cleaned_data
            redirect_to = request.POST.get("next")
            response = {'status':0,'errors':None,'next':redirect_to}
        else:
            errors = {}
            for k in login_form.errors:
                errors[k] = login_form.errors[k][0]
            response = {'status':1,'errors':errors}

    if request.POST.get("action") == "register":
        register_form = RegisterForm(data=request.POST)
        if register_form.is_valid():
            cd = register_form.cleaned_data
            email = cd["email"]
            password = pass_generator()
            # Create user
            user = User.objects.create_user(username=email, email=email, password=password)

            # Create customer
            customer = customer_utils.get_or_create_customer(request)
            customer_attributes = customer_utils.get_or_create_customer_attributes(request)
            customer_added.send(sender=user,password=password)
            user = authenticate(username=email, password=password)
            lfs_login(request, user)
            redirect_to = request.POST.get("next")
            response = {'status':0,'errors':None,'next':redirect_to}
        else:
            errors = {}
            for k in register_form.errors:
                errors[k] = register_form.errors[k][0]
            response = {'status':1,'errors':errors,'next':redirect_to}
    return HttpResponse(json.dumps(response))

@login_required(login_url='/?show_login=True')
def orders(request):
    """Displays the orders of the current user
    """
    orders = Order.objects.filter(user=request.user)

    return render_to_response("customer/orders.html", RequestContext(request, {
        "orders" : orders,
    }))

@login_required(login_url='/?show_login=True')
def order(request, id):
    """
    """
    orders = Order.objects.filter(user=request.user)
    order = get_object_or_404(Order, pk=id, user=request.user)

    return render_to_response("customer/order.html", RequestContext(request, {
        "current_order" : order,
        "orders" : orders,
    }))

@login_required(login_url='/?show_login=True')
def account(request):
    """Displays the main screen of the current user's account.
    """
    user = request.user
    customer_attributes = customer_utils.get_or_create_customer_attributes(request)
    orders = Order.objects.filter(user=request.user)
    order = None
    if orders:
        order = orders[0]
    return render_to_response("customer/account.html", RequestContext(request, {
        "user" : user,
        'current_order': order,
        'customer_attributes':customer_attributes,
    }))


@login_required(login_url='/?show_login=True')
def change_data(request):
    user = request.user
    customer_attributes = customer_utils.get_or_create_customer_attributes(request)

    form_lk = FormForLk(initial={
        'fio': customer_attributes.fio,
        'email':customer_attributes.email,
        'phone':customer_attributes.phone,
        'dop_phone':customer_attributes.dop_phone,
        'organization': customer_attributes.organization,
        'inn':customer_attributes.inn,
        'kpp':customer_attributes.kpp,
        'region': customer_attributes.region,
        'city':customer_attributes.city,
        'street':customer_attributes.street,
        'building':customer_attributes.house,
        'corpus':customer_attributes.corpus,
        'builder':customer_attributes.stroenie,
        'office':customer_attributes.office,
        })

    if request.is_ajax():
        response = {'status':0,'errors':None}
        form_errors = {}
        main_form = FormForLk(request.POST)
        if not main_form.is_valid():
            for k in main_form.errors:
                form_errors[k] = main_form.errors[k][0]
        else:
            cd = main_form.cleaned_data
            customer_attributes.fio = cd.get('fio')
            customer_attributes.phone = cd.get('phone')
            customer_attributes.email = cd.get('email')
            customer_attributes.dop_phone = cd.get('dop_phone')

            customer_attributes.inn = cd.get('inn')
            customer_attributes.kpp = cd.get('kpp')
            customer_attributes.organization = cd.get('organization')

            customer_attributes.region = cd.get('region')
            customer_attributes.city = cd.get('city')
            customer_attributes.street = cd.get('street')
            customer_attributes.building = cd.get('building')
            customer_attributes.corpus = cd.get('corpus')
            customer_attributes.stroenie = cd.get('builder')
            customer_attributes.office = cd.get('office')
            customer_attributes.save()
      
        if len(form_errors) > 0:
            response = {'status':1,'errors':form_errors}
            return HttpResponse(json.dumps(response))
        else:
            response = {'status':100,'errors':None}
        return HttpResponse(json.dumps(response))
    
    return render_to_response("customer/change_data.html", RequestContext(request, {
        "user" : user,
        'customer_attributes':customer_attributes,
        "form_lk": form_lk,
    }))


@login_required(login_url='/?show_login=True')
def email(request):
    """Saves the email address from the data form.
    """
    saved = False
    if request.method == "POST":
        email_form = EmailForm(initial={"email" : request.user.email}, data=request.POST)
        if email_form.is_valid():
            request.user.email = email_form.cleaned_data.get("email")
            request.user.save()
            saved = True
            #return HttpResponseRedirect(reverse(email))
    else:
        email_form = EmailForm(initial={"email" : request.user.email})

    return render_to_response("customer/email.html", RequestContext(request, {
        "email_form": email_form,
        "saved": saved,
    }))

@login_required(login_url='/?show_login=True')
def password(request):
    """Changes the password of current user.
    """
    if request.method == "POST":
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse(password))
    else:
        form = PasswordChangeForm(request.user)

    return render_to_response("customer/password.html", RequestContext(request, {
        "form" : form
    }))


def password_reset(request, is_admin_site=False,
                   template_name='registration/password_reset_form.html',
                   email_template_name='registration/password_reset_email.html',
                   subject_template_name='registration/password_reset_subject.txt',
                   password_reset_form=PasswordResetForm,
                   token_generator=default_token_generator,
                   post_reset_redirect=None,
                   from_email=None,
                   current_app=None,
                   extra_context=None,
                   html_email_template_name=None):
    response = {}
    if request.method == "POST":
        form = password_reset_form(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user_exist = list(User.objects.filter(email=cd['email'],is_active=True))
            if len(user_exist) == 0:
                errors = {}
                errors['email'] = u'Пользователь не найден'
                response = {'status':1,'errors':errors}
                return HttpResponse(json.dumps(response))
            opts = {
                'use_https': request.is_secure(),
                'token_generator': token_generator,
                'from_email': from_email,
                'email_template_name': email_template_name,
                'subject_template_name': subject_template_name,
                'request': request,
                'html_email_template_name': html_email_template_name,
            }
            if is_admin_site:
                opts = dict(opts, domain_override=request.get_host())
            form.save(**opts)
            response = {'status':0,'errors':None}
        else:
            form = password_reset_form()
            errors = {}
            for k in form.errors:
                errors[k] = form.errors[k][0]
            response = {'status':1,'errors':errors}
    else:
        response['email'] = u'Введите адрес почты'
    return HttpResponse(json.dumps(response))
