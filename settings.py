# -*- coding: utf-8 -*-
# Django settings for apple project.
import os

DIRNAME = os.path.dirname(__file__)

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DEBUG_TOOLBAR = False
LOCAL_SETTINGS = True

ADMINS = (
    ('ak', 'akoptev1989@yandex.ru'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'tractor',                     
        'USER': 'docent',                      
        'PASSWORD': '165136',                  
        'HOST': '',                     
        'PORT': '',    
    
    }
}

USE_TZ = True
TIME_ZONE = 'Europe/Moscow'
LANGUAGE_CODE = 'ru-RU'

DATE_INPUT_FORMATS = ('%d.%m.%Y',)

SITE_ID = 1

USE_I18N = True
USE_L10N = True


MEDIA_ROOT = DIRNAME + "/../media"
MEDIA_URL = '/media/'


STATIC_ROOT = MEDIA_ROOT + '/static'
STATIC_URL = '/media/static/'

ADMIN_MEDIA_PREFIX = '/media/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    #Put strings here, like "/home/html/static" or "C:/www/django/static".
    #Always use forward slashes, even on Windows.
    #Don't forget to use absolute paths, not relative paths.
    #DIRNAME + '/gblocks/static',
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

SESSION_SAVE_EVERY_REQUEST = True
#FEINCMS_ADMIN_MEDIA = '/media/feincms/'
#FEINCMS_ADMIN_MEDIA_LOCATION = DIRNAME + FEINCMS_ADMIN_MEDIA

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'je7y77=7uz%qdmgjm7rj#5wu08&1qkaj0c1n6+sb1k6_oe596d'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'annoying.middlewares.StaticServe',
    'unslashed.middleware.RemoveSlashMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'pagination.middleware.PaginationMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.core.context_processors.debug',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'docent.context_processors.settings',
    
)
APPEND_SLASH = False
REMOVE_SLASH = True
ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    DIRNAME + '/templates',
    DIRNAME + '/docent/templates',
    DIRNAME + '/plugins',
)

THUMBNAIL_DEBUG = False
INSTALLED_APPS = (
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',

    # plugins
   # 'plugins',
    'plugins.cart',
    'plugins.catalog',
    'plugins.core',
    'plugins.customer',
    'plugins.mail',
    'plugins.manufacturer',
    'plugins.news',
    'plugins.simplepage',
    'plugins.order',
    'plugins.shipping',
    'plugins.slider',
    'plugins.reviews',
    'plugins.commerceml',
    'plugins.service',
    'plugins.photogallery',
    'plugins.videogallery',
    'plugins.contacts',
    'plugins.map',
    'plugins.vacancies',
    'plugins.articles',

    
    # utils
    'helpers',
    'helpers.gblocks',
    'helpers.filesandimages',

    #other
    'docent',
    'django_generic_flatblocks',
    'pagination',
    'sorl.thumbnail',
    'pytils',
    'widget_tweaks',
    'robokassa',
    'annoying',
    'mptt',
    'feincms',
    'ckeditor',
    'rollyourown.seo',
    'djcelery',
    'easy_thumbnails',
    'filer',
    'chronograph',
    'tinymce',
   # 'compressor',
    #'dbbackup',
    #'sphinxdoc',
    #'haystack',
    


)



MIGRATION_MODULES = {
    'filer': 'filer.migrations_django',
}
#django-celery
import djcelery
djcelery.setup_loader()
BROKER_URL = "localhost"
BROKER_BACKEND="redis"
REDIS_PORT=6379
REDIS_HOST = "localhost"
BROKER_USER = ""
BROKER_PASSWORD =""
BROKER_URL = "localhost"
REDIS_DB = 0
REDIS_CONNECT_RETRY = True
CELERY_SEND_EVENTS=True
CELERY_RESULT_BACKEND='redis'
CELERY_TASK_RESULT_EXPIRES =  10
CELERYBEAT_SCHEDULER="djcelery.schedulers.DatabaseScheduler"
CELERY_ALWAYS_EAGER=False


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
ALLOWED_HOSTS = ['voltrak.ru', 'www.xn--80adzconn.xn--p1ai',]

INTERNAL_IPS = ('127.0.0.1',)

PAGINATION_DEFAULT_WINDOW = 2

LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'
LOGIN_REDIRECT_URL = '/my-account/'


AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)
IPGEOBASE_ALLOWED_COUNTRIES = ['RU',]
ADMIN_TOOLS_MENU = 'menu.CustomMenu'
ADMIN_TOOLS_INDEX_DASHBOARD = 'dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'dashboard.CustomAppIndexDashboard'
CKEDITOR_JQUERY_URL = 'http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'
CKEDITOR_CONFIGS = {
    "default": {
        "removePlugins": "stylesheetparser",
        'allowedContent': True,
        'autoParagraph': False,
        'enterMode':2,
        'toolbar_Full': [
        ['Styles', 'Format', 'Bold', 'Italic', 'FontSize','Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ],
        ['Image', 'Flash', 'Table', 'HorizontalRule'],
        ['TextColor', 'BGColor'],
        ['Smiley','sourcearea', 'SpecialChar'],
        [ 'Link', 'Unlink', 'Anchor' ],
        [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ],
        [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ],
        [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
        [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ],
        [ 'Maximize', 'ShowBlocks' ]
    ],
    }
}
MPTT_USE_FEINCMS = True
SERVER_EMAIL = 'no-reply@voltrak.ru'
DEFAULT_FROM_EMAIL = 'no-reply@voltrak.ru'
EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'serkuz35@yandex.ru'
EMAIL_HOST_PASSWORD = 'kxbzAjFtlMq45vIGxOTkyA'
EMAIL_USE_TLS = False


CPU_TIME_LIMIT = 20
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

# robokassa ------------------------
ROBOKASSA_LOGIN = ''
ROBOKASSA_PASSWORD1 = ''  
ROBOKASSA_PASSWORD2 = ''
ROBOKASSA_USE_POST = True
ROBOKASSA_TEST_MODE = False

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'
CKEDITOR_UPLOAD_PATH = "uploads/"

THUMBNAIL_COLORSPACE = None
THUMBNAIL_PRESERVE_FORMAT = True 

TINYMCE_DEFAULT_CONFIG = {
    'theme': "advanced",
    'cleanup_on_startup': True,
    'custom_undo_redo_levels': 10,
    'width':1000,
    'height':300,
    'mode' : "textareas",
    'force_br_newlines' : False,
    'force_p_newlines' : False,
    'forced_root_block' : '',
}

try:
    from local_settings import *
except ImportError:
    pass
