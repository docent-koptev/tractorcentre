# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gblocks', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Seo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='title', blank=True)),
                ('keyword', models.TextField(verbose_name='keyword', blank=True)),
                ('description', models.TextField(verbose_name='keyword', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
