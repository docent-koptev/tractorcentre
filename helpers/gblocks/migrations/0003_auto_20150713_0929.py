# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('gblocks', '0002_seo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='titleandtext',
            name='text',
            field=tinymce.models.HTMLField(verbose_name='text', blank=True),
            preserve_default=True,
        ),
    ]
