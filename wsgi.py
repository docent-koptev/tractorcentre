import os
import sys

sys.path.insert(0, '/var/www/voltrak.ru/tenv/lib/python2.7/site-packages')
sys.path.insert(0, '/var/www/voltrak.ru/project')

#from django.core.handlers.wsgi import WSGIHandler

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
#application = WSGIHandler()
from django.core.wsgi import get_wsgi_application


application = get_wsgi_application()
